Starting from v4 of the analysis code there's the possibility to process SusySkimHiggsino ntuples in parallel using docker. In this folder you can find
1) A python script to create condor submission files
2) A python script to sequentially submit files to condor system

# How to use the scripts
The scripts have been written to create and submit .sub files to condor for each input sample that has to be processed. What you need to do is to configure 
- the name of the input ntuples in the sample_list array in both python scripts
- the path to the working directory on the local machine where submit files are created, as well as the path to input ntuples, the macros needed to perform the analysis (analysis_new.C, analysis_new.h, run_analysis.py) and the path to the local machine folder where you want to save job's output
- the selection, the path to input and ouput directories for run_analysis.py script
- some properties of the job, like the quantity of memory that the system should allocate, the number of CPUs, ...

# How it works
Provided that everything above has been configured correctly, when the job is executed on condor the Docker image specified by docker_image = dr.mi.infn.it/alesala/susy-analysis is downloaded from the local Docker hub (dr.mi.infn.it) and then a Docker container is created on the first available machine of the entire Milan computing cluster. Inside of it, a complete AnalysisBase release is set up to retrieve the necessary ROOT libraries. At the same time a new directory named DisplacedTrack is created. Thanks to the option transfer_input_files = YES, the macros required to perform the analysis (analysis_new.C, analysis_new.h) and the steering python script (run_analysis.py) are transferred by condor inside the container together with the executable (sample_name.sh) and the input ntuples. At this point the analysis on the ntuples is performed and the resulting output trees are saved in a new .root file inside the previously created DisplacedTrack folder (to avoid overwriting the existing .root input file). Once the job terminates, only the output ntuples are transferred back to the local machine working directory via the transfer_output_files command, while log, error and output files are saved. Using transfer_output_remaps you can also specify the path to the local machine folder where you want to save the output given by transfer_output_files.
