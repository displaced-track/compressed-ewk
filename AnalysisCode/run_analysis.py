import ROOT
from ROOT import *
import os
import argparse
import re

ROOT.gROOT.LoadMacro("analysis.C")

parser = argparse.ArgumentParser(description='Flags needed to drive the steering script of the analysis code')
parser.add_argument('-i','--InputPath', default='./', action='store', type=str, help='Path to input directory')
parser.add_argument('-o','--OutputDirectory', default='./', action='store', type=str, help='Path to output directory')
parser.add_argument('-n','--name', default='', action='store', type=str, help='Use this flag if you want to process only one specific sample from input path')

args = parser.parse_args()
InputPath = args.InputPath 
OutputDirectory = args.OutputDirectory 
name = args.name

for sample in os.listdir(InputPath):

	# If used, skip all samples whose name is not
	# equal to the one provided by the user
	if name not in sample: continue

	# Make sure we are reading trees from .root file
	if ".root" not in sample: continue

	# Setup analysis class
	ana = ROOT.analysis()

	print("Reading sample " + sample)
	ROOTFile = TFile(InputPath + sample)

	# Loop over trees in ROOTFile
	for key in ROOTFile.GetListOfKeys():

		# Check we are retrieving trees only
		if key.GetClassName() != "TTree": continue    
		chain = ROOTFile.Get(key.GetName())

		# Now do the analysis and save output trees
		print("Reading " + chain.GetName() + " with " + str(chain.GetEntries()) + " entries")

		if "data" in sample or "NoSys" in chain.GetName():
			ana.Loop(chain, -1) 
		else:
			ana.LoopOnSyst(chain, -1) 

		ana.Write(OutputDirectory + sample.split('_merged')[0] + ".root", str(chain.GetName()))
