import uproot
import numpy as np
np.set_printoptions(suppress=True) # print arrays without e notation
np.set_printoptions(threshold=10000)
import pandas as pd
pd.set_option('display.max_columns', None)  
import os.path
import warnings
from pandas.core.common import SettingWithCopyWarning
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)


import argparse
parser = argparse.ArgumentParser(description='Make Dataframse, specify SampleName, inputfile, outputDataframeName')
parser.add_argument('--S', type=str, default='N2C1p', help='Sample Name')
parser.add_argument('--I', type=str, default='../MGPy8EG_A14N23LO_N2C1p_150p7_150p35_150p0_MET75_merged_processed.root', help='Input File Name')
parser.add_argument('--O', type=str, default='df.pickle', help='Output Dataframe Name')
args = parser.parse_args()


SampleName = args.S
inputfile = uproot.open(args.I)
outputName = args.O

features_to_read = ['EventNumber','eventWeight','trkPt','trkZ0','trkD0','trkD0Sig','trkD0Err','trkparpdgId','trkPhi','trkEta','trkZ0SinTheta','trkisTight','jetPt','jetPhi','jetEta','met_Et','met_Phi','nLep_base','IsMETTrigPassed','minDPhi_met_allJets30','passEmulTightJetCleaning1Jet','nJet30','trkNonBaseAssocPtcone30','trknIBLHits'] 


inputfile = inputfile['MGPy8EG_A14N23LO_HH_150p7_150p35_150p0_MET75_NoSys;1']
inputfile.keys()

LeadingJet_pTCut = 250.
LeadingJet_EtaCut = 2.4
DPhiJetMET_Cut = 0.4

dictionary = {} #dictionary

for feature in features_to_read:
    dictionary[feature] = np.array(inputfile[feature]) #fill the dictionary

df = pd.DataFrame(dictionary) 

df['Sample'] = np.full(df.index.size,SampleName)
df['njet'] = np.full(df.index.size,0) 
df['LeadingJetPt'] = np.full(df.index.size,0.) 
df['LeadingJetEta'] = np.full(df.index.size,0.) 
df['DPhiJetMET'] = np.full(df.index.size, 0.)
df['ntracks'] = np.full(df.index.size,0) 
df['IsBaseline_trk'] = np.empty(df.index.size, dtype=object)
df['ntracks_baseline'] = np.full(df.index.size, 0)
df['IsAny_trk'] = np.empty(df.index.size, dtype=object)
df['IsIsolated_trk'] = np.empty(df.index.size, dtype=object)
df['minDeltaR_BaselineAny_trk'] = np.empty(df.index.size, dtype=object)
df['ntracks_isolated'] = np.full(df.index.size, 0)
df['IsDisplaced_trk'] = np.empty(df.index.size, dtype=object)
df['ntracks_displaced'] = np.full(df.index.size, 0)
df['DPhiTrackMET'] = np.empty(df.index.size, dtype=object)
df['IsMETAligned_track'] = np.empty(df.index.size, dtype=object)
df['ntracks_METAligned'] = np.full(df.index.size, 0)
df['HasIBLHits_track'] = np.empty(df.index.size, dtype=object)
df['ntracks_HasIBLHits'] = np.full(df.index.size, 0)
df['HasSR_track'] = np.empty(df.index.size, dtype=object)
df['ntracks_SR'] = np.full(df.index.size, 0)


def get_dphi(a, b):
    dphi = a-b
    if(dphi<-1*np.pi): dphi= 2*np.pi+dphi
    if(dphi>np.pi): dphi=2*np.pi-dphi
    return(dphi)

def get_entries():
    n_entries = df.index.size
    n_entries_SUSY = 0
    n_tracks = 0
    n_tracks_SUSY = 0
    n_tracks_N2 = 0
    n_tracks_C1p = 0
    n_tracks_C1m = 0
    for entry in df.index:
        if ( df['trkparpdgId'][entry].tolist().count(1000023) + df['trkparpdgId'][entry].tolist().count(1000024) + df['trkparpdgId'][entry].tolist().count(-1000024) != 0 ): 
            n_entries_SUSY += 1
        n_tracks += len(df['trkparpdgId'][entry].tolist())
        n_tracks_SUSY += df['trkparpdgId'][entry].tolist().count(1000023) + df['trkparpdgId'][entry].tolist().count(1000024) + + df['trkparpdgId'][entry].tolist().count(-1000024)
        n_tracks_N2 += df['trkparpdgId'][entry].tolist().count(1000023)
        n_tracks_C1p += df['trkparpdgId'][entry].tolist().count(1000024)
        n_tracks_C1m += df['trkparpdgId'][entry].tolist().count(-1000024)
    return n_entries, n_entries_SUSY, n_tracks, n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m


print('-------')
print('Sample: ',SampleName)

print('-------')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
print('Dataframe: n_entries = {},  n_entries_SUSY = {},  n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('Dataframe: n_tracks = {},  n_tracks_SUSY = {},  n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
print('-------')


UpTo4Jets_rejected_index = []


print('Total entries = {}'.format(df.index))
for entry in df.index:
    if(entry%100 == 0): print('Processing entry: {}'.format(entry))
    #if(entry>300): continue
    # Event selection
    if df['nLep_base'][entry] != 0: continue
    if df['IsMETTrigPassed'][entry] == False: continue

    if df['met_Et'][entry] < 300.: continue 
    if df['minDPhi_met_allJets30'][entry] < 0.4: continue 

    if df['passEmulTightJetCleaning1Jet'][entry] != 1: continue # Tight jet event cleaning
    if df['nJet30'][entry] == 0: continue # At least one central jet with pT > 30 GeV (|eta|<2.8)

    df['LeadingJetPt'][entry] = df['jetPt'][entry][0]
    df['LeadingJetEta'][entry] = df['jetEta'][entry][0]
    if ( (df['LeadingJetPt'][entry] < LeadingJet_pTCut) | (np.fabs(df['LeadingJetEta'][entry]) > LeadingJet_EtaCut) ): continue #Leading jet pT > 250 GeV and |eta| < 2.4

    if df['nJet30'][entry] > 4: continue # At least one central jet with pT > 30 GeV (|eta|<2.8)

    # Track selection
    df['ntracks'][entry] = df['trkPt'][entry].size
    if df['ntracks'][entry] == 0: continue

    # Displacement
    df['IsDisplaced_trk'][entry] = np.fabs(df['trkD0Sig'][entry]) > 2.
    df['ntracks_displaced'][entry] = df['IsDisplaced_trk'][entry].tolist().count(True)
    if df['ntracks_displaced'][entry] == 0: continue

    # Baseline
    df['IsBaseline_trk'][entry] = ((df['IsDisplaced_trk'][entry] == True) & (df['trkPt'][entry] > 1.) & (np.fabs(df['trkEta'][entry]) < 1.5) & (df['trkisTight'][entry] != False) & (np.fabs(df['trkD0'][entry]) < 10) & (np.fabs(df['trkZ0SinTheta'][entry]) < 3.))
    df['ntracks_baseline'][entry] = df['IsBaseline_trk'][entry].tolist().count(True)
    if df['ntracks_baseline'][entry] == 0: continue

    # MET alignment
    DPhiTrackMET_i = []
    for track in range(len(df['trkPt'][entry])): 
        DPhiTrackMET_i.append( np.fabs( get_dphi( df['met_Phi'][entry], df['trkPhi'][entry][track] ) ) )
    df['DPhiTrackMET'][entry] = np.array(DPhiTrackMET_i)
    df['IsMETAligned_track'][entry] = (df['IsBaseline_trk'][entry] == True) & (np.array(DPhiTrackMET_i) < 1)
    df['ntracks_METAligned'][entry] = df['IsMETAligned_track'][entry].tolist().count(True)
    if df['ntracks_METAligned'][entry] == 0: continue

    # Isolation
    df['IsIsolated_trk'][entry] = (df['IsMETAligned_track'][entry] == True) & (df['trkNonBaseAssocPtcone30'][entry] < 1e-10) # Isolated tracks
    df['ntracks_isolated'][entry] = df['IsIsolated_trk'][entry].tolist().count(True)
    if df['ntracks_isolated'][entry] == 0: continue

    # Track IBL Hits
    df['HasIBLHits_track'][entry] = (df['IsIsolated_trk'][entry] == True) & (df['trknIBLHits'][entry] > 0) # Isolated tracks
    df['ntracks_HasIBLHits'][entry] = df['HasIBLHits_track'][entry].tolist().count(True)
    if df['ntracks_HasIBLHits'][entry] == 0: continue

    # SR Displacement
    df['HasSR_track'][entry] = (df['HasIBLHits_track'][entry] == True) & (df['trkD0Sig'][entry] > 6) # Isolated tracks
    df['ntracks_SR'][entry] = df['HasSR_track'][entry].tolist().count(True)
    if df['ntracks_SR'][entry] == 0: continue



print('Proceeding to apply selection')


import csv

with open(args.I.replace("../MGPy8EG_A14N23LO_","").replace("_MET75_merged_processed.root",".csv"), 'w', newline='') as csvfile:

    writer = csv.writer(csvfile)

    writer.writerow(['selection','n_entries', 'n_entries_SUSY', 'n_tracks', 'n_tracks_SUSY','n_tracks_N2','n_tracks_C1p','n_tracks_C1m'])
    writer.writerow(['All',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])


    LeptonVeto_rejected_index = df[df.nLep_base!=0].index
    df.drop(LeptonVeto_rejected_index, inplace=True) 
    print('---Lepton-Veto---- All entries with leptons are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['LeptonVeto',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')

    METTrigger_rejected_index = df[df.IsMETTrigPassed!=1].index
    df.drop(METTrigger_rejected_index, inplace=True) 
    print('---METTrigger--')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['METTrigger',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')

    MET_rejected_index = df[df.met_Et<300.].index
    df.drop(MET_rejected_index, inplace=True) 
    print('---MET>300. GeV--')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['METcut',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')

    minDPhi_met_allJets30_rejected_index = df[df.minDPhi_met_allJets30<0.4].index
    df.drop(minDPhi_met_allJets30_rejected_index, inplace=True) 
    print('---DPhi(MET, AllJets30)>0.4--')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['DPhiJetMETcut',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')


    TightJetCleaning_rejected_index = df[df.passEmulTightJetCleaning1Jet!=1].index
    df.drop(TightJetCleaning_rejected_index, inplace=True) 
    print('---TightJetCleaning--')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['TightJetCleaning',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')


    AtLeastOneJet_rejected_index = df[df.nJet30==0].index
    df.drop(AtLeastOneJet_rejected_index, inplace=True) 
    print('---At Least One Jet with pT>30 GeV and |eta|<2.8--')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['AtLeastOneJet',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')


    LeadingJet_rejected_index = df[(df.LeadingJetPt < LeadingJet_pTCut) | (np.fabs(df.LeadingJetEta) > LeadingJet_EtaCut)].index
    df.drop(LeadingJet_rejected_index, inplace=True) 
    print('---LeadingJet pT > {} GeV and | LeadingJet Eta | < {}'.format(LeadingJet_pTCut,LeadingJet_EtaCut))
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['LeadingJet',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')


    UpTo4Jets_rejected_index = df[df.nJet30>4].index
    df.drop(UpTo4Jets_rejected_index, inplace=True) 
    print('---Up to 4 Jets with pT>30 GeV and |eta|<2.8--')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['UpTo4Jets',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
    print('-------')


    NoTracksVeto_rejected_index = df[df.ntracks==0].index
    #print(NoTracksVeto_rejected_index) -------
    df.drop(NoTracksVeto_rejected_index, inplace=True)
    print('---NoTracksVeto_rejected_index ---- All entries without any displaced tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    writer.writerow(['NoTracksVeto',n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m])
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))
    print('-------')


    NoDisplacedTrackVeto_rejected_index = df[df.ntracks_displaced==0].index
    df.drop(NoDisplacedTrackVeto_rejected_index, inplace=True)
    print('---NoDisplacedTrackVeto ---- All entries without any displaced tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    n_displaced_tracks = 0
    n_displaced_tracks_SUSY = 0
    n_displaced_tracks_N2 = 0
    n_displaced_tracks_C1p = 0
    n_displaced_tracks_C1m = 0
    for entry in df.index:
        for track in range(len(df['trkPt'][entry])):
            if df['IsDisplaced_trk'][entry][track] == True:
                n_displaced_tracks += 1
                if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024 or df['trkparpdgId'][entry][track] == -1000024:
                    n_displaced_tracks_SUSY += 1
                if df['trkparpdgId'][entry][track] == 1000023:
                    n_displaced_tracks_N2 += 1
                if df['trkparpdgId'][entry][track] == 1000024:
                    n_displaced_tracks_C1p += 1
                if df['trkparpdgId'][entry][track] == -1000024:
                    n_displaced_tracks_C1m += 1
    writer.writerow(['Displacement',n_entries,n_entries_SUSY,n_displaced_tracks,n_displaced_tracks_SUSY,n_displaced_tracks_N2,n_displaced_tracks_C1p,n_displaced_tracks_C1m])
    print('n_displaced_tracks = {}, n_displaced_tracks_SUSY = {}, n_displaced_tracks_SUSY/n_displaced_tracks = {}'.format(n_displaced_tracks,n_displaced_tracks_SUSY,n_displaced_tracks_SUSY/n_displaced_tracks))
    print('-------')


    NoBaselineTrackVeto_rejected_index = df[df.ntracks_baseline==0].index 
    df.drop(NoBaselineTrackVeto_rejected_index, inplace=True)
    print('---NoBaselineTrackVeto ---- All entries without any displaced tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    n_baseline_tracks = 0
    n_baseline_tracks_SUSY = 0
    n_baseline_tracks_N2 = 0
    n_baseline_tracks_C1p = 0
    n_baseline_tracks_C1m = 0
    for entry in df.index:
        for track in range(len(df['IsBaseline_trk'][entry])):
            if df['IsBaseline_trk'][entry][track] == True:
                n_baseline_tracks += 1
                if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024 or df['trkparpdgId'][entry][track] == -1000024:
                    n_baseline_tracks_SUSY += 1
                if df['trkparpdgId'][entry][track] == 1000023:
                    n_baseline_tracks_N2 += 1
                if df['trkparpdgId'][entry][track] == 1000024:
                    n_baseline_tracks_C1p += 1
                if df['trkparpdgId'][entry][track] == -1000024:
                    n_baseline_tracks_C1m += 1
    writer.writerow(['Baseline',n_entries,n_entries_SUSY,n_baseline_tracks,n_baseline_tracks_SUSY,n_baseline_tracks_N2,n_baseline_tracks_C1p,n_baseline_tracks_C1m])
    print('n_baseline_tracks = {}, n_baseline_tracks_SUSY = {}, n_baseline_tracks_SUSY/n_baseline_tracks = {}'.format(n_baseline_tracks,n_baseline_tracks_SUSY,n_baseline_tracks_SUSY/n_baseline_tracks))      
    print('-------')


    NoMETAlignedTrackVeto_rejected_index = df[df.ntracks_METAligned==0].index 
    df.drop(NoMETAlignedTrackVeto_rejected_index, inplace=True)
    print('---NoMETalignedTrackVeto ---- All entries without any METaligned tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    n_METaligned_tracks = 0
    n_METaligned_tracks_SUSY = 0
    n_METaligned_tracks_N2 = 0
    n_METaligned_tracks_C1p = 0
    n_METaligned_tracks_C1m = 0
    for entry in df.index:
        for track in range(len(df['trkPt'][entry])):
            if df['IsMETAligned_track'][entry][track] == True:
                n_METaligned_tracks += 1
                if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024 or df['trkparpdgId'][entry][track] == -1000024:
                    n_METaligned_tracks_SUSY += 1
                if df['trkparpdgId'][entry][track] == 1000023:
                    n_METaligned_tracks_N2 += 1
                if df['trkparpdgId'][entry][track] == 1000024:
                    n_METaligned_tracks_C1p += 1
                if df['trkparpdgId'][entry][track] == -1000024:
                    n_METaligned_tracks_C1m += 1
    writer.writerow(['METAlignement',n_entries,n_entries_SUSY,n_METaligned_tracks,n_METaligned_tracks_SUSY,n_METaligned_tracks_N2,n_METaligned_tracks_C1p,n_METaligned_tracks_C1m])
    print('n_METaligned_tracks = {}, n_METaligned_tracks_SUSY = {}, n_METaligned_tracks_SUSY/n_METaligned_tracks = {}'.format(n_METaligned_tracks,n_METaligned_tracks_SUSY,n_METaligned_tracks_SUSY/n_METaligned_tracks))   
    print('-------')



    NoIsolatedTrackVeto_rejected_index = df[df.ntracks_isolated==0].index
    df.drop(NoIsolatedTrackVeto_rejected_index, inplace=True)
    print('---NoIsolatedTrackVeto ---- All entries without any isolated tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    n_isolated_tracks = 0
    n_isolated_tracks_SUSY = 0
    n_isolated_tracks_N2 = 0
    n_isolated_tracks_C1p = 0
    n_isolated_tracks_C1m = 0
    for entry in df.index:
        for track in range(len(df['trkPt'][entry])):
            if df['IsIsolated_trk'][entry][track] == True:
                n_isolated_tracks += 1
                if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024 or df['trkparpdgId'][entry][track] == -1000024:
                    n_isolated_tracks_SUSY += 1
                if df['trkparpdgId'][entry][track] == 1000023:
                    n_isolated_tracks_N2 += 1
                if df['trkparpdgId'][entry][track] == 1000024:
                    n_isolated_tracks_C1p += 1
                if df['trkparpdgId'][entry][track] == -1000024:
                    n_isolated_tracks_C1m += 1
    writer.writerow(['Isolation',n_entries,n_entries_SUSY,n_isolated_tracks,n_isolated_tracks_SUSY,n_isolated_tracks_N2,n_isolated_tracks_C1p,n_isolated_tracks_C1m])
    print('n_isolated_tracks = {}, n_isolated_tracks_SUSY = {}, n_isolated_tracks_SUSY/n_isolated_tracks = {}'.format(n_isolated_tracks,n_isolated_tracks_SUSY,n_isolated_tracks_SUSY/n_isolated_tracks))      
    print('-------')


    NoIBLHitsTrackVeto_rejected_index = df[df.ntracks_HasIBLHits==0].index
    df.drop(NoIBLHitsTrackVeto_rejected_index, inplace=True)
    print('---NoIBLHitsTrackVeto ---- All entries without any IBL hit tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    n_HasIBLHits_tracks = 0
    n_HasIBLHits_tracks_SUSY = 0
    n_HasIBLHits_tracks_N2 = 0
    n_HasIBLHits_tracks_C1p = 0
    n_HasIBLHits_tracks_C1m = 0
    for entry in df.index:
        for track in range(len(df['trkPt'][entry])):
            if df['HasIBLHits_track'][entry][track] == True:
                n_HasIBLHits_tracks += 1
                if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024 or df['trkparpdgId'][entry][track] == -1000024:
                    n_HasIBLHits_tracks_SUSY += 1
                if df['trkparpdgId'][entry][track] == 1000023:
                    n_HasIBLHits_tracks_N2 += 1
                if df['trkparpdgId'][entry][track] == 1000024:
                    n_HasIBLHits_tracks_C1p += 1
                if df['trkparpdgId'][entry][track] == -1000024:
                    n_HasIBLHits_tracks_C1m += 1
    writer.writerow(['IBLHits',n_entries,n_entries_SUSY,n_HasIBLHits_tracks,n_HasIBLHits_tracks_SUSY,n_HasIBLHits_tracks_N2,n_HasIBLHits_tracks_C1p,n_HasIBLHits_tracks_C1m])
    print('n_HasIBLHits_tracks = {}, n_HasIBLHits_tracks_SUSY = {}, n_HasIBLHits_tracks_SUSY/n_HasIBLHits_tracks = {}'.format(n_HasIBLHits_tracks,n_HasIBLHits_tracks_SUSY,n_HasIBLHits_tracks_SUSY/n_HasIBLHits_tracks))      
    print('-------')


    NoHasSRtracks_rejected_index = df[df.ntracks_SR==0].index
    df.drop(NoHasSRtracks_rejected_index, inplace=True)
    print('---NoHasSRtracks ---- All entries without S(d0)>6 tracks are removed')
    n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY,n_tracks_N2,n_tracks_C1p,n_tracks_C1m = get_entries()
    print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
    n_SR_tracks = 0
    n_SR_tracks_SUSY = 0
    n_SR_tracks_N2 = 0
    n_SR_tracks_C1p = 0
    n_SR_tracks_C1m = 0
    for entry in df.index:
        for track in range(len(df['trkPt'][entry])):
            if df['HasSR_track'][entry][track] == True:
                n_SR_tracks += 1
                if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024 or df['trkparpdgId'][entry][track] == -1000024:
                    n_SR_tracks_SUSY += 1
                if df['trkparpdgId'][entry][track] == 1000023:
                    n_SR_tracks_N2 += 1
                if df['trkparpdgId'][entry][track] == 1000024:
                    n_SR_tracks_C1p += 1
                if df['trkparpdgId'][entry][track] == -1000024:
                    n_SR_tracks_C1m += 1
    writer.writerow(['SRtracks',n_entries,n_entries_SUSY,n_SR_tracks,n_SR_tracks_SUSY,n_SR_tracks_N2,n_SR_tracks_C1p,n_SR_tracks_C1m])
    print('n_SR_tracks = {}, n_SR_tracks_SUSY = {}, n_SR_tracks_SUSY/n_SR_tracks = {}'.format(n_SR_tracks,n_SR_tracks_SUSY,n_SR_tracks_SUSY/n_SR_tracks))      
    print('-------')

csvfile.close()
#df.to_pickle(outputName)
