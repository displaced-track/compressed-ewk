# How to process the ntuples and save them as flat trees
To process the ntuples, apply the selections at the event and track levels, and save them as flat trees to be easily processed by machine learning algorithms or HistFitter, you can use the analysis.C macro. The code has been updated with the possibility of performing a 0-lepton, 1-lepton or 2-leptons selection (the latters used for background studies). Older versions of the same code can be found in the commit history of this same repo.

In the most recent version of the analysis code, you can process the ntuples by simply doing
<pre>setupATLAS
lsetup root
python run_analysis.py -s selection -i input_path_to_the_ntuples -o output_path </pre>
where -s stands for the selection (0, 1, 2), -i for the path to the input ntuples and -o for the output path were the ntuples are saved. If you want to process just one sample you can use -n sample_name.

Alternatively, a python implementation can be used. This is very slow and not recommended!
<pre> cd python_code </pre>
A script can be used to run MakeDataframe.py first and SaveTracks.py subsequently
<pre> python script_All_Dataframes.py </pre>
The output is a pandas dataframe in the pickle format.
