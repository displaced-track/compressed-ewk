# Compressed-EKW
Repository containing the useful code for the Compressed-EWK 2ndWave Analysis
Authors: Eric Ballabene, Tommaso Lari, Alessia Murrone, Alessandro Sala

# Git workflow
Different projects can have different upstreams. To avoid conflicts,
- cd into the repository home
<pre> cd compressed-ewk </pre>
- from here, git add all files, and follow the usual workflow
<pre> git add . --all </pre>
<pre> git commit -m "Commit message" </pre>
<pre> git push </pre>
Only the modified files will be committed.

# SusySkimAna
- Documented in README

# SimpleAnalysis
- The SimpleAnalysis implementation of the analysis can be found in SimpleAnalysis/SimpleAnalysisCodes/src/DisplacedTrack2018.cxx
- Note: To compiple SimpleAnalysis, a fresh new version from https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis.git is needed. Remember to clone with --recursive flag.
- Note: 2 different branches in local repository, one compiled with AnalysisBase,21.2.100, one compiled with AnalysisBase,21.2.149
- Don't commit files from here, since changes will be tentatively commited to the upstream https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis.git, and they will fail. 
