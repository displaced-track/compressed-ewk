# Draw exclusion limits on summary plot
First setup the environment
<pre> source setup.sh </pre>
Then move to the existing_limits folder and produce the csv file needed to draw the contour plot
<pre> cd existing_limits </pre>
<pre> python rootTGraph_to_csv.py -i path_to_input_file -o path_to_output_file </pre> 
where the input file should be the output file obtained running the harvestToContour.py script. Now move back and do
<pre> python summary_higgsino.py </pre>
which will produce a nice contour plot in the figs folder. 
