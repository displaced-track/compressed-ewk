# Setup - HistFitter v1.0.0
<pre> export ALRB_rootVersion=6.22.00-x86_64-centos7-gcc8-opt </pre>
<pre> setupATLAS </pre>
<pre> lsetup root </pre>
<pre> lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt" </pre>
<pre> source histfitter/setup.sh </pre>

# ABCD fit
Compute the normalization factors via the ABCD method. Use 

<pre> HistFitter.py -twxf -F bkg -u"--doABCDBkg --doMethodX" ABCD-ewk-fit.py </pre>

to perform abackground-only  fit and select one of the available methods via --doMethodX flag. Use

<pre> HistFitter.py -twxf -F excl -p -u"--doExclusion --doMethodX --doTestSystematics --point point" ABCD-ewk-fit.py </pre>

to perform a model-dependent exclusion fit on a specific mass signal mass point (e.g. 151-151-150). Use --useAlternativeSR whenever the mass splitting between the higgsinos is 1 GeV.

# Plots
If you want to make some plots use

<pre> HistFitter.py -twxf -F bkg -D "before,after" -u"--doABCDBkg --doMethodX --doValidationPlots --VariableToPlot VARIABLE --ChannelToPlot CHANNEL" ABCD-ewk-fit.py </pre>

