#!/usr/bin/env python

import itertools
import argparse
import systematicsTool as st #import the systematic tool
import readDatabase as rdb   #import the systematic database
import os,sys
import pprint
from collections import OrderedDict
from ROOT import TObject, TFile

inputParser = argparse.ArgumentParser(description='Parse command line options')
inputParser.add_argument('--sample',    type = str, default = '',    required = True,                         help = 'Sample name')
inputParser.add_argument('--region',    type = str, default = '',    required = True,                         help = 'Region name')
args = inputParser.parse_args()

customReplacements = OrderedDict([
    ('.','p'),
    (':',''),
	('=',''),
])

for sample, region in itertools.product( args.sample.split(","), args.region.split(",") ):

	print("Computing theory systematics for sample " + sample + " in region " + region)

	if "Znunu" in sample or "Zee" in sample or "Zmumu" in sample or "Ztautau" in sample or "Zjets" in sample:
		dsid = 700335
	elif "Wenu" in sample or "Wmunu" in sample or "Wtaunu" in sample:
		dsid = 700335
	elif "Gjets" in sample:
		dsid = 364541
	elif "dijet" in sample:
		dsid = 364702
	elif "ttbar" in sample:
		dsid = 410470
	elif "singletop" in sample:
		dsid = 410644
	elif "diboson" in sample:
		dsid = 345706 
	elif "higgsino" in sample:
		dsid = 510685
	else:
		raise Exception("***** BAD CHOICE - BREAK *******")

	# Input and output file directories
	indir = "./histograms"
	inFile = sample + "_" + region
	outdir = "./Combined_2/" + sample + "/" + region
	
	# For V+jets we need to find the max and min EW corrections so that we
	# can then combine correctly the uncertainties in the custom recipe
	sample_name = sample
	if "had_lep" in sample_name: sample_name = "Wtaunu_hadlep"
	if "others" in sample_name: sample_name = "Wtaunu_others"
	
	if dsid == 700335:
		ROOTFile = TFile(indir + "/" + inFile + ".root", "update")
		EWWeights = {}
		for key in ROOTFile.GetListOfKeys():
			if key.GetName() == "h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200":            nominal = ROOTFile.Get(key.GetName()).GetBinContent(1)
			if key.GetName() == "h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_ASSEW":      EWWeights["ASSEW"] = abs(ROOTFile.Get(key.GetName()).GetBinContent(1) - nominal) 
			if key.GetName() == "h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_EXPASSEW":   EWWeights["EXPASSEW"] = abs(ROOTFile.Get(key.GetName()).GetBinContent(1) - nominal)
			if key.GetName() == "h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_MULTIASSEW": EWWeights["MULTIASSEW"] = abs(ROOTFile.Get(key.GetName()).GetBinContent(1) - nominal)

		histoMAX = ROOTFile.Get("h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_" + max(EWWeights, key = EWWeights.get)).Clone()
		histoMAX.SetName("h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_MAXASSEW")
		histoMIN = ROOTFile.Get("h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_" + min(EWWeights, key = EWWeights.get)).Clone()
		histoMIN.SetName("h" + sample_name + "_" + region + "_cuts__MUR1_MUF1_PDF303200_MINASSEW")

		histoMAX.Write("",TObject.kOverwrite)
		histoMIN.Write("",TObject.kOverwrite)

		ROOTFile.Close()
		
	regexFilter = None # Filter for only certain histogram names
	regexVeto   = None # Veto any histogram names you wish to skip

	# Retrieve the weights for this samples and their combination recipes
	weightList = rdb.getWeights(dsid)[0] 

    # Manually remove weights and recipes we don't want to use
	if dsid == 700335:
		weightList.pop("Other_ME_notForAnalyses", None)
		weightList.pop("ME_PDF91400_var", None)
		weightList.pop("ME_PDF91400", None)
		weightList.pop("ME_ONLY_PDF303200", None)
		weightList.pop("ME_PDF304400", None)
		weightList.pop("ME_PDF303200_ASSEWLO1_var", None)
		weightList.pop("ME_PDF303200_ASSEWLO1LO2_var", None)
		weightList.pop("ME_PDF303200_ASSEWLO1LO2LO3_var", None)
		weightList.pop("ME_PDF303200_ASSEWLO1LO2LO3", None)
		weightList.pop("ME_PDF303200_ASSEWLO1LO2", None)
		weightList.pop("ME_PDF303200_ASSEWLO1", None)
		weightList.pop("ME_PDF303200_ASSEW_var", None)
	
		'''weightList.pop("PDF303200_Nominal", None)
		weightList.pop("ME_PDF303200_var", None)
		weightList.pop("ME_PDF303200_ASSEW", None)
		weightList.pop("ME_PDF303200_alphaS_NNPDF_NNLO", None)
		weightList.pop("ME_PDF27400", None)
		weightList.pop("ME_PDF14068", None)'''
		
		EWweightList = {'ME_PDF303200_MINASSEW': {'combination': 'lhapdf',
          			                              'nominal': 'MUR1_MUF1_PDF303200_MINASSEW',
                             					  'nominal_pdf': '303200',
                            					  'type': 'altPDF',
						                          'weights': ['MUR1_MUF1_PDF303200_MINASSEW']},
	        	        'ME_PDF303200_MAXASSEW': {'combination': 'lhapdf',
                                                  'nominal': 'MUR1_MUF1_PDF303200_MAXASSEW',
                                                  'nominal_pdf': '303200',
                                                  'type': 'altPDF',
                                                  'weights': ['MUR1_MUF1_PDF303200_MAXASSEW']},
                       	}
		weightList.update(EWweightList)

	elif dsid == 364541:
		weightList.pop("Other_ME_notForAnalyses", None)
		weightList.pop("ME_PDF303200_MULTIASSEW", None)
		weightList.pop("ME_PDF303200_EXPASSEW", None)

	elif dsid == 364702:
		weightList.pop("Other_ME_notForAnalyses", None)
		weightList.pop("transferred_Pythia8_PDF247000_Var3c_alphaS", None)	
		weightList.pop("transferred_Pythia8_PDF247000_ISR_PDF", None)
		weightList.pop("ME_PDF303200_MULTIASSEW", None)
		weightList.pop("ME_PDF303200_EXPASSEW", None)

	pprint.pprint(weightList)

	# User defined schema to retrieve histograms from file
	schema = "!INDIR/!INFILE.root:h!AONAME__!CRWEIGHTNAME" 

	os.system("mkdir -p %s" %outdir)

	# Automagically combine the per-weight histograms into per-variation analysis objects!
	result = st.combineAllVariations(weightList, indir, outdir, regexFilter = regexFilter, regexVeto = regexVeto, returnOnlyVariationsInComination = True, schema = schema, inFile = inFile, customWeightNameReplacements = customReplacements) 
	
	if dsid == 700335:
		result = st.combineAllVariations(weightList, indir, outdir, regexFilter = regexFilter, regexVeto = regexVeto, returnOnlyVariationsInComination = True, schema = schema, inFile = inFile, customWeightNameReplacements = customReplacements, combinationRecipe = "myCombination.yaml:ME_PDF303200_combined") 
	if dsid == 364541:
		result = st.combineAllVariations(weightList, indir, outdir, regexFilter = regexFilter, regexVeto = regexVeto, returnOnlyVariationsInComination = True, schema = schema, inFile = inFile, customWeightNameReplacements = customReplacements, combinationRecipe = "myCombination.yaml:ME_PDF261000_combined") 
	if dsid == 364702:
		result = st.combineAllVariations(weightList, indir, outdir, regexFilter = regexFilter, regexVeto = regexVeto, returnOnlyVariationsInComination = True, schema = schema, inFile = inFile, customWeightNameReplacements = customReplacements, combinationRecipe = "myCombination.yaml:ME_PDF247000_combined") 
