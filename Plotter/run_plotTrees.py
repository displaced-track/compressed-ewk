import os


variables_to_plot = [
    "lep1pT",
    "lep2pT",
    "MET",
    "MT2",
    "mll",
    "METsig",
    # "dPhiLL",
    "dPhiMETL1",
    "dPhiMETL2",
    "cosTstar",
    "DPhib",
    "BDTDeltaM100_90",
    "BDTVVDeltaM100_90",
    "BDTtopDeltaM100_90",
    "BDTothersDeltaM100_90",
    "BDTDeltaM100_90_high"

]

# variables_to_plot = [
#     "BDTDeltaM100_90", "BDTothersDeltaM100_90", "BDTVVDeltaM100_90", "BDTtopDeltaM100_90"]

# variables_to_plot = ["BDTVVDeltaM100_90","BDTDeltaM100_90"]
# variables_to_plot = ["BDTDeltaM100_90_high"]
# variables_to_plot += ["BDTtopDeltaM100_90"]
os.system("echo plotting > output.log")
from cuts_2L0J import configure
cut_dict = configure()

cuts_to_do = ["DF0J_presel", "SF0J_presel", "SF1J_presel", "DF1J_presel"]

cuts_to_do=["DF1J_presel"]


# cuts_to_do = ["DF0J_VRVV", "DF0J_CRVV", "DF1J_VRVV", "DF1J_CRVV","SF0J_VRVV", "SF0J_CRVV","SF1J_VRVV", "SF1J_CRVV" ]
cuts_to_do += ["DF0J_VRtop", "DF0J_CRtop", "DF1J_VRtop", "DF1J_CRtop","SF0J_VRtop", "SF0J_CRtop","SF1J_VRtop", "SF1J_CRtop" ]
cuts_to_do += ["SR_DF0J_inclusive", "SR_SF0J_inclusive", "SR_DF1J_inclusive", "SR_SF1J_inclusive" ]

# cuts_to_do = ["DF1J_VRVV",
# "DF1J_CRVV",
# "SF1J_VRVV",
# "SF1J_CRVV"]

cuts_to_do += ["SF0J_VR_reversedMETsig", "DF1J_VR_reversedMETsig",
            "SF1J_VR_reversedMETsig"]


cuts_to_do = ["DF0J_presel", "SF0J_presel","DF0J_VR_reversedMETsig"]
# cuts_to_do+=["SF0J_presel_blind", "DF0J_presel_blind"]

cuts_to_do= ["DF0J_SR_BDT80", "DF0J_SR_BDT85"]
cuts_to_do=["DF0J_SR_BDT80_METsig8"]
# for cut in cut_dict:
for cut in cuts_to_do:
    print (cut) 

    #if "DF0J_VRVV" in cut or "DF0J_CRVV" in cut:
    #if cut == "DF0J_VR_reversedMETsig" or cut == "SF0J_VR_reversedMETsig" or cut == "SF1J_VR_reversedMETsig" or cut == "DF1J_VR_reversedMETsig":
    # if cut in cuts_to_do:
    # if "DM30" in cut:
    # if cut in cuts_to_do:
    # if "1J" in cut:
    # if True:
    # if "current" not in cut:
    #     continue
    for var in variables_to_plot:

        if "SR" not in cut:
            os.system("./plotTrees.py -v {0} -s {1} -u | tee output.log".format(var,cut))
        else: os.system("./plotTrees.py -v {0} -s {1} | tee output.log".format(var,cut))

    