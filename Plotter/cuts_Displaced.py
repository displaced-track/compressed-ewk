"""
Specify cut strings for 2L0J region definitions - copied from Rupert's

Write output with
python -c "import cuts;cuts.write()"
"""

def join(cuts):
    """Join boolean logic list into an executable string"""
    return cuts# hack "(" + " && ".join(cuts) + ")"


def join_dict(d):
    """Join all values in dict d"""
    return {key: join(value) for key, value in d.items()}


def replace(L, was, now):
    """Return a copy of L with the first `was' replaced by `now'"""
    newL = list(L)
    newL[newL.index(was)] = now
    return newL


def configure(precut=False):
    """Return dict of cuts. precut True assumes dev skim slim"""
    # cuts for now
    cuts = join_dict({
        
        "presel"   : ["1."],

        'Inclusive_blind' : ['TrackPt > 0.'], 

        'MET_100_300' : ['MET>100' , 'MET<300'], 
      
        'MET_300' : ['MET>300'], 

        'MET700_Sd05' : ['MET>700','TrackD0Sig > 5'], 

      
    })
    return cuts


def write(outname="regions.h"):
    """Write out a header file with macros for these regions"""
    cuts = configure(precut=False)
    with open(outname, "w") as f:
        for R in cuts:
            # switch to vector pointer syntax
            S = cuts[R].replace("[", "->at(").replace("]", ")")
            f.write("#define _" + R + "_ " + S + "\n")

d_cuts = configure(True)
def configure_cuts(var, add_cuts, sig_reg, isData18, allNCuts, print_cuts=True):
    # From cut lists, build cut string doing N-1 appropriately
    l_cuts = d_cuts[sig_reg]

    # In case we do NOT want to skip cut on variable to be plotted
    #if allNCuts or var in l_blinding_cuts or 'presel' in sig_reg or 'VR-com' in sig_reg:
    if allNCuts:
        l_cuts_nMinus1 = l_cuts
    else:
        # (N-1) if variable to be plotted is in cut, do not cut on it 
        l_cuts_nMinus1 = [cut for cut in l_cuts if var not in cut]

    # join cuts with && (AND) operator
    cuts = ' && '.join(l_cuts_nMinus1)
    added_cuts = cuts + ' && ' + add_cuts

    if print_cuts:
        print('===============================================')
        print('Cuts applied:')
        for x in l_cuts_nMinus1:
          print (x)
        print('-----------------------------------------------')
        print( 'Unweighted final cut-string:', added_cuts)
        print('===============================================')

    return added_cuts
