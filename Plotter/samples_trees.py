
#____________________________________________________________________________
def configure_samples(isData15_16, isData17, isData18, isData15_18, isRJProcessed, withoutDNN = False):
  from ROOT import TColor
  from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange

  # Blues
  #myLighterBlue=TColor.GetColor('#deebf7')
  myLighterBlue=kAzure-2
  myLightBlue  =TColor.GetColor('#9ecae1')
  myMediumBlue =TColor.GetColor('#0868ac')
  myDarkBlue   =TColor.GetColor('#08306b')

  # Greens
  myLightGreen   =TColor.GetColor('#c7e9c0')
  myMediumGreen  =TColor.GetColor('#41ab5d')
  myDarkGreen    =TColor.GetColor('#006d2c')

  # Oranges
  myLighterOrange=TColor.GetColor('#ffeda0')
  myLightOrange  =TColor.GetColor('#fec49f')
  myMediumOrange =TColor.GetColor('#fe9929')

  # Greys
  myLightestGrey=TColor.GetColor('#f0f0f0')
  myLighterGrey=TColor.GetColor('#e3e3e3')
  myLightGrey  =TColor.GetColor('#969696')

  # Pinks
  myLightPink = TColor.GetColor('#fde0dd')
  myMediumPink = TColor.GetColor('#fcc5c0')
  myDarkPink = TColor.GetColor('#dd3497')

  # Purples
  myLightPurple   = TColor.GetColor('#dadaeb')
  myMediumPurple  = TColor.GetColor('#9e9ac8')
  myDarkPurple    = TColor.GetColor('#6a51a3')
  #myLightPurple   = TColor.GetColor('#967BB6')
  #myMediumPurple  = TColor.GetColor('#66369D')
  #myDarkPurple    = TColor.GetColor('#522888')

  # Turquoise
  myLightTurquoise  = TColor.GetColor('#7FB1B2')
  myMediumTurquoise = TColor.GetColor('#539798')
  myDarkTurquoise   = TColor.GetColor('#297D7D')

  # Reds
  myMediumRed = TColor.GetColor('#EE220D')
  myLightRed = TColor.GetColor('#EE220D')


  # sample path suffix
  
  # data
  data_suffix='_tree.root'#'_merged_processed.root'
  # if isData15_16: data_suffix='15-16_merged_processed.root'
  # elif isData17: data_suffix='17_merged_processed.root'
  # elif isData18: data_suffix='18_merged_processed.root'
  # elif isData15_18: data_suffix='15-18_merged_processed.root'
  
  # bkg
  bkg_suffix='_tree.root'#'_merged_processed.root'
  
  # ttbar
  ttbar_suffix='_tree.root'#'_merged_processed.root'
  #ttbar_suffix='_nonallhad_merged_processed.root'
  #if isData18 or isData15_18: ttbar_suffix='_nonallhad_merged_processed.root'
  #elif isData15_16 or isData17: ttbar_suffix='_dilep_merged_processed.root'
  
  # signal
  sig_suffix='_tree.root'#'_merged_processed.root'




  if withoutDNN == True:
      d_samp = {
    'Wenu'     :{'type':'bkg', 'leg':'Wenu',        'f_color':kRed+2, 'l_color':0,  'path' :'Wenu.root'},  # template dict element
    'Wmunu'    :{'type':'bkg', 'leg':'Wmunu',       'f_color':kOrange,'l_color':0,  'path' :'Wmunu.root'},  # template dict element
    'Wtaunu'   :{'type':'bkg', 'leg':'Wtaunu',      'f_color':kYellow,'l_color':0,  'path' :'Wtaunu.root'},  # template dict element
    'Znunu'    :{'type':'bkg', 'leg':'Znunu',       'f_color':kRed,'l_color':0,  'path' :'Znunu.root'},  # template dict element
    'ttbar'    :{'type':'bkg', 'leg':'ttbar',       'f_color':kGray+1,'l_color':0,  'path' :'ttbar.root'},  # template dict element

    # Signals
    "151_150p5_150" :{"type":"sig","leg":"(151, 150.5, 150)","l_color":  kGreen,"path":"higgsino_151_150p5_150.root"},
    "150p7_150p35_150" :{"type":"sig","leg":"(150.7, 150.35, 150)","l_color":  myMediumBlue,"path":"higgsino_150p7_150p35_150.root"},
    "150p5_150p5_150" :{"type":"sig","leg":"(150.5, 150.5, 150)","l_color":  myDarkGreen,"path":"higgsino_150p5_150p5_150.root"},
    "151_151_150" :{"type":"sig","leg":"(151, 151, 150)","l_color":  myLightPurple,"path":"higgsino_151_151_150.root"},
    }

  
  elif withoutDNN == False:
      d_samp = {
    'ttbar'    :{'type':'bkg', 'leg':'ttbar',        'f_color':kGray+1,'l_color':0,  'path' :'ttbar_withDNN.root'},  # template dict element
    'Wenu'     :{'type':'bkg', 'leg':'Wenu',        'f_color':kRed+2, 'l_color':0,  'path' :'Wenu_withDNN.root'},  # template dict element
    'Wmunu'    :{'type':'bkg', 'leg':'Wmunu',        'f_color':kOrange,'l_color':0,  'path' :'Wmunu_withDNN.root'},  # template dict element
    'Wtaunu'   :{'type':'bkg', 'leg':'Wtaunu',        'f_color':kYellow,'l_color':0,  'path' :'Wtaunu_withDNN.root'},  # template dict element
    'Znunu'    :{'type':'bkg', 'leg':'Znunu',        'f_color':kRed,'l_color':0,  'path' :'Znunu_withDNN.root'},  # template dict element

    # Signals
    "151_150p5_150" :{"type":"sig","leg":"sig (151, 150.5, 150)","l_color":  kGreen,"path":"151_150p5_150_withDNN.root"},
    }

  return d_samp


