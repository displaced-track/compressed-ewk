#____________________________________________________________________________
def configure_vars(sig_reg):

  # format for each variable is
  #'var_name_in_ntuple':{TLaTeX axis entry, units, Nbins, xmin, xmax, arrow position (for N-1 plots), arrow direction}
  # to do variable bin widths, place 'var' as value of 'hXNbins' and specify lower bin edges as 'binsLowE':[0,4,5,11,15,20,40,60]
  # e.g.     'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':60,'binsLowE':[0,4,5,11,15,20,40,60],'cut_pos':200,'cut_dir':'left'}, 
  d_vars = {

    #---------------------------------
    #my saved variables
    #'DNN'        :{'tlatex':'DNN',    'units':'','hXNbins':20,'hXmin':0.,'hXmax':0.2,'cut_pos':230,'cut_dir':'right'},
    'DNN'        :{'tlatex':'DNN output score', 'units':'', 'hXNbins':'var', 'hXmin':0., 'hXmax':1., 'binsLowE':[0.,0.005,0.01,0.02,0.05,0.15], 'cut_pos':230, 'cut_dir':'right'},

    'TrackPt'    :{'tlatex':'Track p_{T}','units':'GeV','hXNbins':50,'hXmin':0.,'hXmax':5.,'cut_pos':230,'cut_dir':'right'},
    'TrackD0'    :{'tlatex':'Track d_{0}','units':'','hXNbins':50,'hXmin':0.,'hXmax':10.,'cut_pos':230,'cut_dir':'right'},

  }
  

  return d_vars
