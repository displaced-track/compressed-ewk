# Machine Learning 

# 2. Training and Testing
To run the training, just do
<pre> python script_to_run.py </pre>
This will execute the main function of the Train.py script.
The structure of Train.py code is the following:
1. read_samples() 
 This method will read the input tree samples, convert into a dataframe, label the signal tracks as 1 and the background tracks as 0, and separate the dataframe into two sets (namely, A and B) according to the event number.
2. train_A()
 This method will train a neural network based on the training set A. The classifier model will be saved as "DNN_A.h5".
3. train_B()
 This method will train a neural network based on the training set B. The classifier model will be saved as "DNN_B.h5".
4. test()
 This method will test the set A with the classifier model B, and it will test the set B with the classifier model A. The output dataframe is saved as "tested_df.pickle".
5. save_tree()
 This method will write the dataframe as output trees (one tree for each sample), ready to be analyzed by HistFitter.
