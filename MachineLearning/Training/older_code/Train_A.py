import uproot
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path

print('Using tf version {}'.format(tf.__version__))

path = '/storage/ballaben/samples_DisplacedTrack/c++/hadded_output/'
print('Using samples in {} : {}'.format(path, os.listdir(path)))


higgsino_151_150p5_150_file = uproot.open(path+'higgsino_151_150p5_150.root') 
Wenu_file = uproot.open(path+'Wenu.root') 
Wmunu_file = uproot.open(path+'Wmunu.root') 
Wtaunu_file = uproot.open(path+'Wtaunu.root') 
Znunu_file = uproot.open(path+'Znunu.root') 
ttbar_file = uproot.open(path+'ttbar.root') 


higgsino_151_150p5_150_file = higgsino_151_150p5_150_file['Tree']
Wenu_file = Wenu_file['Tree']
Wmunu_file = Wmunu_file['Tree']
Wtaunu_file = Wtaunu_file['Tree']
Znunu_file = Znunu_file['Tree']
ttbar_file = ttbar_file['Tree']

branches = higgsino_151_150p5_150_file.keys()


print('Reading Samples')

dictionary_151_150p5_150 = {}
dictionary_151_150p5_150['Sample'] = '151_150p5_150'
for branch in branches:
    dictionary_151_150p5_150[branch] = np.array(higgsino_151_150p5_150_file[branch])
df_151_150p5_150 = pd.DataFrame(dictionary_151_150p5_150)

dictionary_Wenu = {}
dictionary_Wenu['Sample'] = 'Wenu'
for branch in branches:
    dictionary_Wenu[branch] = np.array(Wenu_file[branch])
df_Wenu = pd.DataFrame(dictionary_Wenu)

dictionary_Wmunu = {}
dictionary_Wmunu['Sample'] = 'Wmunu'
for branch in branches:
    dictionary_Wmunu[branch] = np.array(Wmunu_file[branch])
df_Wmunu = pd.DataFrame(dictionary_Wmunu)

dictionary_Wtaunu = {}
dictionary_Wtaunu['Sample'] = 'Wtaunu'
for branch in branches:
    dictionary_Wtaunu[branch] = np.array(Wtaunu_file[branch])
df_Wtaunu = pd.DataFrame(dictionary_Wtaunu)

dictionary_Znunu = {}
dictionary_Znunu['Sample'] = 'Znunu'
for branch in branches:
    dictionary_Znunu[branch] = np.array(Znunu_file[branch])
df_Znunu = pd.DataFrame(dictionary_Znunu)

dictionary_ttbar = {}
dictionary_ttbar['Sample'] = 'ttbar'
for branch in branches:
    dictionary_ttbar[branch] = np.array(ttbar_file[branch])
df_ttbar = pd.DataFrame(dictionary_ttbar)


frames = [df_151_150p5_150, df_Wenu, df_Wmunu, df_Wtaunu, df_Znunu, df_ttbar]
df = pd.concat(frames)


df['Label'] = np.full( len(df.index), -1)
df['Label'][  df['TrackOrigin'] == -1 ] = 0  
df['Label'][ (df['TrackOrigin'] == 1)  | (df['TrackOrigin'] == 2) ] = 1
if (  len(df[df['Label'] == -1].index)  != 0 ):
        raise Exception('Errors in associating track labels')
if (  len(df[df['Label'] == -1].index)  == 0 ):
        print('All tracks have been labeled')
print('Displaced tracks from C1/N2 decays = {}, Background tracks = {}'.format(len(df[df['Label'] == 1].index), len(df[df['Label'] == 0].index)))


print('Dataframe')
print(df)



A = df[df['EventNumber']%2 == 0]
B = df[df['EventNumber']%2 == 1]

print('Displaced tracks from C1/N2 decays in A = {}, Background tracks in A = {}'.format(len(A[A['Label'] == 1].index), len(A[A['Label'] == 0].index)))
print('Displaced tracks from C1/N2 decays in B = {}, Background tracks in B = {}'.format(len(B[B['Label'] == 1].index), len(B[B['Label'] == 0].index)))


features = ["LeadingJetPt","DPhiJetMET","TrackPt","TrackD0","TrackD0Sig","TrackZ0SinThetha","TrackDeltaR","DPhiTrackMET"]

saved_model_A = '/storage/ballaben/samples_DisplacedTrack/DNN_A.h5'

import datetime
#os.system('rm -rf ./logs/') # Clear any logs from previous runs

from tensorflow import keras

if os.path.isfile(saved_model_A) is True:
    print ('model already exists')
    model_A = keras.models.load_model(saved_model_A)

if os.path.isfile(saved_model_A) is False:

    model_A = tf.keras.Sequential([
        
        tf.keras.layers.Flatten(),
        
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Dropout(rate=0.2), 
        tf.keras.layers.Dense(100, activation='relu'),
        
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Dropout(rate=0.2), 
        tf.keras.layers.Dense(100, activation='relu'),
        
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Dropout(rate=0.2), 
        tf.keras.layers.Dense(100, activation='relu'),
        
        tf.keras.layers.Dense(1, activation='sigmoid')
    ])

    model_A.compile(optimizer='adam', 
              #optimizer='sgd',
              loss='binary_crossentropy',
              #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    history = model_A.fit(A[features], A[['Label']], epochs=5, callbacks=[tensorboard_callback])
    model_A.save(saved_model_A)

print(model_B.summary())
print(model_B.history)


