# Machine Learning

# 0. Create a virtual environment
Performing machine learning studies tipically involves setting up a series of python packages. A virual environment avoids getting impacted into conflicts between different versions of python modules, and it allows to setup the same package versions every time is created. This procedure has been tested to work on CentOS 7.7.
<pre> virtualenv -p opt/python-3.8.6/bin/python3.8 mypython </pre>
<pre> source mypython/bin/activate </pre>
<pre> python3 -m pip install tensorflow==2.4.1
 python3 -m pip install pandas==1.2.3
 python3 -m pip install uproot==4.0.6
 python3 -m pip install uproot3==3.14.4
 python3 -m pip install matplotlib==3.3.4
 python3 -m pip install seaborn==0.11.1 </pre>

To activate the virtual environment (needed every time for using the scripts)
<pre> source mypython/bin/activate </pre>

Warning: it was seen that setting up ATLAS bindings or some LCG views breaks the importing python encodings. Avoid them.

# 1. Process the ntuples and save them as flat trees
The first step is processing the ntuples, apply the selections at the event and track levels, and save them as flat trees to be easily processed in the next step. Details on how to do it can be found in [AnalysisCode](../AnalysisCode/README.md).

# 2. Training and Testing
<pre> cd Training </pre>
To run the training, just do
<pre> python script_to_run.py </pre>
This will execute the main function of the Train.py script.
The structure of Train.py code is the following:
1. read_samples()
 This method will read the input tree samples, convert into a dataframe, label the signal tracks as 1 and the background tracks as 0, and separate the dataframe into two sets (namely, A and B) according to the event number.
2. train_A()
 This method will train a neural network based on the training set A. The classifier model will be saved as "DNN_A.h5".
3. train_B()
 This method will train a neural network based on the training set B. The classifier model will be saved as "DNN_B.h5".
4. test()
 This method will test the set A with the classifier model B, and it will test the set B with the classifier model A. The output dataframe is saved as "tested_df.pickle".
5. save_tree()
 This method will write the dataframe as output trees (one tree for each sample), ready to be analyzed by HistFitter.
