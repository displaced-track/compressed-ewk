# Simplified ATLAS SUSY analysis framework

Holds a collections of SUSY analyses. These can be run over samples in different
input formats:
*  DAOD_TRUTH      (TRUTH1 and TRUTH3 tested)
*  xAOD            (either truth-level and or reco-level - the latter with some constraints)
*  slimmed ntuples (Reduced ntuples produced from above input)

It provides the analysis acceptance per control and signal region as well as
optionally histograms or ntuples with event level objects. Smearing of the truth-level objects
can optionally be done using the latest upgrade performance smearing functions.

## Compiling:
It should compile out-of-the-box on top of any recent release

(tested with AnalysisBase,21.2.100, only fully works on Centos7) 
```
 setupATLAS
 lsetup git
 asetup AnalysisBase,21.2.100
 git clone --recursive https://:@gitlab.cern.ch:8443/atlas-phys-susy-wg/SimpleAnalysis.git
 cd SimpleAnalysis/

 #Compile
 mkdir build; mkdir run
 cd build
 #Without b-jet weighting:
 cmake ..
 #With b-jet weighting
 cmake -DDO_TRUTHTAGGING=ON ..

 make
 source x86_64-centos7-gcc8-opt/setup.sh
 cd ../run
```

## To run:  
```
simpleAnalysis [-a listOfAnalysis] <inputFile1> [inputFile2]...
```
This will run the analyses specified with "-a" option (or all if not given)
over all of the input files and provide acceptances in a text file for each
analysis ("analysisName.txt") and histograms in a root file ("analysisName.root").

The following additional commandline options that can be specified:
* _--nevents \<num\>_ limit the number of events processed
* _-l [--listanalysis]_ lists all available analyses
* _-n [ --ntuple]_ activates ntuple outputs
* _-o [--output] \<name\>_ merges the different analysis outputs into single text and root files
* _-s [--smear] \<args\>_ activates smearing. Use _"-s layout=run4,addPileupJets"_ for HL-LHC and _"-s layout=run2"_ for Run-2. Use _"-s help"_ for additional options
* _-r [--readReco]_ will read reco-level objects instead of truth-level. **Note this is still experimental**
* _-w [--mcweight] \<num\>_ chooses which event weight to use in case of multiple. Default is 0 and weighting can be disabled by setting it to -1
* _-S [--susyProcess] \<ranges\>_ consider certain SUSY processes, one can select these with "-S <ranges>", for example "-S 51-52,61-62" for stop and sbottom pair production
* _-P [--pdfVariations] \<initPDF\>_ do PDF variations following U.L. method of https://arxiv.org/abs/1206.2892
* _-T [--useTrueTau]_ use pT of true tau instead of visible pT (not recommended, but for backward compatibility)

### Outputs
SimpleAnalysis will produce a .txt and .root file, either one pair of files per analysis or a single pair if the "-o" option is used. In the latter case, 
everything gets prefixed with the analysis name to avoid name clashes.

The .txt file contains the number of events accepted, the acceptance and its error for each signal region in the analysis in the form of a comma-separated table. 
The number of events is the actual number of MC events accepted, while the acceptance is calculated taking into account event weights (unless disabled with option "-w -1").
Besides the analysis defined signal regions, there is an "_All" line which gives the total number of events processed, 
the sum of all event weights and the sum of the square of the event weight, in that order. This can be used for normalization and merging results.

The root file(s) will contain all histograms defined in the analysis. Note all histogram filling is done with the event weight. 
If the "-n" option is used, the root file will also contain one ntuple per analysis with all the variables defined in the analysis code. 
The ntuple has one entry per event and in case the analysis code filling a variable was not reached in a given event, the value will either be 0 or an empty vector.
This has to be accounted for in case 0 is a valid value for an ntuple variable.
Besides the analysis defined variables, the ntuple has an Event number (counting from 1) branch, an event weight branch and a branch for each signal region.
The latter will be 1 for events that passed that selection.

### Submission on the grid:
Submission to the grid is fairly straightforward:
```
lsetup panda
mkdir submit
cd submit
ln -s ../build/x86_64-centos7-gcc8-opt
prun --osMatching --exec 'source x86_64-centos7-gcc8-opt/setup.sh;simpleAnalysis -a ZeroLepton2015,ThreeBjets2015 %IN' --outputs='*.txt,*.root' --extFile \*.root --athenaTag 21.2.100,AnalysisBase --maxFileSize 40000000 --noCompile --followLinks --inDS <inputDS> --outDS user.<userName>.simple.v1
```

## Slimming:
For running over large input files more than once, it can be advantageous to
first slim the files to an absolute minimum by making an ntuple with all the
input objects. This can be done trivially with `slimMaker`. Minimum object pTs
can be specified on the commandline, see `slimMaker --help`. The output can
supplied to `simpleAnalysis` in the same way as DAODs and the program will
automatically detect the type of input.

## Adding a new analysis:
Simply make new C++ routine in `SimpleAnalysisCodes/src/MyAnalysisName.cxx` with the structure:
```
#include "SimpleAnalysisFramework/AnalysisClass.h"
DefineAnalysis(MyAnalysisName)
void MyAnalysisName::Init() {}
void MyAnalysis::ProcessEvent(AnalysisEvent *event)
```

Recompilation will automatically make the analysis available to run. See
`ExampleAnalysis.cxx` and the other supplied analysis examples on
how to specify signal regions, kinematic selections and filling
histograms/ntuples.

## Submission of new SUSY analyses

It is suggested to use analysis names that ends in the year of the latest used
data, i.e. "2015" or "2016", and to add "Conf" in front if it is a preliminary
analysis. Examples: `ZeroLepton2016.cxx` or `StopOneLeptonConf2018.cxx`

To submit either fork the main repository in gitlab, push changes to the private 
fork and submit a merge request or simply email the relevant code to @aagaard

For more help, please contact @aagaard
