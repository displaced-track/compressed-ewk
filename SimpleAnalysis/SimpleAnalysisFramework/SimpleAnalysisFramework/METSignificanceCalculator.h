#ifndef METSIGNIFICANCECALCULATOR_H
#define METSIGNIFICANCECALCULATOR_H

class AnalysisEvent;

double calcMETSignificance(AnalysisEvent *event);


#endif
