# Compressed EWK 2ndWave Analysis
SimpleAnalysis implementation to produce smeared truth samples from TRUTH1 derivation

# How to run the smearing
- Setup the AnalysisBase version SimpleAnalyis is compiled with (in branch SimpleAnalysis_AnalysisBase_21.2.149):
<pre> asetup AnalysisBase,21.2.149 </pre>
- Setup the executables, e.g. inside /storage/ballaben/compressed-ekw/SimpleAnalysis/run do:
<pre> source ../build/x86_64-centos7-gcc8-opt/setup.sh </pre>
- Run the smearing command
<pre> simpleAnalysis -a DisplacedTrack2018 /storage/ballaben/DAOD_TRUTH1.signal/DAOD_TRUTH1.signal.truth1.21.2.114.0.pool.root -s layout=run2 -n </pre>
where -a specifies the analysis, -s specifies thedetector layout for the smearing and -n is used to store the variables in the ntuple produced
