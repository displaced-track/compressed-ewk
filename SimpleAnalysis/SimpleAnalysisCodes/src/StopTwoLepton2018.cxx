#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StopTwoLepton2018)

void StopTwoLepton2018::Init()
{
  // 3-body CR/VR/SRs
  addRegions({"CR3BodyT", "VR3BodyT", "VR3BodyT2", "CR3BodyVV", "VR3BodyVV", "SR3BodyTSF", "SR3BodyTDF", "SR3BodyWSF", "SR3BodyWDF"});
  addHistogram("MET",100,0,2000);
  addHistogram("genMET",100,0,2000);
  addHistogram("METPreSelect",100,0,2000);
  addHistogram("METCR3BodyT",100,0,2000);
  addHistogram("METVR3BodyT",100,0,2000);
  addHistogram("METVR3BodyT2",100,0,2000);
  addHistogram("METCR3BodyVV",100,0,2000);
  addHistogram("METVR3BodyVV",100,0,2000);
  addHistogram("METSR3BodyTSF",100,0,2000);
  addHistogram("METSR3BodyTDF",100,0,2000);
  addHistogram("METSR3BodyWSF",100,0,2000);
  addHistogram("METSR3BodyWDF",100,0,2000);

  // 4-body SR
  addRegions({"preselection4body", "CR4BodyT", "VR4BodyT", "CR4BodyVV", "VR4BodyVV", "VR4bodyVV3L", "SRA4body", "SRB4body"});
  addHistogram("METPreSelect4body",100,0,2000);
  addHistogram("ptLep1PreSelect4body",100,0,100);
  addHistogram("ptLep2PreSelect4body",100,0,100);
  addHistogram("ptLep1_SRA",100,0,100);
  addHistogram("ptLep2_SRA",100,0,100);
  addHistogram("ptLep1_SRB",100,0,100);
  addHistogram("ptLep2_SRB",100,0,100);


  addRegions({"preselection2body","CR2BodyTOP","CR2BodyttZ","VR2BodyTOPDF","VR2BodyTOPSF", "SR2BodySF0", "SR2BodyDF0", "SR2BodySF1", "SR2BodyDF1", "SR2BodySF2", "SR2BodyDF2","SR2BodySF3", "SR2BodyDF3","SR2BodySF4", "SR2BodyDF4","SR2BodySF5", "SR2BodyDF5"});
  addRegions({"SR2BodyDisc0","SR2BodyDisc1","SR2BodyDisc2","SR2BodyDisc3","SR2BodyDisc4","SR2BodyDisc5","SR2BodyDisc6",});

  // Set up RestFrames trees through m_RF_Helper (RestFramesHelper class)
  //
  LabRecoFrame*       lab  = m_RF_helper.addLabFrame("lab");
  DecayRecoFrame*     ss   = m_RF_helper.addDecayFrame("ss");
  DecayRecoFrame*     s1   = m_RF_helper.addDecayFrame("s1");
  DecayRecoFrame*     s2   = m_RF_helper.addDecayFrame("s2");
  VisibleRecoFrame*   v1   = m_RF_helper.addVisibleFrame("v1");
  VisibleRecoFrame*   v2   = m_RF_helper.addVisibleFrame("v2");
  InvisibleRecoFrame* i1   = m_RF_helper.addInvisibleFrame("i1");
  InvisibleRecoFrame* i2   = m_RF_helper.addInvisibleFrame("i2");

  // Connect the frames
  lab->SetChildFrame(*ss);
  ss->AddChildFrame(*s1);
  ss->AddChildFrame(*s2);
  s1->AddChildFrame(*v1);
  s1->AddChildFrame(*i1);
  s2->AddChildFrame(*v2);
  s2->AddChildFrame(*i2);

  // Initialize the tree
  lab->InitializeTree();

  // Define groups
  InvisibleGroup* inv = m_RF_helper.addInvisibleGroup("inv");
  inv->AddFrame(*i1);
  inv->AddFrame(*i2);

  CombinatoricGroup* vis = m_RF_helper.addCombinatoricGroup("vis");
  vis->AddFrame(*v1);
  vis->SetNElementsForFrame(*v1, 1, false);
  vis->AddFrame(*v2);
  vis->SetNElementsForFrame(*v2, 1, false);

  InvisibleJigsaw* MinMassJigsaw = m_RF_helper.addInvisibleJigsaw("MinMassJigsaw", kSetMass);
  inv->AddJigsaw(*MinMassJigsaw);

  InvisibleJigsaw* RapidityJigsaw = m_RF_helper.addInvisibleJigsaw("RapidityJigsaw", kSetRapidity);
  inv->AddJigsaw(*RapidityJigsaw);
  RapidityJigsaw->AddVisibleFrames(lab->GetListVisibleFrames());

  InvisibleJigsaw* ContraBoostJigsaw = m_RF_helper.addInvisibleJigsaw("ContraBoostJigsaw", kContraBoost);
  inv->AddJigsaw(*ContraBoostJigsaw);
  ContraBoostJigsaw->AddVisibleFrames((s1->GetListVisibleFrames()), 0);
  ContraBoostJigsaw->AddVisibleFrames((s2->GetListVisibleFrames()), 1);
  ContraBoostJigsaw->AddInvisibleFrame(*i1, 0);
  ContraBoostJigsaw->AddInvisibleFrame(*i2, 1);

  MinMassesCombJigsaw* HemiJigsaw = m_RF_helper.addCombinatoricJigsaw("HemiJigsaw", kMinMasses);
  vis->AddJigsaw(*HemiJigsaw);
  HemiJigsaw->AddFrame(*v1, 0);
  HemiJigsaw->AddFrame(*v2, 1);

  // Initialize analysis
  lab->InitializeAnalysis();
}

void StopTwoLepton2018::ProcessEvent(AnalysisEvent *event)
{



  const float mZ = 91.2;

  auto baselineElectrons = event->getElectrons(4.5, 2.47, ELooseBLLH | EZ05mm);     //Jared: event->getElectrons(4.5, 2.47, ELooseBLLH|ED0Sigma5|EZ05mm);
  auto baselineMuons = event->getMuons(4., 2.7, MuMedium | MuZ05mm);
  auto jets          = event->getJets(20., 2.8, JVT120Jet); 
  auto metVec        = event->getMET();
  double metSignificance =  event->getMETSignificance(); 
  

  //Sliding delta-R cone for boosted lepton analyses
  auto radiusCalcEl = [] (const AnalysisObject& ele, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/ele.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };

  //bORaware for electrons
  AnalysisObjects jets_BORaware_OR;
  AnalysisObjects jets_BORaware_skipOR;
  for (const auto& jet : jets){
      if (!jet.pass(BTag85MV2c10)) jets_BORaware_OR.push_back(jet);  
      else{
        bool lowLepPt = true;
        for(const auto& ele : baselineElectrons){
           if (ele.Pt()>100.){
              lowLepPt=false;
              break;
           }
        }
        if(lowLepPt) jets_BORaware_skipOR.push_back(jet);
        else jets_BORaware_OR.push_back(jet);
      }
  }
  jets               = overlapRemoval(jets_BORaware_OR, baselineElectrons, 0.2); //bORaware 
  for (const auto& jet : jets_BORaware_skipOR){ jets.push_back(jet);};

  baselineElectrons  = overlapRemoval(baselineElectrons, jets, radiusCalcEl);
  jets               = overlapRemoval(jets, baselineMuons, 0.2, LessThan3Tracks);
  baselineMuons      = overlapRemoval(baselineMuons, jets, radiusCalcMuon);

  //UPTOHERE
  auto signalElectrons = filterObjects(baselineElectrons, 20., 2.47, EMediumLH | ED0Sigma5 | EIsoGradient);
  auto signalMuons     = filterObjects(baselineMuons, 20., 2.4, MuD0Sigma3 | MuIsoFCLoose); //2.4 needed for the trigger coverage

  auto signalElectrons_fourBody = filterObjects(baselineElectrons, 4.5, 2.47, EMediumLH | ED0Sigma5 | EIsoGradient);
  auto signalMuons_fourBody     = filterObjects(baselineMuons, 4, 2.7, MuD0Sigma3 | MuIsoFCLoose);

  auto bjets           = filterObjects(jets, 25., 2.5, BTag77MV2c10);
  jets            = filterObjects(jets, 25., 2.5);

  auto n_bjets          = countObjects(bjets, 25, 2.5);
  auto n_jets25         = countObjects(jets, 25, 2.5);

  AnalysisObjects signalLeptons = signalElectrons + signalMuons;
  AnalysisObjects signalLeptons_fourBody = signalElectrons_fourBody + signalMuons_fourBody;

  sortObjectsByPt(signalLeptons);
  sortObjectsByPt(signalLeptons_fourBody);
  sortObjectsByPt(jets);
  sortObjectsByPt(bjets);

  int nLeptons_fourBody = signalLeptons_fourBody.size();
  int nLeptons = signalLeptons.size();

  double gen_met   = event->getGenMET();
  if (event->getMCNumber() == 407345 && (gen_met < 200 || gen_met > 300)) {return;}
  if (event->getMCNumber() == 407346 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 407347 && (gen_met < 400)) {return;}
  if (event->getMCNumber() == 410472 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 410646 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 410647 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 410654 && (gen_met > 300)) {return;}
  if (event->getMCNumber() == 410655 && (gen_met > 300)) {return;}
  if (event->getMCNumber() == 411036 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 411037 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 411193 && (gen_met < 200 || gen_met > 300)) {return;}
  if (event->getMCNumber() == 411194 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 411195 && (gen_met < 400)) {return;}
  if (event->getMCNumber() == 411196 && (gen_met < 200 || gen_met > 300)) {return;}
  if (event->getMCNumber() == 411197 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 411198 && (gen_met < 400)) {return;}
  if (event->getMCNumber() == 411200 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 411201 && (gen_met < 400)) {return;}
  if (event->getMCNumber() == 411202 && (gen_met < 200 || gen_met > 300)) {return;}
  if (event->getMCNumber() == 411203 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 411204 && (gen_met < 400)) {return;}
  if (event->getMCNumber() == 410465 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 407351 && (gen_met < 200 || gen_met > 300)) {return;}
  if (event->getMCNumber() == 407352 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 407353 && (gen_met < 400)) {return;}
  if (event->getMCNumber() == 410558 && (gen_met > 200)) {return;}
  if (event->getMCNumber() == 407357 && (gen_met < 200 || gen_met > 300)) {return;}
  if (event->getMCNumber() == 407358 && (gen_met < 300 || gen_met > 400)) {return;}
  if (event->getMCNumber() == 407359 && (gen_met < 400)) {return;}

  double MET = metVec.Et();


  if (nLeptons_fourBody < 2 && nLeptons < 2) return;
  fill("MET", MET);
  fill("genMET", gen_met);
  // ntupVar("xsec", xsec);
  // ntupVar("ChannelNumber", mc_channel);
  ntupVar("genFiltMET", gen_met);
  ntupVar("MET", MET);
  ntupVar("METsig", metSignificance);
  ntupVar("nbjet", n_bjets);
  ntupVar("njet", n_jets25);
  ntupVar("nlep", nLeptons);
  ntupVar("nlep4b", nLeptons_fourBody);

  //neutrinofication for 4body
  auto GetLepNeut = [] (const AnalysisObjects& Objs, const AnalysisObject& METObj){

    std::map<TString,float> vars;
    vars["Mll_3Lcorr"] = -9999.;
    vars["MET_3Lcorr"] = -9999.;
    vars["pbll_3Lcorr"] = -9999.;
    vars["ptLep1_3Lcorr"] = -9999.;
    vars["ptLep2_3Lcorr"] = -9999.;
    vars["met_over_ptleps_3Lcorr"] = -9999.;

    if (Objs.size() != 3) return vars;
    else{

       std::vector<std::pair<int,int>> OSSFpair;
       for (unsigned int i=0;i<Objs.size();i++){
         for (unsigned int j=0;j<Objs.size();j++){
           if(i>j){
             if ((Objs[i].charge() != Objs[j].charge()) && (Objs[i].type() == Objs[j].type())){
                OSSFpair.push_back(std::make_pair(i,j)); 
             }
           }
         } 
       }
  
       //paired leptons 
       std::pair<unsigned int,unsigned int> Zpair; 
       if (OSSFpair.size() == 1) Zpair = OSSFpair[0];
       else if (OSSFpair.size() == 2){
          double m1 = (Objs[OSSFpair[0].first]+Objs[OSSFpair[0].second]).M();
          double m2 = (Objs[OSSFpair[1].first]+Objs[OSSFpair[1].second]).M();
          if(fabs(m1-91.2) < fabs(m2-91.2)) Zpair = OSSFpair[0];
          else Zpair = OSSFpair[1];
       }else return vars;

       //find not paired lepton
       int indx0 = -1;
       for (unsigned int i=0; i<Objs.size();i++){
           if (i != Zpair.first && i != Zpair.second){
               indx0 = i;
               break;
           }
       }

       
       //find lepton to be neutrinofied
       int indx1 = -1;
       int indxN = -1;
       if (Objs[indx0].charge()*Objs[Zpair.first].charge()<0){
          indx1 = Zpair.first;
          indxN = Zpair.second;
       }else{
          indx1 = Zpair.second;
          indxN = Zpair.first;
       }

       float m3L = (Objs[indx0]+Objs[indx1]).M();
       float MET3L = (Objs[indxN]+METObj).Pt();
       float pbll3L = (Objs[indx0]+Objs[indx1]+METObj).Pt();
       float ptLep13L = Objs[indx0].Pt()>Objs[indx1].Pt()? Objs[indx0].Pt():Objs[indx1].Pt();
       float ptLep23L = Objs[indx0].Pt()>Objs[indx1].Pt()? Objs[indx1].Pt():Objs[indx0].Pt();

       vars["Mll_3Lcorr"] = m3L;
       vars["MET_3Lcorr"] = MET3L;
       vars["pbll_3Lcorr"] = pbll3L;
       vars["ptLep1_3Lcorr"] = ptLep13L;
       vars["ptLep2_3Lcorr"] = ptLep23L;
       vars["met_over_ptleps_3Lcorr"] = MET3L/(ptLep13L+ptLep23L);
         
       
    }

    return vars;   
 
  }; 


  if ( nLeptons_fourBody == 2 || nLeptons_fourBody == 3) {

    auto lep0 = signalLeptons_fourBody[0];
    auto lep1 = signalLeptons_fourBody[1];
    ntupVar("mll_4b", (lep0 + lep1).M());
    int nLep = 2;
    if (nLeptons_fourBody == 3)nLep = 3;

    
    // Variables we'll be cutting on
    double ptJet1 = 0., ptJet2 = 0.;
    bool isB1 = false;
    if ( n_jets25 >= 1){
        ptJet1 = jets[0].Pt();
        isB1 =jets[0].pass(BTag77MV2c10);
    }
    if ( n_jets25 >= 2) ptJet2 = jets[1].Pt();

    double mll                            = (lep0 + lep1).M();
    auto pbll_TLV                         = metVec + lep0 + lep1;
    double meff                           = sumObjectsPt(jets, 4, 25) + lep0.Pt() + lep1.Pt() + MET;
    auto pll                              = lep0 + lep1;

    double R2l                            = MET / pll.Pt();
    double R2l4j                          = MET / meff;
    //double dPhiEtmisspbll                 = fabs(metVec.DeltaPhi(pbll_TLV));
    double minDRL2Jets                    = AnalysisClass::minDR(lep1,jets);
 
    bool isSF = false;
    if (lep0.type() == lep1.type()) isSF = true;

    //Opposite Sign leptons
    bool isSS = false;
    if (lep0.charge() == lep1.charge()) isSS = true;

    bool isOS = false;
    if (lep0.charge() != lep1.charge())
      isOS = true;

    ntupVar("isSF_4b", isSF);
    ntupVar("isOS_4b", isOS);
    ntupVar("isSS_4b", isSS);


    bool isSS3L = false;
    std::map<TString,float> vars3L;
    if (nLeptons_fourBody == 3){if(lep0.charge() == lep1.charge() && lep1.charge() == signalLeptons_fourBody[2].charge()) isSS3L = true;}
    vars3L = GetLepNeut(signalLeptons_fourBody,metVec);
    double Mll3Lcorr = vars3L["Mll_3Lcorr"];
    double MET3Lcorr = vars3L["MET_3Lcorr"];
    double R2L3Lcorr = vars3L["met_over_ptleps_3Lcorr"];
   


    //SR4Body
    bool ZVeto = (!isSF || (isSF && fabs(mll-91.2)>10.));
    bool preselection = (nLep==2 && !isSS && lep0.Pt()<100. && lep1.Pt()<100. && mll>10. && MET>250. && ptJet1>150. && minDRL2Jets>1.);
    bool CRtop_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && n_bjets>1 && MET>350. && R2l<5);
    bool VRtop_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && isB1 && R2l>5 && R2l4j>0.3 && R2l4j<0.38);
    bool CRVV_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && mll>45. && ZVeto && (( n_jets25==1 && n_bjets==0)||(n_jets25==2 && n_bjets==0 && ptJet2<40.)) && R2l<4.);
    bool VRVV_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<50. && mll>45. && ZVeto && ( (n_jets25==1 && n_bjets==0) || (n_jets25==2 && n_bjets==0) || (n_jets25==3 && n_bjets==0) || (n_jets25==4 && n_bjets==0) ) && R2l>4 && R2l<5);
    bool VRVV3L_4body = false;
    if (nLep==3) VRVV3L_4body = (nLep==3 && !isSS3L && lep0.Pt()<100. && lep1.Pt()<100. && mll>10. && MET>250. && ptJet1>150. && Mll3Lcorr>10. && MET3Lcorr>300. && metSignificance > 5 && R2L3Lcorr>5 && (n_jets25<5 && n_bjets==0) && minDRL2Jets>1.);
    bool SRA_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()<10. && lep0.Pt()<25. && R2l>25. && R2l4j>0.44 && MET>400.);
    bool SRB_4body = (preselection && pbll_TLV.Pt()>280. && metSignificance>10. && lep1.Pt()>10. && lep1.Pt()<50. && R2l>13. && R2l4j>0.38 && MET>400.);

    ntupVar("Mll3Lcorr", Mll3Lcorr);
    ntupVar("MET3Lcorr", MET3Lcorr);
    ntupVar("R2L3Lcorr", R2L3Lcorr);
    ntupVar("isSS3L", isSS3L);
    ntupVar("lep1_4b", lep0.Pt());
    ntupVar("lep2_4b", lep1.Pt());
    ntupVar("ptJet1_4b", ptJet1);
    ntupVar("ptJet2_4b", ptJet2);
    ntupVar("R2l_4b", R2l);
    ntupVar("R2l4j_4b", R2l4j);
    ntupVar("isB1_4b", isB1);
    ntupVar("ZVeto_4b", ZVeto);
    ntupVar("pbll_TLV_4b", pbll_TLV.Pt());
    ntupVar("minDRL2Jets", minDRL2Jets);

    if(preselection){
       accept("preselection4body");
       fill("METPreSelect4body", MET);
       fill("ptLep1PreSelect4body", lep0.Pt());
       fill("ptLep2PreSelect4body", lep1.Pt());
    }
    if(CRtop_4body) accept("CR4BodyT");
    if(VRtop_4body) accept("VR4BodyT");
    if(CRVV_4body) accept("CR4BodyVV");
    if(VRVV_4body) accept("VR4BodyVV");
    if(VRVV3L_4body) accept("VR4bodyVV3L");
    if(SRA_4body){ 
      accept("SRA4body");
      fill("ptLep1_SRA", lep0.Pt());
      fill("ptLep2_SRA", lep1.Pt());
    }
    if(SRB_4body){
      accept("SRB4body");
      fill("ptLep1_SRB", lep0.Pt());
      fill("ptLep2_SRB", lep1.Pt());
    }

  }

  if (nLeptons == 2) {

    auto lep0 = signalLeptons[0];
    auto lep1 = signalLeptons[1];

    // Variables we'll be cutting on
    auto pbll_TLV                         = metVec + lep0 + lep1;
    auto pll                              = lep0 + lep1;
    double MT2                            = calcMT2(lep0, lep1, metVec);;
    double dPhiEtmisspbll                 = fabs(metVec.DeltaPhi(pbll_TLV));
    double mll                            = (lep0 + lep1).M();
    bool isSF = false;
    bool isOS = false;

    if (lep0.type() == lep1.type()) isSF = true;
    if (lep0.charge() != lep1.charge()) isOS = true;
    //Opposite Sign leptons
    if (lep0.charge() == lep1.charge()) return;

    //Leading lepton with pT>25 GeV
    if (lep0.Pt() < 25.) return;
    //SubLeading lepton with pT>25 GeV
    if (lep1.Pt() < 20.) return;

    //Reject low mass DY etc
    if (mll < 20.) return;
    ntupVar("mll", mll);
    ntupVar("MT2", MT2);
    ntupVar("DPhib", dPhiEtmisspbll);
    ntupVar("isSF", isSF);
    ntupVar("isOS", isOS);
    //
    // Here BEGINs the 3-Body SRs
    //

    // Get the RJ tree
    LabRecoFrame* lab      = m_RF_helper.getLabFrame("lab");
    DecayRecoFrame* ss     = m_RF_helper.getDecayFrame("ss");
    DecayRecoFrame* s1     = m_RF_helper.getDecayFrame("s1");
    InvisibleGroup* inv    = m_RF_helper.getInvisibleGroup("inv");
    CombinatoricGroup* vis = m_RF_helper.getCombinatoricGroup("vis");
    VisibleRecoFrame* v1   = m_RF_helper.getVisibleFrame("v1");

    // Clear the event
    lab->ClearEvent();

    // Set MET
    inv->SetLabFrameThreeVector(metVec.Vect());

    // Add leptons to the visible group
    vis->AddLabFrameFourVector(lep0);
    vis->AddLabFrameFourVector(lep1);

    // Analyze the event
    lab->AnalyzeEvent();

    // Get the variables

    // RPT
    double shat_jigsaw = ss->GetMass();
    TVector3 vPTT      = (ss->GetFourVector(*lab)).Vect();
    double RPT         = vPTT.Pt() / (vPTT.Pt() + shat_jigsaw / 4.);

    // MDR
    double MDR         = 2.0 * v1->GetEnergy(*s1);

    // gamInvRp1
    double gamInvRp1   = ss->GetVisibleShape();

    // DPB_vSS
    double DPB_vSS     = ss->GetDeltaPhiBoostVisible();

    // cosTheta_b
    auto lepPos = lep0, lepNeg = lep1;
    if (signalLeptons[0].charge() < 0.) {
      lepPos = lep1;
      lepNeg = lep0;
    }

    TVector3 boost = pll.BoostVector();
    lepPos.Boost(-boost);
    lepNeg.Boost(-boost);

    //double cosTheta_b = tanh((lepPos.Eta() - lepNeg.Eta()) / 2);

    fill("METPreSelect", MET);

    ntupVar("MDR", MDR);
    ntupVar("DPB_vSS", DPB_vSS);
    // ntupVar("cosTheta_b", cosTheta_b);
    ntupVar("gamInvRp1", gamInvRp1);
    ntupVar("RPT", RPT);
    // Apply selection

    //CR3Body
    if ( !isSF && n_bjets > 1 && MDR > 80. && DPB_vSS < 2.3 && gamInvRp1 > 0.7 && metSignificance > 10. ) {
      accept("CR3BodyT");
      fill("METCR3BodyT", MET);
    }
    if ( ((isSF && fabs(mll - mZ)>20.) || !isSF) && n_bjets == 0 && MDR > 100. && RPT > 0.3 && gamInvRp1 > 0.7 && DPB_vSS < 2.3 && metSignificance > 10 ) {
      accept("CR3BodyVV");
      fill("METCR3BodyVV", MET);
    }
    //VR3Body
    if ( !isSF && n_bjets == 0 && MDR > 80 && MDR < 105 && RPT > 0.7 && gamInvRp1 > 0.7 && metSignificance > 12. && DPB_vSS > 2.3 ) {
      accept("VR3BodyT");
      fill("METVR3BodyT", MET);
    }
    if ( !isSF && n_bjets > 0 && MDR > 80 && MDR < 120 && RPT > 0.7 && gamInvRp1 > 0.7 && metSignificance > 12. && DPB_vSS > 2.3 ) {
      accept("VR3BodyT2");
      fill("METVR3BodyT2", MET);
    }
    if ( ((isSF && fabs(mll - mZ)>20) || !isSF) && n_bjets == 0 && MDR > 100. && RPT > 0.7 && gamInvRp1 > 0.45 && gamInvRp1 < 0.7 && metSignificance > 12. && DPB_vSS > 2.3 ) {
      accept("VR3BodyVV");
      fill("METVR3BodyVV", MET);
    }
    // SR3Body W SF
    if (isSF  && RPT > 0.78 && gamInvRp1 > 0.7 && DPB_vSS > 2.3 && MDR > 105. && n_bjets == 0 && fabs(mll - mZ) > 20. && metSignificance > 12. ) {
      accept("SR3BodyWSF");
      fill("METSR3BodyWSF", MET);
    }
    // SR3Body W DF
    if (!isSF  && RPT > 0.78 && gamInvRp1 > 0.7 && DPB_vSS > 2.3 && MDR > 105. && n_bjets == 0 && metSignificance > 12. ) {
      accept("SR3BodyWDF");
      fill("METSR3BodyWDF", MET);
    }
    // SR3Body T SF
    if (isSF  && RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > 2.3 && MDR > 120. && n_bjets >= 1 && fabs(mll - mZ) > 20. && metSignificance > 12. ) {
      accept("SR3BodyTSF");
      fill("METSR3BodyTSF", MET);
    }
    // SR3Body T DF
    if (!isSF  && RPT > 0.7 && gamInvRp1 > 0.7 && DPB_vSS > 2.3 && MDR > 120. && n_bjets >= 1 && metSignificance > 12. ) {
      accept("SR3BodyTDF");
      fill("METSR3BodyTDF", MET);
    }

    //
    // Here ENDs the 3-Body SRs
    //

    //
    // Here STARTs the 2-Body and DM SRs
    //
    if (MT2 < 100.) return;
  
    if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 110. && MT2 < 120. && metSignificance > 12.)  accept("SR2BodySF0");
    if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 110. && MT2 < 120. && metSignificance > 12.)  accept("SR2BodyDF0");
    if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 120. && MT2 < 140. && metSignificance > 12.)  accept("SR2BodySF1");
    if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 120. && MT2 < 140. && metSignificance > 12.)  accept("SR2BodyDF1");
    if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 140. && MT2 < 160. && metSignificance > 12.)  accept("SR2BodySF2");
    if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 140. && MT2 < 160. && metSignificance > 12.)  accept("SR2BodyDF2");
    if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 160. && MT2 < 180. && metSignificance > 12.)  accept("SR2BodySF3");
    if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 160. && MT2 < 180. && metSignificance > 12.)  accept("SR2BodyDF3");
    if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 180. && MT2 < 220. && metSignificance > 12.)  accept("SR2BodySF4");
    if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 180. && MT2 < 220. && metSignificance > 12.)  accept("SR2BodyDF4");
    if (n_bjets > 0 && isSF  && fabs(mll - mZ) > 20.  && dPhiEtmisspbll < 1.5 && MT2 > 220.               && metSignificance > 12.)  accept("SR2BodySF5");
    if (n_bjets > 0 && !isSF                          && dPhiEtmisspbll < 1.5 && MT2 > 220.               && metSignificance > 12.)  accept("SR2BodyDF5");

    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 220. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc6");
    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 200. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc5");
    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 180. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc4");
    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 160. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc3");
    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 140. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc2");
    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 120. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc1");
    if(n_bjets > 0 && ((isSF  && fabs(mll - mZ) > 20.) || !isSF) && metSignificance>12 && MT2 > 110. && dPhiEtmisspbll < 1.5) accept("SR2BodyDisc0");

    if (n_bjets > 0 && !isSF && dPhiEtmisspbll > 1.5 && MT2 > 100. && MT2 < 120. && metSignificance > 8)  accept("CR2BodyTOP");
    if (n_bjets > 0 && !isSF && dPhiEtmisspbll < 1.5 && MT2 > 100. && MT2 < 110. && metSignificance > 12) accept("VR2BodyTOPDF");
    if (n_bjets > 0 && isSF && dPhiEtmisspbll <1.5 && MT2 > 100. && MT2 < 110. && metSignificance > 12)   accept("VR2BodyTOPSF");


  }

  float mll_12 = -1;
  float mll_23 = -1;
  float mll_13 = -1;
  float MET_corr = -1;
  float MET_corrphi = -1;

  float closest_mll = -1.;
  if (nLeptons == 3)
  {

    // std::cout << "nlep == 3" << std::endl;
    auto lepZ1 = signalLeptons[0];
    auto lepZ2 = signalLeptons[1];
    auto lepW = signalLeptons[2];

    auto lep0 = signalLeptons[0];
    auto lep1 = signalLeptons[1];
    auto lep2 = signalLeptons[2];

    if (lep0.Pt() > 25. && lep1.Pt() > 20. && lep2.Pt() > 20. && nLeptons == 3)
    {
      if ((lep0.type() == lep1.type()) && (lep0.charge() != lep1.charge()))
      {
        mll_12 = (lep0 + lep1).M();
        closest_mll = mll_12;
        lepZ1 = lep0;
        lepZ2 = lep1;
        lepW = lep2;
        // accept("preselection3lep2body");
      }
      if ((lep1.type() == lep2.type()) && (lep1.charge() != lep2.charge()))
      {
        mll_23 = (lep1 + lep2).M();
        if (mll_12 != -1)
        {
          if (fabs(mll_23 - mZ) < fabs(closest_mll - mZ))
          {closest_mll = mll_23;
            lepZ1 = lep1;
            lepZ2 = lep2;
            lepW = lep0;}
        }
        else{
          closest_mll = mll_23;
          lepZ1 = lep1;
          lepZ2 = lep2;
          lepW = lep0;}
      }
      if ((lep0.type() == lep2.type()) && (lep0.charge() != lep2.charge()))
      {
        mll_13 = (lep0 + lep2).M();
        if (mll_12 != -1 || mll_23 != -1)
        {if (fabs(mll_13 - mZ) < fabs(closest_mll - mZ)){
            closest_mll = mll_13;
            lepZ1 = lep0;
            lepZ2 = lep2;
            lepW = lep1;}}
        else
        { closest_mll = mll_13;
          lepZ1 = lep0;
          lepZ2 = lep2;
          lepW = lep1;}}
      MET_corr = (metVec + lepZ1 + lepZ2).Pt();
      MET_corrphi = (metVec + lepZ1 + lepZ2).Phi();
    }
  if (nLeptons==3 && n_bjets>1 && n_jets25>2 && closest_mll<111. && closest_mll>71. && MET_corr>140.) accept("CR2BodyttZ");
  }
  ntupVar("closest_mll", closest_mll);
  ntupVar("MET_corr", MET_corr);
  ntupVar("MET_corrphi", MET_corrphi);

  return;
}
