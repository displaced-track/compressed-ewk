#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <unordered_set>
#include <algorithm>
#include <iostream>
using namespace std;

//---------------------------------------------------------
//  
//  SUSY EWK Compressed 'Higgsino' Analysis 2nd Wave
//  
//
//---------------------------------------------------------

DefineAnalysis(DisplacedTrack2018)

void DisplacedTrack2018::Init()
{

  //Electroweakino exclusive SRs
  addRegions( {"SR_dummy"} );

  //addHistogram("Met",20,0,2000);

  //---------------------------------------------------------
  //
  //                   Intitialize RJR
  //
  //---------------------------------------------------------

  LabRecoFrame* LAB        = m_RF_helper.addLabFrame("LAB");
  DecayRecoFrame* CM       = m_RF_helper.addDecayFrame("CM");
  DecayRecoFrame* S        = m_RF_helper.addDecayFrame("S");
  VisibleRecoFrame* ISR    = m_RF_helper.addVisibleFrame("ISR");
  VisibleRecoFrame* V      = m_RF_helper.addVisibleFrame("V");
  VisibleRecoFrame* L      = m_RF_helper.addVisibleFrame("L");
  InvisibleRecoFrame* I    = m_RF_helper.addInvisibleFrame("I");

  InvisibleGroup* INV      = m_RF_helper.addInvisibleGroup("INV");

  // set the invisible system mass to zero
  InvisibleJigsaw* InvMass = m_RF_helper.addInvisibleJigsaw("InvMass", kSetMass);
  //kSetMass does InvisibleJigsaw = new SetMassInvJigsaw("InvMass","InvMass");

  CombinatoricGroup* VIS   = m_RF_helper.addCombinatoricGroup("VIS");

  // define the rule for partitioning objects between "ISR" and "V"
  MinMassesCombJigsaw* SplitVis = m_RF_helper.addCombinatoricJigsaw("SplitVis", kMinMasses);
  //kMinMasses does MinMassesCombJigsaw = new MinMassesCombJigsaw(SplitVis,SplitVis);

  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*ISR);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*V);
  S->AddChildFrame(*I);
  S->AddChildFrame(*L);

  LAB->InitializeTree();

  INV->AddFrame(*I);

  VIS->AddFrame(*ISR);
  VIS->SetNElementsForFrame(*ISR,1,false);
  VIS->AddFrame(*V);
  VIS->SetNElementsForFrame(*V,0,false);

  INV->AddJigsaw(*InvMass);

  VIS->AddJigsaw(*SplitVis);
  // "0" group (ISR)
  SplitVis->AddFrame(*ISR, 0);
  // "1" group (V + I + L)
  SplitVis->AddFrame(*V,1);
  SplitVis->AddFrame(*I,1);
  SplitVis->AddFrame(*L,1);

  LAB->InitializeAnalysis();

}

//---------------------------------------------------------

//---------------------------------------------------------  

void DisplacedTrack2018::ProcessEvent(AnalysisEvent *event)
{
  //---------------------------------------------------------
  //
  //                   Prepare objects
  //
  //---------------------------------------------------------

  auto baseElectrons  = event->getElectrons(4.5, 2.47, EVeryLooseLH); // pT cut, eta cut, isolation
  auto baseMuons      = event->getMuons(3, 2.5, MuMedium); //actually lowPt
  auto preJets        = event->getJets(20., 4.5); 
  auto metVec         = event->getMET();
  float met = metVec.Pt();

  // Reject events with bad jets
 // if (countObjects(preJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  // hard lepton overlap removal and signal lepton/jet definitions
  auto baseJets        = overlapRemoval(preJets,       baseElectrons, 0.2, NOT(BTag85MV2c10));
       baseElectrons   = overlapRemoval(baseElectrons, baseJets,      0.4);
       baseElectrons   = overlapRemoval(baseElectrons, baseMuons,     0.01);
       baseJets        = overlapRemoval(baseJets,      baseMuons,     0.4, LessThan3Tracks);
       baseMuons       = overlapRemoval(baseMuons,     baseJets, 0.4);
  auto signalJets      = filterObjects( baseJets,      30,  2.8,  JVT50Jet);
  auto signalBJets     = filterObjects( baseJets,      20,  2.5,  BTag85MV2c10);
  auto signalElectrons = filterObjects( baseElectrons, 4.5, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradient);
  auto signalMuons     = filterObjects( baseMuons,     3,   2.5,  MuD0Sigma3|MuZ05mm|MuIsoFixedCutTightTrackOnly);

  AnalysisObjects baseLeptons   = baseElectrons+baseMuons;
  AnalysisObjects signalLeptons = signalElectrons+signalMuons;

  //get tracks within R < 0.01 of reconstructed electron or muon candidate
  auto preElTracks   = event->getElectrons(0.5, 2.5);
  auto preMuTracks   = event->getMuons(0.5, 2.5);

  // reconstructed electron or muon candidate must fail signal lepton criteria
  AnalysisObjects baseElTracks;
  for (const auto& preElTrack : preElTracks) {
    bool match = false;
    for (const auto& signalElectron : signalElectrons) {
      if (preElTrack == signalElectron) {
        match = true;
        break;
      }
    }
    if (!match) baseElTracks.push_back(preElTrack);
  }

  AnalysisObjects baseMuTracks;
  for (const auto& preMuTrack : preMuTracks) {
    bool match = false;
    for (const auto& signalMuon : signalMuons) {
      if (preMuTrack == signalMuon) {
        match = true;
        break;
      }
    }
    if (!match) baseMuTracks.push_back(preMuTrack);
  }

  //baseline tracks do not have signal leptons
  AnalysisObjects baseTracks   = baseElTracks+baseMuTracks;

  // Tracks are required to be separated from preselected jets by at least dR > 0.5
  baseElTracks = overlapRemoval(baseElTracks, baseJets, 0.5);
  baseMuTracks = overlapRemoval(baseMuTracks, baseJets, 0.5);

  // sum pT of preselected tracks within dR < 0.3 of signal tracks is required to be smaller than 0.5 GeV (basically means nothing within 0.3 of the track)
  auto signalElTracks = overlapRemoval(baseElTracks, baseTracks, 0.3);
  auto signalMuTracks = overlapRemoval(baseMuTracks, baseTracks, 0.3);
    
  // tracks must pass pT, z0 and d0 requirements
  signalElTracks = filterObjects(signalElTracks, 1, 2.5, ED0Sigma5|EZ05mm);
  signalMuTracks = filterObjects(signalMuTracks, 1, 2.5, MuD0Sigma3|MuZ05mm);

  AnalysisObjects signalTracks = signalElTracks+signalMuTracks;

  // Preselect at least 1 baseline lepton
  // Final selection requires exactly 1 or exactly 2 signal leptons
  //if ( !(baseLeptons.size() >=1) ) return;

  // preselect at least one signal jet because this is an ISR analysis
  // also RJR doesn't work without this...
  //if ( !(signalJets.size() >= 1) ) return;
  
  if (baseLeptons.size() != 0) {cout << "baseLeptons.size() = " << baseLeptons.size() << ", signalJets.size() = " << signalJets.size() << endl;}

  //---------------------------------------------------------
  //
  //                Calculate RJR variables
  //
  //---------------------------------------------------------

/*

  // analyze event in RestFrames tree
  // Combinatoric setup for jets
  LabRecoFrame* LAB = m_RF_helper.getLabFrame("LAB");
  CombinatoricGroup* VIS = m_RF_helper.getCombinatoricGroup("VIS");
  InvisibleGroup* INV = m_RF_helper.getInvisibleGroup("INV");
  DecayRecoFrame* CM = m_RF_helper.getDecayFrame("CM");
  DecayRecoFrame* S = m_RF_helper.getDecayFrame("S");
  VisibleRecoFrame* ISR = m_RF_helper.getVisibleFrame("ISR");
  VisibleRecoFrame* V = m_RF_helper.getVisibleFrame("V");
  VisibleRecoFrame* L = m_RF_helper.getVisibleFrame("L");
  InvisibleRecoFrame* I = m_RF_helper.getInvisibleFrame("I");

  LAB->ClearEvent();

  std::vector<RFKey> jetID;
  for (const auto& jet : signalJets){
    jetID.push_back(VIS->AddLabFrameFourVector(jet.transFourVect()));
  } 

  TLorentzVector lepSys(0.,0.,0.,0.);
  for(const auto lep1 : signalLeptons){
    lepSys = lepSys + lep1.transFourVect();
  }
  L->SetLabFrameFourVector(lepSys);

  INV->SetLabFrameThreeVector(metVec.Vect());
  if (!LAB->AnalyzeEvent()) std::cout << "Something went wrong..." << std::endl;

  // Compressed variables from tree
  int Njet_V = 0;
  int Njet_ISR = 0;  
  for (unsigned int i = 0; i < signalJets.size(); ++i){
    if (VIS->GetFrame(jetID[i]) == *V) {
      Njet_V++;
    } else {
      Njet_ISR++;
    }
  }

  //RJR variables
  float Pt_ISR = 0.0; 
  float R_ISR = 0.0;
  float M_S = 0.0;
  float M_ISR = 0.0;
  float DPhi_I_ISR = 0.0;

  TVector3 v_P_ISR = ISR->GetFourVector(*CM).Vect();
  TVector3 v_P_I   = I->GetFourVector(*CM).Vect();
  Pt_ISR = v_P_ISR.Mag();
  R_ISR = fabs(v_P_I.Dot(v_P_ISR.Unit())) / Pt_ISR;
  M_S = S->GetMass();
  M_ISR = ISR->GetMass();
  DPhi_I_ISR = fabs(v_P_ISR.DeltaPhi(v_P_I));



  //---------------------------------------------------------
  //
  //                Calculate other variables
  //
  //---------------------------------------------------------

  // Events must have ==2 signal leptons or ==1 signal lepton plus >=1 track
  bool is2L  = (signalLeptons.size() == 2);
  bool is1LT = (signalLeptons.size() == 1 && signalTracks.size()>=1); 
  cout << "is2L = " << is2L << ", is 1LT = " << is1LT << endl;
  //if ( !(is2L || is1LT) ) return;

  auto lep1 = signalLeptons[0];
  auto lep2 = is2L ? signalLeptons[1] : signalTracks[0];
  auto lep = lep1+lep2;
  
  float jet1Pt    = signalJets.size() >= 1 ? signalJets[0].Pt() : -1.;
  float dPhiJ1Met = signalJets.size() >= 1 ? fabs( signalJets[0].DeltaPhi( metVec ) ) : -1.; 

  // Minimum of the delta phi between all jets with MET
  //float minDPhiAllJetsMet = minDphi(metVec,signalJets);

  float lep1Pt = lep1.Pt();
  float lep2Pt = lep2.Pt();

  float dPhiLepMet = fabs( lep.DeltaPhi( metVec ) );

  // SFOS decision
  bool isOS = ( lep1.charge() != lep2.charge() ); // opposite charge
  bool isSF = ( lep1.type() == lep2.type() ) ; // same flavour
  bool isee = ( lep1.type() == ELECTRON && lep2.type() == ELECTRON ); //di-electron
  bool ismm = ( lep1.type() == MUON && lep2.type() == MUON ); //di-muon

  // Higher level leptonic variables
  float mll          = (lep1+lep2).M();
  float drll         = lep1.DeltaR(lep2);
  float metOverHtLep = met / (lep1Pt + lep2Pt); 
  float MTauTau      = calcMTauTau(lep1, lep2, metVec);
  float mt           = calcMT( lep1,metVec );
  
  // mT2 with trial invisible particle mass at 100 GeV
  float mn = 100.; 
  //  float mt2 = calc_massive_MT2(baseLeptons[0], baseLeptons[1], metVec, mn);   
  float mt2=calcAMT2(lep1,lep2,metVec,mn,mn);
*/
  // SUSYTools final state (fs) code
  int fs = event->getSUSYChannel();
  // stau veto for slepton signal - note this does not affect the denominator in the acceptance numbers (not needed in R21)
  //  bool isNotStau = ( fs != 206 && fs != 207 );

  int mcChannel = event->getMCNumber();
  //bool is1LTsample = (mcChannel >= 396160 && mcChannel <= 396205) || (mcChannel >= 397091 && mcChannel <= 397099);

  //---------------------------------------------------------
  //
  //                Perform selection
  //
  //---------------------------------------------------------

  if (true) accept("SR_dummy");  // why this saves a non-flat distribution?

  // Fill in optional ntuple variables
  ntupVar("FS",                fs);   
  ntupVar("DSID",              mcChannel);
  ntupVar("baseTracks",              baseTracks,true,true,true,true);
  ntupVar("signalTracks",          signalTracks,true,true,true,true);
/*            
  ntupVar("mc_weight",  event->getMCWeights()[0]);  
  ntupVar("susyChannel",event->getSUSYChannel());
  ntupVar("mcWeights",event->getMCWeights());
  ntupVar("pdf_id1",event->getPDF_id1());
  ntupVar("pdf_x1",event->getPDF_x1());
  ntupVar("pdf_pdf1",event->getPDF_pdf1());
  ntupVar("pdf_id2",event->getPDF_id2());
  ntupVar("pdf_x2",event->getPDF_x2());
  ntupVar("pdf_pdf2",event->getPDF_pdf2());
  ntupVar("pdf_scale",event->getPDF_scale());
  ntupVar("met_Et",            met);               
  ntupVar("nLep_base",         static_cast<int>(baseLeptons.size()));               
  ntupVar("nLep_signal",       static_cast<int>(signalLeptons.size()));
  ntupVar("nEle_signal",       static_cast<int>(signalElectrons.size()));
  ntupVar("nMu_signal",        static_cast<int>(signalMuons.size()));         
  ntupVar("nBJet20_MV2c10",    static_cast<int>(signalBJets.size()));
  ntupVar("nJet30",            static_cast<int>(signalJets.size()));
  ntupVar("nTrk_signal",       static_cast<int>(signalTracks.size()));
  ntupVar("isOS",              isOS);               
  ntupVar("isSF",              isSF);
  ntupVar("isee",              isee);               
  ntupVar("ismm",              ismm);
  ntupVar("is2L",              is2L);
  ntupVar("is1LT",             is1LT);
  //ntupVar("is1LTsample",       is1LTsample);               
  ntupVar("DPhiJ1Met",         dPhiJ1Met);               
  ntupVar("lep1Pt",            lep1Pt);               
  ntupVar("lep2Pt",            lep2Pt);               
  ntupVar("jet1Pt",            jet1Pt);               
  ntupVar("mll",               mll);               
  ntupVar("Rll",               drll);               
  ntupVar("mt_lep1",           mt);               
  ntupVar("mt2leplsp_100",     mt2);               
  ntupVar("MTauTau",           MTauTau);               
  ntupVar("METOverHTLep",      metOverHtLep);               
  //ntupVar("minDPhiAllJetsMet", minDPhiAllJetsMet);
  ntupVar("dPhiLepMet",        dPhiLepMet);
  ntupVar("RJR_PTISR",         Pt_ISR);
  ntupVar("RJR_RISR",          R_ISR);
  ntupVar("RJR_MS",            M_S);
  ntupVar("RJR_MISR",          M_ISR);
  ntupVar("RJR_dphiISRI",      DPhi_I_ISR);
  ntupVar("RJR_NjV",           Njet_V);
  ntupVar("RJR_NjISR",         Njet_ISR);
*/
}

