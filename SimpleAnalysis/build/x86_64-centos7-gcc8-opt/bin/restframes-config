#!/bin/bash
#
# restframes-config.  Generated from restframes-config.in by configure.
#
# This is the base script for retrieving all information
# regarding compiling and linking programs using RestFrames.
# Run ./restframes-config without arguments for usage details
#########################################################

# print a usage message and exit
# exit code passed as argument:
#   0 if it is a normal call
#   1 if it is due to a misusage.
usage()
{
  cat 1>&2 <<EOF

This is RestFrames v1.0.0 configuration tool.
Usage:
  restframes-config [--help] [--version] [--prefix] [--config] [--cxxflags] [--libs]

The arguments are:

  --help       prints this message and exits
  --version    prints RestFrames version and exits
  --prefix     prints the RestFrames installation directory
  --config     prints a summary of how RestFrames was configured
  --cxxflags   returns the compilation flags to be used with C++ programs
  --libs       returns the flags to pass to the linker

EOF
}
# first deal with the case where no argument is passed
[ $# -gt 0 ] || usage 1

# tools to parse options
########################

# option_name _string
# Returns NAME if _string is of the form: --NAME[=...]
option_name()
{
    echo "$1" | sed 's/^--//;s/=.*//' | tr '-' '_'
}

# option_value _string
# Returns FOO if _string is of the form: --option=FOO
option_value()
{
    echo "$1" | sed 's/^[^=]*=//'
}

# is_in_list _arg _arg_list
# return true if the argument _arg is in the list _arg_list
# and false otherwise
is_in_list()
{
    arg_match="$1"
    shift
    for arg_match_i do
        [ "x$arg_match_i" != "x$arg_match" ] || return 0
    done
    false
}


# useful utilities
##################

# wite error messages and exit
write_error()
{
    echo "Error: $1"
    echo "Use restframes-config --help for more information"
    exit 1
}


# browse the argument list
# This is done the following way:
#  - at first pass, we check if the --help argument is set. If yes, 
#    print usage and exit.
#    we also and make sure that there is no interference between the
#    arguments (e.g. --cxxflags --libs is wrong)
#  - we then check for extra arguments and return the requested info
#####################################################################
# useful lists of arguments
arg_query_list="version prefix help config" 

# no query found initially
found_query="no"
found_cxxflags="no"
found_libs="no"

# browse arguments
for arg do
    case "$arg" in
	--help|-h)
	    usage 0
	    ;;
	--cxxflags)
	    # we've found a query, make sure we don't already have one
	    # except if it is --libs
	    if [[ "x$found_query" != "xno" && "x$found_query" != "xlibs" ]]; then
		write_error "--cxxflags cannot be used with --$found_query"
	    fi

	    # update found_query 
	    # note: don't overwrite if libs are already asked for
	    found_cxxflags="yes"
	    if [ "x$found_query" != "xlibs" ]; then
		found_query="cxxflags"
	    fi	    
	    ;;
	--libs)
	    # we've found a query, make sure we don't already have one
	    # except if it is --cxxflags
	    if [[ "x$found_query" != "xno" && "x$found_query" != "xcxxflags" ]]; then
		write_error "--libs cannot be used with --$found_query"
	    fi

	    # update found_query 
	    found_libs="yes"
	    found_query="libs"
	    ;;
	--*)
	    arg_name=`option_name $arg`
	    if is_in_list $arg_name $arg_query_list ; then
		# we've found a query, make sure we don't already have one
		if [ "x$found_cxxflags" != "xno" ] ; then
		    write_error "--$arg_name cannot be used with --cxxflags"
		fi
		if [ "x$found_libs" != "xno" ] ; then
		    write_error "--$arg_name cannot be used with --libs"
		fi
		if [ "x$found_query" != "xno" ] ; then
		    write_error "Too many arguements to restframes-config"
		fi
		found_query="$arg_name"
	    else
		write_error "$arg: unrecognised argument"
	    fi
	    ;;
	*)
	    write_error "$arg is not a valid argument"
	    ;;
    esac
done


# now deal with the output
case $found_query in
    no)
	write_error "you must at least specify one argument to restframes-config"
	;;
    version)
	echo 1.0.0
	;;
    prefix)
	echo /storage/ballaben/compressed-ekw/SimpleAnalysis/build/CMakeFiles/RestFramesBuild
	;;
    cxxflags)
	echo -I/storage/ballaben/compressed-ekw/SimpleAnalysis/build/CMakeFiles/RestFramesBuild${prefix}/include 
	;;
    libs)
	libs_string=" -lm -L/storage/ballaben/compressed-ekw/SimpleAnalysis/build/CMakeFiles/RestFramesBuild${exec_prefix}/lib -lRestFrames"
	if [ "x$found_cxxflags" = "xyes" ] ; then
	    echo -I/storage/ballaben/compressed-ekw/SimpleAnalysis/build/CMakeFiles/RestFramesBuild${prefix}/include  $libs_string
	else
	    echo $libs_string
	fi
	;;
    config)
	echo "This is RestFrames version 1.0.0"
	echo ""
	echo "Configuration invocation was"
	echo ""
	echo "  /storage/ballaben/compressed-ekw/SimpleAnalysis/build/src/RestFrames/configure  '--prefix=/storage/ballaben/compressed-ekw/SimpleAnalysis/build/CMakeFiles/RestFramesBuild' '--enable-shared' '--disable-static' '--with-rootsys=/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.149/InstallArea/x86_64-centos7-gcc8-opt' 'CXX=/cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/g++' 'CC=/cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/gcc'"
	echo ""
	;;
esac
