#!/usr/bin/env python


# So Root ignores command line inputs so we can use argparse
import time 
from math import sqrt
# from random import gauss #seg fault?!
import os, time, argparse
from array import array

import numpy as np 
#it seg faults if imported before?!?
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(1)
# from ROOT import THStack, TCanvas, TPad, kBlue, kGray, kBlack,kWhite, kOrange, kAzure, TChain, TLine, TH1D, TLegend, RooStats, gPad, gStyle, TGaxis, TArrow, TLatex, TTree, TH1F
from ROOT import THStack, TCanvas, TPad, kBlue, kGray, kBlack,kWhite, kOrange, kAzure, TChain, TLine, TH1D, TLegend, gPad, gStyle, TGaxis, TArrow, TLatex, RDataFrame, TTree, TColor
#customise_gPad

#____________________________________________________________________________
def main():
  d_Znunu  = ROOT.RDataFrame("Znunu",        "/Users/ericballabene/DisplacedTrack/New_HitInfo_False/New_HitInfo_False_withDNN/Znunu_withDNN.root");
  d_Wmunu  = ROOT.RDataFrame("Wmunu",        "/Users/ericballabene/DisplacedTrack/New_HitInfo_False/New_HitInfo_False_withDNN/Wmunu_withDNN.root");
  d_Wenu   = ROOT.RDataFrame("Wenu",         "/Users/ericballabene/DisplacedTrack/New_HitInfo_False/New_HitInfo_False_withDNN/Wenu_withDNN.root");
  d_Wtaunu = ROOT.RDataFrame("Wtaunu",       "/Users/ericballabene/DisplacedTrack/New_HitInfo_False/New_HitInfo_False_withDNN/Wtaunu_withDNN.root");
  d_ttbar  = ROOT.RDataFrame("ttbar",        "/Users/ericballabene/DisplacedTrack/New_HitInfo_False/New_HitInfo_False_withDNN/ttbar_withDNN.root");

  dataframes = [d_ttbar, d_Wenu, d_Wmunu, d_Wtaunu, d_Znunu] 

  xaxis = 'DNN'
  xaxis_units = ''

  yaxis = 'Tracks'
  yaxis_units = ''

  hs1 = ROOT.THStack("hs1","")
  hs2 = ROOT.THStack("hs2","")

  histo_array = []

  for df in dataframes:

      #HISTOGRAM WITH MET>400

      histo1 = df.Filter("MET>400").Histo1D(("Histogram1","Histogram_background", 40, 0., 0.2),'DNN','weight')

      #histo1.SetMarkerStyle(2)
      #histo1.SetLineColor(ROOT.kBlue+1)
      if df == d_ttbar:
          histo1.SetLineColor(ROOT.kGray+1)
          histo1.SetMarkerColor(ROOT.kGray+1)
          histo1.SetFillColor(ROOT.kGray+1)
      if df == d_Znunu:
          histo1.SetLineColor(ROOT.kRed)
          histo1.SetMarkerColor(ROOT.kRed)
          histo1.SetFillColor(ROOT.kRed)
      if df == d_Wmunu:
          histo1.SetLineColor(ROOT.kOrange)
          histo1.SetMarkerColor(ROOT.kOrange)
          histo1.SetFillColor(ROOT.kOrange)
      if df == d_Wenu:
          histo1.SetLineColor(ROOT.kRed+2)
          histo1.SetMarkerColor(ROOT.kRed+2)
          histo1.SetFillColor(ROOT.kRed+2)
      if df == d_Wtaunu:
          histo1.SetLineColor(ROOT.kYellow)
          histo1.SetMarkerColor(ROOT.kYellow)
          histo1.SetFillColor(ROOT.kYellow)

      histo1.GetXaxis().SetTitle(xaxis+xaxis_units)
      histo1.GetYaxis().SetTitle(yaxis+yaxis_units)
      histo_array.append(histo1)
      histo1.SetStats(0)
      histo1.GetXaxis().SetRangeUser(0,0.2)
      #histo1.SetMinimum(0.01) 
      
      hs1.Add(TH1D(histo1.GetPtr()))


      #HISTOGRAM WITH 200<MET<400


      histo2 = df.Filter("MET<400 && MET>200").Histo1D(("Histogram2","Histogram_background", 40, 0., 0.2),'DNN','weight')
      histo2.SetMarkerStyle(2)
      histo2.SetLineColor(ROOT.kRed)
      histo2.SetMarkerColor(ROOT.kRed)
      histo2.SetFillColor(ROOT.kRed)

      histo2.GetXaxis().SetTitle(xaxis+xaxis_units)
      histo2.GetYaxis().SetTitle(yaxis+yaxis_units)

      histo2.SetStats(0)
      histo2.GetXaxis().SetRangeUser(0,0.2)
      histo2.SetMinimum(0.01)

      hs2.Add(TH1D(histo2.GetPtr()))


  #NORMALIZING AND DRAWING HISTOGRAMS

  histomax=hs1.GetStack().Last()
  histomin=hs2.GetStack().Last()
  integral1=histomax.Integral()
  integral2=histomin.Integral()
  histomin.Scale(integral1/integral2)

  c = ROOT.TCanvas("","",600,400)
  c.SetRightMargin(0.22)
  gPad.SetLogy()
  c.Draw()
  histo_array[4].Draw("SAME")
  leg = ROOT.TLegend(0.60, 0.68, 1., 0.93)
  leg.SetBorderSize(0)
  leg.SetTextSize(0.028)
  leg.SetNColumns(1)
  leg.AddEntry(histomax, "Znunu with MET > 400 GeV", "l")
  leg.Draw()
  c.SaveAs('NEW_DNN_MET.pdf')


  c2 = ROOT.TCanvas("","",600,400)
  c2.SetRightMargin(0.22)
  gPad.SetLogy()
  c2.Draw()
  hs1.Draw()
  leg = ROOT.TLegend(0.60, 0.68, 1., 0.93)
  leg.SetBorderSize(0)
  leg.SetTextSize(0.028)
  leg.SetNColumns(1)
  leg.AddEntry(hs1, "THStack MET > 400 GeV", "l")
  leg.Draw()
  c2.SaveAs('NEW_DNN_MET2.pdf')

  c3 = ROOT.TCanvas("","",600,400)
  c3.SetRightMargin(0.22)
  gPad.SetLogy()
  c3.Draw()
  histomax.SetMarkerColorAlpha(ROOT.kRed,0.5)
  histomax.SetLineColorAlpha(ROOT.kRed,0.5)
  histomax.Draw()
  histomin.SetMarkerColorAlpha(ROOT.kGreen+2,0.5)
  histomin.SetLineColorAlpha(ROOT.kGreen+2,0.5)
  histomin.SetLineStyle(2)
  histomin.Draw("SAME")
  leg3 = ROOT.TLegend(0.40, 0.68, 0.5, 0.85)
  leg3.SetBorderSize(0)
  leg3.SetTextSize(0.028)
  leg3.SetNColumns(1)
  leg3.AddEntry(histomax, "THStack with MET > 400 GeV", "l")
  leg3.AddEntry(histomin, "THStack with 200 GeV < MET < 400 GeV", "l")
  leg3.Draw()
  c3.SaveAs('NEW_DNN_MET3.pdf')

 
if __name__ == "__main__":
  #main(sys.argv)
  main()


