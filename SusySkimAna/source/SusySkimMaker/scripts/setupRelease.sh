#!/bin/bash

# Analysis base release for release 21
RELEASE_21="21.2.221"

# Dependencies needed by the framework
TRUTHDECAYCONTAINER_TAG='TruthDecayContainer-05-03'
IFFTRUTHCLASSFIER_TAG="v1.0.0"

currentUserName=$USER
if [[ ! -z $CERN_USER ]]; then
  cernUserName=$CERN_USER
else
  cernUserName=alsala
fi

ARG="$@"

# reset command line arguments to avoid other scripts like rcSetup
# read them
set -- ""

########################

if [[ $ARG == *--skip-kinit* || `hostname` == *lxplus* ]]
then
    shift
else
    echo "Your current username is $currentUserName, your cern username is $cernUserName"
    echo "Grabbing a kerberos ticket:"
    kinit $cernUserName@CERN.CH
    if [ $? -ne 0 ] ;  then
	echo "kinit failed."
	echo "Abort. re-source me."
	return
    fi
    echo "kinit succeeded."
fi

#
echo "Setting up ATLAS..."
setupATLAS

echo "Setting up GIT from cvmfs..."
lsetup git

# If running in Rel. 21
# make the required directories
# and checkout packages into the 
# source directory
echo "Setting up a Release 21 version..."
lsetup "asetup AnalysisBase,${RELEASE_21},here" "rucio -w" pyami
mkdir -p "../build"

echo "Cloning SusySkimDriver..."
git clone ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimDriver.git

echo "Cloning IFFTruthClassifier tool..."
git clone -b "$IFFTRUTHCLASSFIER_TAG" --single-branch ssh://git@gitlab.cern.ch:7999/ATLAS-IFF/IFFTruthClassifier.git

echo "Cloning TruthDecayContainer ..."
git clone ssh://git@gitlab.cern.ch:7999/shion/TruthDecayContainer.git

echo "Copying set_groupset_env.sh template script to TestArea directory..."
cp ${TestArea}/SusySkimMaker/scripts/set_groupset_env.sh ${TestArea}

echo "Generating setup script..."
 
# Clean up if it exists..
if [ -e "setup.sh" ]; then
  rm -rf setup.sh
fi

echo "#!/bin/bash" >> "setup.sh"
echo "" >> "setup.sh"
echo "setupATLAS" >> "setup.sh"
echo "" >> "setup.sh"
echo "lsetup \"asetup AnalysisBase,${RELEASE_21},here\" \"rucio -w\" pyami" >> "setup.sh"
echo "echo \"Creating an alias \"build\". In order to compile you just need to type build\"" >> "setup.sh"
echo "alias build='cmake --build \$TestArea/../build; source \$TestArea/../build/*/setup.sh'" >> "setup.sh"
echo "" >> "setup.sh"
echo "# RestFrames requires additions to CMakeList after each asetup" >> "setup.sh"
echo "# (see https://gitlab.cern.ch/atlas-phys-susy-wg/Common/Ext_RestFrames.git)" >> "setup.sh"
echo "if [ -d \"Ext_RestFrames\" ]; then" >> "setup.sh"
echo "  sed -i 's/find_package( AnalysisBase )/find_package( AnalysisBase ) \\" >> "setup.sh"
echo "  \n\n# Include the externals configuration: \\" >> "setup.sh"
echo "  \ninclude( Ext_RestFrames\/externals.cmake )/g' CMakeLists.txt" >> "setup.sh"
echo "fi" >> "setup.sh"
echo "" >> "setup.sh"
echo "source set_groupset_env.sh" >> "setup.sh"
echo "" >> "setup.sh"
echo "cd \$TestArea/../build" >> "setup.sh"
echo "" >> "setup.sh"
echo "cmake \$TestArea" >> "setup.sh"
echo "" >> "setup.sh"
echo "cd \$TestArea" >> "setup.sh"

echo "Running setup script..."
chmod +x "setup.sh"
source "setup.sh"

echo ""
echo "Finished R21 setup.."
echo "Next time you login, simply source setup.sh and you will be setup!"
