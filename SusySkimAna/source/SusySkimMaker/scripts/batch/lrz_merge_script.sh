#!/bin/bash
##
##
#SBATCH --clusters=lcg
#SBATCH --partition=lcg_serial
#SBATCH --qos=lcg_add
#SBATCH --export=NONE
#SBATCH --get-user-env
#SBATCH --mem=1000mb
#SBATCH --time=04:00:00

echo Running on host `hostname`
echo Time is `date`
echo Directory is `pwd`

shopt -s expand_aliases

source /home/grid/lcg/sw/setup_atlas_cvmfs.sh.rod

pushd $rootcorebin/../

set -- ""
source rcSetup.sh

popd

echo "run_merge_standalone \\"
echo "    -sys $sys \\"
echo "    -d $path \\"
echo "    -tag $mytag \\"
echo "    -groupSet $groupset \\"
echo "    -p $process \\"
echo "    -deepConfig $deepConfig"

run_merge_standalone -sys $sys -d $path -tag $mytag -groupSet $groupset -p $process -deepConfig $deepConfig 

