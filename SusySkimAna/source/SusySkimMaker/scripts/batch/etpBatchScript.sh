#!/bin/bash
##
##
#SBATCH --partition=lsschaile
#SBATCH --export=NONE  # do not export current environemnt to job (recommended)
#SBATCH --get-user-env
#SBATCH --mem=2000mb
#SBATCH --time=02:00:00
#SBATCH -o /project/etp4/nhartmann/log/SkimNtRun.%j
#

source /project/etp3/ThomasMaier/Singularity/setup_slc6.sh

if [[ -z "$SINGULARITY_IMAGE" ]]
then
  slc6 $0 $@
  exit 0
fi

echo Running on host `hostname`
echo Time is `date`
echo Directory is `pwd`

echo "Selector: " $selector
echo "Input:    " $input
echo "Output:   " $output
echo "nEvt:     " $nEvt
echo "nSkip:    " $nSkip
echo "TestArea: " $TestArea

shopt -s expand_aliases

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

pushd $TestArea
acmSetup
popd

run_SkimNtRun -F $input -s $output -selector $selector -k $nSkip -n $nEvt
