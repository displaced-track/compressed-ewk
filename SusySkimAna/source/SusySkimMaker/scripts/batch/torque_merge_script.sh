#!/bin/bash

#PBS -N merge_script
#PBS -l walltime=24:00:00

echo Running on host `hostname`
echo Time is `date`
echo Directory is `pwd`
echo This jobs runs on the following processors:
echo `cat $PBS_NODEFILE`

run_merge_standalone -sys $sys -d $path -tag $mytag -groupSet $groupset -p $process -deepConfig $deepConfig 

