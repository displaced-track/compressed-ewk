#include "SusySkimMaker/JetVariable.h"

JetVariable::JetVariable() : truthLink(0)
{

  //
  setDefault(jvt,-1.0);
  setDefault(mv2c10,-1.0);
  setDefault(dl1,0.0);
  setDefault(dl1r,0.0);
  setDefault(bjet,false);
  setDefault(signal,false);
  setDefault(passOR,false);
  setDefault(passDRcut,false);
  setDefault(bad,false);
  setDefault(badTile,false);
  setDefault(truthLabel,-1);
  setDefault(nTrk,-1);
  setDefault(tileEnergy,0.0);
  setDefault(EMFrac,0.0);
  setDefault(HECFrac,0.0);
  setDefault(LArQuality,0.0);
  setDefault(HECQuality,0.0);
  setDefault(Timing,0.0);
  setDefault(sumpttrk,0.0);
  setDefault(FracSamplingMax,0.0);
  setDefault(NegativeE,0.0);
  setDefault(AverageLArQF,0.0);
  setDefault(FracSamplingMaxIndex,0.0);
  setDefault(Width,0.0);
  setDefault(D2,0.0);
  setDefault(Tau32,0.0);
  setDefault(nGATrackJets,0);
  setDefault(nGATrackBJets60Eff,0),
  setDefault(nGATrackBJets70Eff,0),
  setDefault(nGATrackBJets77Eff,0),
  setDefault(nGATrackBJets85Eff,0),
  setDefault(nGATrackBJets60Eff_DL1r,0),
  setDefault(nGATrackBJets70Eff_DL1r,0),
  setDefault(nGATrackBJets77Eff_DL1r,0),
  setDefault(nGATrackBJets85Eff_DL1r,0),
  setDefault(passWTag50,false);
  setDefault(passWTag50_mass,false);
  setDefault(passWTag50_sub,false);
  setDefault(passWTag50_nTrk,false);
  setDefault(passZTag50,false);
  setDefault(passZTag50_mass,false);
  setDefault(passZTag50_sub,false);
  setDefault(passZTag50_nTrk,false);
  setDefault(bjt_wsf50,-1);
  setDefault(bjt_zsf50,-1);
  setDefault(passWTagIncl50,false);
  setDefault(passWTagIncl50_mass,false);
  setDefault(passWTagIncl50_sub,false);
  setDefault(passWTagIncl50_nTrk,false);
  setDefault(passZTagIncl50,false);
  setDefault(passZTagIncl50_mass,false);
  setDefault(passZTagIncl50_sub,false);
  setDefault(passZTagIncl50_nTrk,false);
  setDefault(bjt_wsfIncl50,-1);
  setDefault(bjt_zsfIncl50,-1);
  setDefault(Nominal_bjt_weffIncl50,-1);
  setDefault(Nominal_bjt_zeffIncl50,-1);
  setDefault(Nominal_pt,0.);
  setDefault(Nominal_eta,0.);
  setDefault(Nominal_phi,0.);
  setDefault(Nominal_m,0.);
  setDefault(passWTag80,false);
  setDefault(passWTag80_mass,false);
  setDefault(passWTag80_sub,false);
  setDefault(passWTag80_nTrk,false);
  setDefault(passZTag80,false);
  setDefault(passZTag80_mass,false);
  setDefault(passZTag80_sub,false);
  setDefault(passZTag80_nTrk,false);
  setDefault(bjt_wsf80,-1);
  setDefault(bjt_zsf80,-1);
  setDefault(XbbScoreQCD,-999);
  setDefault(XbbScoreTop,-999);
  setDefault(XbbScoreHiggs,-999);
  //

}
// -------------------------------------------------------------------------------- //
JetVariable::JetVariable(const JetVariable &rhs):
  ObjectVariable(rhs),
  jvt(rhs.jvt),
  mv2c10(rhs.mv2c10),
  dl1(rhs.dl1),
  dl1r(rhs.dl1r),
  bjet(rhs.bjet),
  signal(rhs.signal),
  bad(rhs.bad),
  badTile(rhs.badTile),
  passOR(rhs.passOR),
  passDRcut(rhs.passDRcut),
  FT_SF(rhs.FT_SF),
  JVT_SF(rhs.JVT_SF),
  truthLabel(rhs.truthLabel),
  nTrk(rhs.nTrk),
  tileEnergy(rhs.tileEnergy),
  EMFrac(rhs.EMFrac),
  HECFrac (rhs.HECFrac),
  LArQuality(rhs.LArQuality),
  HECQuality (rhs.HECQuality ),
  Timing(rhs.Timing),
  sumpttrk(rhs.sumpttrk),
  FracSamplingMax(rhs.FracSamplingMax),
  NegativeE(rhs.NegativeE),
  AverageLArQF(rhs.AverageLArQF),
  FracSamplingMaxIndex(rhs.FracSamplingMaxIndex),
  Width(rhs.Width),
  D2(rhs.D2),
  Tau32(rhs.Tau32),
  GATrackJets(rhs.GATrackJets),
  nGATrackJets(rhs.nGATrackJets),
  nGATrackBJets60Eff(rhs.nGATrackBJets60Eff),
  nGATrackBJets70Eff(rhs.nGATrackBJets70Eff),
  nGATrackBJets77Eff(rhs.nGATrackBJets77Eff),
  nGATrackBJets85Eff(rhs.nGATrackBJets85Eff),
  nGATrackBJets60Eff_DL1r(rhs.nGATrackBJets60Eff_DL1r),
  nGATrackBJets70Eff_DL1r(rhs.nGATrackBJets70Eff_DL1r),
  nGATrackBJets77Eff_DL1r(rhs.nGATrackBJets77Eff_DL1r),
  nGATrackBJets85Eff_DL1r(rhs.nGATrackBJets85Eff_DL1r),
  passWTag50(rhs.passWTag50),
  passWTag50_mass(rhs.passWTag50_mass),
  passWTag50_sub(rhs.passWTag50_sub),
  passWTag50_nTrk(rhs.passWTag50_nTrk),
  passZTag50(rhs.passZTag50),
  passZTag50_mass(rhs.passZTag50_mass),
  passZTag50_sub(rhs.passZTag50_sub),
  passZTag50_nTrk(rhs.passZTag50_nTrk),
  bjt_wsf50(rhs.bjt_wsf50),
  bjt_zsf50(rhs.bjt_zsf50),
  passWTagIncl50(rhs.passWTagIncl50),
  passWTagIncl50_mass(rhs.passWTagIncl50_mass),
  passWTagIncl50_sub(rhs.passWTagIncl50_sub),
  passWTagIncl50_nTrk(rhs.passWTagIncl50_nTrk),
  passZTagIncl50(rhs.passZTagIncl50),
  passZTagIncl50_mass(rhs.passZTagIncl50_mass),
  passZTagIncl50_sub(rhs.passZTagIncl50_sub),
  passZTagIncl50_nTrk(rhs.passZTagIncl50_nTrk),
  bjt_wsfIncl50(rhs.bjt_wsfIncl50),
  bjt_zsfIncl50(rhs.bjt_zsfIncl50),
  Nominal_bjt_weffIncl50(rhs.Nominal_bjt_weffIncl50),
  Nominal_bjt_zeffIncl50(rhs.Nominal_bjt_zeffIncl50),
  Nominal_pt(rhs.Nominal_pt),
  Nominal_eta(rhs.Nominal_eta),
  Nominal_phi(rhs.Nominal_phi),
  Nominal_m(rhs.Nominal_m),
  passWTag80(rhs.passWTag80),
  passWTag80_mass(rhs.passWTag80_mass),
  passWTag80_sub(rhs.passWTag80_sub),
  passWTag80_nTrk(rhs.passWTag80_nTrk),
  passZTag80(rhs.passZTag80),
  passZTag80_mass(rhs.passZTag80_mass),
  passZTag80_sub(rhs.passZTag80_sub),
  passZTag80_nTrk(rhs.passZTag80_nTrk),
  bjt_wsf80(rhs.bjt_wsf80),
  bjt_zsf80(rhs.bjt_zsf80),
  XbbScoreQCD(rhs.XbbScoreQCD),
  XbbScoreTop(rhs.XbbScoreTop),
  XbbScoreHiggs(rhs.XbbScoreHiggs),
  truthLink(rhs.truthLink)  
{

}
// -------------------------------------------------------------------------------- //
JetVariable& JetVariable::operator=(const JetVariable &rhs)
{

  if(this != &rhs){
    ObjectVariable::operator=(rhs);
    jvt         = rhs.jvt;
    mv2c10      = rhs.mv2c10;
    dl1         = rhs.dl1;
    dl1r        = rhs.dl1r;
    bad         = rhs.bad;
    badTile     = rhs.badTile;
    passOR      = rhs.passOR;
    passDRcut   = rhs.passDRcut;
    bjet        = rhs.bjet;
    signal      = rhs.signal;
    FT_SF       = rhs.FT_SF;
    JVT_SF      = rhs.JVT_SF;
    truthLabel  = rhs.truthLabel;
    nTrk        = rhs.nTrk;
    tileEnergy=rhs.tileEnergy;
    EMFrac=rhs.EMFrac;
    HECFrac =rhs.HECFrac;
    LArQuality=rhs.LArQuality;
    HECQuality =rhs.HECQuality;
    Timing=rhs.Timing;
    sumpttrk=rhs.sumpttrk;
    FracSamplingMax=rhs.FracSamplingMax;
    NegativeE=rhs.NegativeE;
    AverageLArQF=rhs.AverageLArQF;
    FracSamplingMaxIndex=rhs.FracSamplingMaxIndex;
    Width=rhs.Width;
    D2=rhs.D2;
    Tau32=rhs.Tau32;
    GATrackJets=rhs.GATrackJets;
    nGATrackJets=rhs.nGATrackJets;
    nGATrackBJets60Eff=rhs.nGATrackBJets60Eff;
    nGATrackBJets70Eff=rhs.nGATrackBJets70Eff;
    nGATrackBJets77Eff=rhs.nGATrackBJets77Eff;
    nGATrackBJets85Eff=rhs.nGATrackBJets85Eff;
    nGATrackBJets60Eff_DL1r=rhs.nGATrackBJets60Eff_DL1r;
    nGATrackBJets70Eff_DL1r=rhs.nGATrackBJets70Eff_DL1r;
    nGATrackBJets77Eff_DL1r=rhs.nGATrackBJets77Eff_DL1r;
    nGATrackBJets85Eff_DL1r=rhs.nGATrackBJets85Eff_DL1r;
    passWTag50          = rhs.passWTag50;
    passWTag50_mass     = rhs.passWTag50_mass;
    passWTag50_sub      = rhs.passWTag50_sub;
    passWTag50_nTrk     = rhs.passWTag50_nTrk;
    passZTag50          = rhs.passZTag50;
    passZTag50_mass     = rhs.passZTag50_mass;
    passZTag50_sub      = rhs.passZTag50_sub;
    passZTag50_nTrk     = rhs.passZTag50_nTrk;
    bjt_wsf50           = rhs.bjt_wsf50;
    bjt_zsf50           = rhs.bjt_zsf50;
    passWTagIncl50      = rhs.passWTagIncl50;
    passWTagIncl50_mass = rhs.passWTagIncl50_mass;
    passWTagIncl50_sub  = rhs.passWTagIncl50_sub;
    passWTagIncl50_nTrk = rhs.passWTagIncl50_nTrk;
    passZTagIncl50      = rhs.passZTagIncl50;
    passZTagIncl50_mass = rhs.passZTagIncl50_mass;
    passZTagIncl50_sub  = rhs.passZTagIncl50_sub;
    passZTagIncl50_nTrk = rhs.passZTagIncl50_nTrk;
    bjt_wsfIncl50       = rhs.bjt_wsfIncl50;
    bjt_zsfIncl50       = rhs.bjt_zsfIncl50;
    Nominal_bjt_weffIncl50 = rhs.Nominal_bjt_weffIncl50;
    Nominal_bjt_zeffIncl50 = rhs.Nominal_bjt_zeffIncl50;
    Nominal_pt          = rhs.Nominal_pt;
    Nominal_eta         = rhs.Nominal_eta;
    Nominal_phi         = rhs.Nominal_phi;
    Nominal_m           = rhs.Nominal_m;
    passWTag80          = rhs.passWTag80;
    passWTag80_mass     = rhs.passWTag80_mass;
    passWTag80_sub      = rhs.passWTag80_sub;
    passWTag80_nTrk     = rhs.passWTag80_nTrk;
    passZTag80          = rhs.passZTag80;
    passZTag80_mass     = rhs.passZTag80_mass;
    passZTag80_sub      = rhs.passZTag80_sub;
    passZTag80_nTrk     = rhs.passZTag80_nTrk;
    bjt_wsf80           = rhs.bjt_wsf80;
    bjt_zsf80           = rhs.bjt_zsf80;
    XbbScoreQCD=rhs.XbbScoreQCD;
    XbbScoreTop=rhs.XbbScoreTop;
    XbbScoreHiggs=rhs.XbbScoreHiggs;
    truthLink = rhs.truthLink;
  }

  return *this;

}
// -------------------------------------------------------------------------------- //
float JetVariable::getFtSF(const TString sys)
{

  auto it = this->FT_SF.find(sys);

  if( it==this->FT_SF.end() ){

    auto nom = this->FT_SF.find("");

    if( nom == this->FT_SF.end() ){
      std::cout << "<JetVariable::getFtSF> WARNING Cannot get flavor tagging SF for systematic: " << sys << " or nominal. Returning 1.0" << std::endl;
      return 1.0;
    }
    else{
      //std::cout << "<JetVariable::getFtSF> WARNING Cannot get flavor tagging SF for systematic: " << sys << ", returning nominal" << std::endl;
      return nom->second;
    }
  }
  else{
    return it->second;
  }

}
// ----------------------------------------------------------------- //
float JetVariable::getJVTSF(const TString sys)
{

  auto it = this->JVT_SF.find(sys);

  if( it==this->JVT_SF.end() ){

    auto nom = this->JVT_SF.find("");

    if( nom == this->JVT_SF.end() ){
      // std::cout << "<JetVariable::getJVTSF> WARNING Cannot get JVT SF for systematic: " << sys << " or nominal. Returning 1.0" << std::endl;
      return 1.0;
    }
    else{
      std::cout << "<JetVariable::getJVTSF> WARNING Cannot get JVT SF for systematic: " << sys << ", returning nominal" << std::endl;
      return nom->second;
    }
  }
  else{
    return it->second;
  }

}
// -------------------------------------------------------------------------------- //
bool JetVariable::hasTruthLink()
{

  if( this->truthLink ) return true;
  else                  return false;

}
// -------------------------------------------------------------------------------- //
void JetVariable::dumpJet()
{
  std::cout << "Dumping jet... " << std::endl;
  printf("Jet (pt,eta,phi,m) %f,%f,%f,%f",this->Pt(),this->Eta(),this->Phi(),this->M());

}


