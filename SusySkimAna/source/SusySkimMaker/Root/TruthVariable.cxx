#include "SusySkimMaker/TruthVariable.h"
#include "SusySkimMaker/MsgLog.h"

TruthVariable::TruthVariable()
{

  setDefault(q,-99);
  setDefault(charge,-999.9);
  setDefault(pdgId,0);
  setDefault(status,0);
  setDefault(barcode,0);
  setDefault(parentPdgId,0);
  setDefault(parentBarcode,0);
  setDefault(parentStatus,0);
  setDefault(isTruthBSM,0);
  setDefault(decayVtx,TVector3(0.,0.,0.));
  setDefault(prodVtx,TVector3(0.,0.,0.));
  setDefault(decayVtxPerp,0.0);
  setDefault(decayVtxX,0.0);
  setDefault(decayVtxY,0.0);
  setDefault(decayVtxZ,0.0);
  setDefault(prodVtxPerp,0.0);
  setDefault(prodVtxX,0.0);
  setDefault(prodVtxY,0.0);
  setDefault(prodVtxZ,0.0);
  setDefault(isAntiKt4Jet,false);
  setDefault(bjet,false);

}
// -------------------------------------------------------------------------------- //
TruthVariable::TruthVariable(const TruthVariable &rhs):
  ObjectVariable(rhs),
  q(rhs.q),
  charge(rhs.charge),
  pdgId(rhs.pdgId),
  status(rhs.status),
  barcode(rhs.barcode),
  parentPdgId(rhs.parentPdgId),
  parentBarcode(rhs.parentBarcode),
  parentStatus(rhs.parentStatus),
  isTruthBSM(rhs.isTruthBSM),
  decayVtx(rhs.decayVtx),
  prodVtx(rhs.prodVtx),
  decayVtxPerp(rhs.decayVtxPerp),
  decayVtxX(rhs.decayVtxX),
  decayVtxY(rhs.decayVtxY),
  decayVtxZ(rhs.decayVtxZ),
  prodVtxPerp(rhs.prodVtxPerp),
  prodVtxX(rhs.prodVtxX),
  prodVtxY(rhs.prodVtxY),
  prodVtxZ(rhs.prodVtxZ),
  isAntiKt4Jet(rhs.isAntiKt4Jet),
  bjet(rhs.bjet),
  bareTLV(rhs.bareTLV)
{
}
// -------------------------------------------------------------------------------- //
TruthVariable& TruthVariable::operator=(const TruthVariable &rhs)
{
  if (this != &rhs) {
    ObjectVariable::operator=(rhs);
    q               = rhs.q;
    charge          = rhs.charge;
    pdgId           = rhs.pdgId;
    status          = rhs.status;
    barcode         = rhs.barcode;
    parentPdgId     = rhs.parentPdgId;
    parentBarcode   = rhs.parentBarcode;
    parentStatus    = rhs.parentStatus;
    isTruthBSM      = rhs.isTruthBSM;
    decayVtx        = rhs.decayVtx;
    prodVtx         = rhs.prodVtx;
    decayVtxPerp    = rhs.decayVtxPerp;
    decayVtxX       = rhs.decayVtxX;
    decayVtxY       = rhs.decayVtxY;
    decayVtxZ       = rhs.decayVtxZ;
    prodVtxPerp     = rhs.prodVtxPerp;
    prodVtxX        = rhs.prodVtxX;
    prodVtxY        = rhs.prodVtxY;
    prodVtxZ        = rhs.prodVtxZ;
    isAntiKt4Jet    = rhs.isAntiKt4Jet;
    bjet            = rhs.bjet;
    bareTLV         = rhs.bareTLV;
  }
  return *this;
}
// -------------------------------------------------------------------------------- //
void TruthVariable::print()
{

  MsgLog::INFO("TruthVarible::print()","Listing truth particle properties...");
  MsgLog::INFO("TruthVarible::print()","Truth particle pT %f",this->Pt() );
  MsgLog::INFO("TruthVarible::print()","Truth particle eta %f",this->Eta() );
  MsgLog::INFO("TruthVarible::print()","Truth particle pdg ID %i",this->pdgId );
  MsgLog::INFO("TruthVarible::print()","Truth particle barcode %i",this->barcode );
  MsgLog::INFO("TruthVarible::print()","Truth particle parent pdgID %i",this->parentPdgId );
  MsgLog::INFO("TruthVarible::print()","Particle classified as a bjet %i",this->bjet );


}

