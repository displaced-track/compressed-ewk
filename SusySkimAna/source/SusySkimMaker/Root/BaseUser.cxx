#include "SusySkimMaker/BaseUser.h"
#include "SusySkimMaker/MsgLog.h"


// ---------------------------------------------------------- //
BaseUser::BaseUser(TString packageName, TString selectorName) : m_selectorName(selectorName),
                                                                m_packageName(packageName),
                                                                m_truthOnly(false),
                                                                m_disableCuts(false)
{

  MsgLog::INFO("BaseUser::BaseUser","Adding a selector %s to package %s",selectorName.Data(),packageName.Data() );
  getList()->push_back(this);

}
// ---------------------------------------------------------- //
BaseUser* getAnalysisSelector(TString selectorName)
{

  BaseUser* UserSelector = 0;

  //
  for( const auto& anaSel : *getList() ){
    if( anaSel->getSelectorName() == selectorName ){
      UserSelector = anaSel;
      MsgLog::INFO("getAnalysisSelector","Found analysis selector %s!", selectorName.Data() );
    }
  }

  // TODO: Decide how we want to handle this. Abort??
  if( !UserSelector ){
    MsgLog::ERROR("getAnalysisSelector","Could not find analysis selector %s!!!", selectorName.Data() );
    abort();
  }

  // Return gracefully
  return UserSelector;

}
// ---------------------------------------------------------- //
std::vector<BaseUser*> *getList()
{

  static std::vector<BaseUser*> *list = new std::vector<BaseUser*>;
  return list;

}
// ---------------------------------------------------------- //
