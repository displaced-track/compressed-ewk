#include "SusySkimMaker/EventVariable.h"

EventVariable::EventVariable()
{
  eventCleaning.clear();
}
// -------------------------------------------------------------------------------- //
EventVariable::EventVariable(const EventVariable &rhs):
  runNumber(rhs.runNumber),
  randomRunNumber(rhs.randomRunNumber),
  evtNumber(rhs.evtNumber),
  lumiBlock(rhs.lumiBlock),
  dsid(rhs.dsid),
  timestamp(rhs.timestamp),
  beampos(rhs.beampos),
  nVtx(rhs.nVtx),
  numHSJets(rhs.numHSJets),
  FS(rhs.FS),
  x1(rhs.x1),
  x2(rhs.x2),
  pdf1(rhs.pdf1),
  pdf2(rhs.pdf2),
  scalePDF(rhs.scalePDF),
  id1(rhs.id1),
  id2(rhs.id2),
  mu(rhs.mu),
  avg_mu(rhs.avg_mu),
  actual_mu(rhs.actual_mu),
  beamSpotX(rhs.beamSpotX),
  beamSpotY(rhs.beamSpotY),
  beamSpotZ(rhs.beamSpotZ),
  beamSpotXerr(rhs.beamSpotXerr),
  beamSpotYerr(rhs.beamSpotYerr),
  beamSpotZerr(rhs.beamSpotZerr),
  beamSpotXYerr(rhs.beamSpotXYerr),
  beamTiltXZ(rhs.beamTiltXZ),
  beamTiltYZ(rhs.beamTiltYZ),
  privtx(rhs.privtx),
  privtxErrX(rhs.privtxErrX),
  privtxErrY(rhs.privtxErrY),
  privtxErrZ(rhs.privtxErrZ),
  pileupvtx(rhs.pileupvtx),
  privtxCorXY(rhs.privtxCorXY),
  privtxCorXZ(rhs.privtxCorXZ),
  privtxCorYZ(rhs.privtxCorYZ),
  privtxFitQ(rhs.privtxFitQ),
  phvtx(rhs.phvtx),
  phvtxErrZ(rhs.phvtxErrZ),
  phvtxFitQ(rhs.phvtxFitQ),
  pileUp(rhs.pileUp),
  pileUp_sysUp(rhs.pileUp_sysUp),
  pileUp_sysDown(rhs.pileUp_sysDown),
  PRWHash(rhs.PRWHash),
  mcWeight(rhs.mcWeight),
  xsec(rhs.xsec),
  xsec_uncert(rhs.xsec_uncert),
  nEvents(rhs.nEvents),
  sumOfWeightsPerEvent(rhs.sumOfWeightsPerEvent),
  runLumi(rhs.runLumi),
  isMC(rhs.isMC),
  stream(rhs.stream),
  eventCleaning(rhs.eventCleaning),
  evtL1Trigger(rhs.evtL1Trigger),
  evtHLTTrigger(rhs.evtHLTTrigger),
  evtTriggerPrescale(rhs.evtTriggerPrescale),
  met_L1(rhs.met_L1),
  met_HLT_pufit(rhs.met_HLT_pufit),
  met_HLT_cell(rhs.met_HLT_cell),
  leadJetPt_L1(rhs.leadJetPt_L1),
  leadJetPt_HLT(rhs.leadJetPt_HLT),
  isVgammaOverlap(rhs.isVgammaOverlap),
  sherpaNJetWeight(rhs.sherpaNJetWeight),
  globalDiLepTrigSF(rhs.globalDiLepTrigSF),
  globalMultiLepTrigSF(rhs.globalMultiLepTrigSF),
  globalPhotonTrigSF(rhs.globalPhotonTrigSF),
  GenHt(rhs.GenHt),
  GenMET(rhs.GenMET),
  MCCorr(rhs.MCCorr)
{
}
// -------------------------------------------------------------------------------- //
EventVariable& EventVariable::operator=(const EventVariable &rhs)
{
  if (this != &rhs) {
    runNumber            = rhs.runNumber;
    randomRunNumber      = rhs.randomRunNumber;
    evtNumber            = rhs.evtNumber;
    lumiBlock            = rhs.lumiBlock;
    dsid                 = rhs.dsid;
    timestamp            = rhs.timestamp;
    beampos              = rhs.beampos;
    nVtx                 = rhs.nVtx;
    numHSJets            = rhs.numHSJets;
    FS                   = rhs.FS;
    x1                   = rhs.x1;
    x2                   = rhs.x2;
    pdf1                 = rhs.pdf1;
    pdf2                 = rhs.pdf2;
    scalePDF             = rhs.scalePDF;
    id1                  = rhs.id1;
    id2                  = rhs.id2;
    mu                   = rhs.mu;
    avg_mu               = rhs.avg_mu;
    actual_mu            = rhs.actual_mu;
    beamSpotX            = rhs.beamSpotX;
    beamSpotY            = rhs.beamSpotY;
    beamSpotZ            = rhs.beamSpotZ;
    beamSpotXerr         = rhs.beamSpotXerr;
    beamSpotYerr         = rhs.beamSpotYerr;
    beamSpotZerr         = rhs.beamSpotZerr;
    beamSpotXYerr        = rhs.beamSpotXYerr;
    beamTiltXZ           = rhs.beamTiltXZ;
    beamTiltYZ           = rhs.beamTiltYZ;
    privtx               = rhs.privtx;
    privtxErrX           = rhs.privtxErrX;
    privtxErrY           = rhs.privtxErrY;
    privtxErrZ           = rhs.privtxErrZ;
    pileupvtx            = rhs.pileupvtx;
    privtxCorXY          = rhs.privtxCorXY;
    privtxCorXZ          = rhs.privtxCorXZ;
    privtxCorYZ          = rhs.privtxCorYZ;
    privtxFitQ           = rhs.privtxFitQ;
    phvtx                = rhs.phvtx;
    phvtxErrZ            = rhs.phvtxErrZ;
    phvtxFitQ            = rhs.phvtxFitQ;
    pileUp               = rhs.pileUp;
    pileUp_sysUp         = rhs.pileUp_sysUp;
    pileUp_sysDown       = rhs.pileUp_sysDown;
    PRWHash              = rhs.PRWHash;
    mcWeight             = rhs.mcWeight;
    xsec                 = rhs.xsec;
    xsec_uncert          = rhs.xsec_uncert;
    nEvents              = rhs.nEvents;
    runLumi              = rhs.runLumi;
    isMC                 = rhs.isMC;
    stream               = rhs.stream;
    eventCleaning        = rhs.eventCleaning;
    evtL1Trigger         = rhs.evtL1Trigger;
    evtHLTTrigger        = rhs.evtHLTTrigger;
    evtTriggerPrescale   = rhs.evtTriggerPrescale;
    met_L1               = rhs.met_L1;
    met_HLT_pufit        = rhs.met_HLT_pufit;
    met_HLT_cell         = rhs.met_HLT_cell;
    leadJetPt_L1         = rhs.leadJetPt_L1;
    leadJetPt_HLT        = rhs.leadJetPt_HLT;
    isVgammaOverlap      = rhs.isVgammaOverlap;
    sumOfWeightsPerEvent = rhs.sumOfWeightsPerEvent;
    sherpaNJetWeight     = rhs.sherpaNJetWeight;
    globalDiLepTrigSF    = rhs.globalDiLepTrigSF;
    globalMultiLepTrigSF = rhs.globalMultiLepTrigSF;
    globalPhotonTrigSF   = rhs.globalPhotonTrigSF;
    GenHt                = rhs.GenHt;
    GenMET               = rhs.GenMET;
    MCCorr               = rhs.MCCorr;
  }

  return *this;

}
// -------------------------------------------------------------------------------- //
void EventVariable::addEvtCleaning( EventCleaning var, bool value )
{

  this->eventCleaning.insert( std::pair<EventCleaning,bool>(var,value) );

}
// -------------------------------------------------------------------------------- //
float EventVariable::getSherpaVjetsNjetsWeight(int var)
{

  if( var!= 0 ){
    std::cout << "<EventVariable::getSherpaVjetsNjetsWeight> WARNING no systematics for this method, only nominal is filled!" << std::endl;
  }

  return this->sherpaNJetWeight;

}
// -------------------------------------------------------------------------------- //
bool EventVariable::passEvtCleaning( EventCleaning var )
{

  auto it = this->eventCleaning.find( var );

  if( it == this->eventCleaning.end() ){
    std::cout << "<EventVariable::passEvtCleaning> WARNING Could not find event cleaning cut: " << var << ". Returning true!" << std::endl;
    return true;
  }
  else{
    return it->second;
  }

}
// -------------------------------------------------------------------------------- //
bool EventVariable::getL1EvtTrigDec(const std::string trigChainName)
{

  auto it = this->evtL1Trigger.find(trigChainName);

  if( it != this->evtL1Trigger.end() ){
    if( it->second ) return true;
    else             return false;
  }
  else{
    return false;
  }


}
// -------------------------------------------------------------------------------- //
bool EventVariable::getHLTEvtTrigDec(const std::string trigChainName)
{

  auto it = this->evtHLTTrigger.find(trigChainName);

  if( it != this->evtHLTTrigger.end() ){
    if( it->second ) return true;
    else             return false;
  }
  else{
    return false;
  }

}
// -------------------------------------------------------------------------------- //
float EventVariable::getTriggerPrescale(const std::string trigChainName)
{

  auto it = this->evtTriggerPrescale.find(trigChainName);

  if( it != this->evtTriggerPrescale.end() ){
    return it->second;
  }
  else{
    return -999.;
  }

}
// -------------------------------------------------------------------------------- //
float EventVariable::getGlobalDiLepTrigSF(const TString &sys)
{
  auto it = this->globalDiLepTrigSF.find(sys);

  if( it != this->globalDiLepTrigSF.end() ){
    return it->second;
  }
  else{
    return 1.;
  }
}
// -------------------------------------------------------------------------------- //
float EventVariable::getGlobalMultiLepTrigSF(const TString &sys)
{
  auto it = this->globalMultiLepTrigSF.find(sys);

  if( it != this->globalMultiLepTrigSF.end() ){
    return it->second;
  }
  else{
    return 1.;
  }
}
// -------------------------------------------------------------------------------- //
float EventVariable::getGlobalPhotonTrigSF(const TString &sys)
{
  auto it = this->globalPhotonTrigSF.find(sys);

  if( it != this->globalPhotonTrigSF.end() ){
    return it->second;
  }
  else{
    return 1.;
  }
}
