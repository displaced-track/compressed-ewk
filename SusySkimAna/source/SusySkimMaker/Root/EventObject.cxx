#include "SusySkimMaker/EventObject.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/TreeMaker.h"

#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "PMGTools/PMGTruthWeightTool.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/MsgLog.h"
#include "PathResolver/PathResolver.h"
#include "AsgTools/StatusCode.h"

#include <sstream>
#include <math.h>

//
EventObject::EventObject() : m_grl(0),
                             m_pileupReweightingTool(0),
                             m_photonVtxSel(0),
                             m_susyCrossSection(0),
                             m_numEvtHist(0),
                             m_nEvents(-1.0),
                             m_isData(false),
                             m_xAODAccess(false),
                             m_dsid(-1),
                             m_runNumber(-1),
                             m_numEvtPath(""),
                             m_xsecPathName(""),
                             m_disablePRW(false),
                             m_ignoreGRL(false),
                             m_writeLHE3(false),
                             m_writeMVAPhotonVtx(false),
                             m_usePMGXsecTool(true),
			     m_PMGTruthWeightTool(0),
			     m_tightEvtCleaningNJets(1),
			     m_doVGammaOR(false)
{

  m_eventMap.clear();
  m_grlVec.clear();
  m_prwFiles.clear();
  m_lumiCalcFiles.clear();
  m_actualMuList.clear();
  m_systSet.clear();

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::init(TreeMaker*& treeMaker,float nEvents,bool isData, bool isAf2)
{

  const char* APP_NAME = "EventObject";

  for( auto& sysName : treeMaker->getSysVector() ){

    //
    EventVariable* evt  = new EventVariable();

    // Save into map
    m_eventMap.insert( std::pair<TString,EventVariable*>(sysName,evt) );

    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);

    // Don't write it out
    if( !sysTree ) continue;
    else{
      MsgLog::INFO("EventObject::init","Adding a branch event to skim tree: %s ",sysTree->GetName() );
      std::map<TString,EventVariable*>::iterator evtItr = m_eventMap.find(sysName);
      sysTree->Branch("event",&evtItr->second);
    }

  }

  m_nEvents    = nEvents;
  m_isData     = isData;
  m_xAODAccess = true;

  // All tools
  CHECK( init_tools( isData, isAf2 ) );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::init_tools(bool isData, bool isAf2, bool disablePRW)
{

  const char* APP_NAME = "EventObject";

  // Maybe useful for future tool
  (void)isAf2;

  CentralDB::retrieve(CentralDBFields::GRL,m_grlVec);
  CentralDB::retrieve(CentralDBFields::IGNOREGRL,m_ignoreGRL);
  CentralDB::retrieve(CentralDBFields::XSEC,m_xsecPathName);
  CentralDB::retrieve(CentralDBFields::NUMEVT,m_numEvtPath);
  CentralDB::retrieve(CentralDBFields::DISABLEPRW,m_disablePRW);
  CentralDB::retrieve(CentralDBFields::WRITEMVAPHOTONVTX,m_writeMVAPhotonVtx);
  CentralDB::retrieve(CentralDBFields::USEPMGXSECTOOL,m_usePMGXsecTool);
  CentralDB::retrieve(CentralDBFields::WRITELHE3,m_writeLHE3); 
  CentralDB::retrieve(CentralDBFields::EMULATETIGHTEVTCLEANINGPFNJET,m_tightEvtCleaningNJets);
  CentralDB::retrieve(CentralDBFields::DOVGAMMAOR,m_doVGammaOR);

  // Initialize the PRW, and load in
  // the lists if not done already
  if( !disablePRW ){
    this->getPRWProfileList();
    this->getLumiCalcList(isData);
    CHECK( init_pileUpReweightTool() );
  }

  // Tools needed for data
  if(isData){
    CHECK( init_GRL() );
  }
  // Tools for MC
  else{
    CHECK( init_Xsec() );
    CHECK( init_numEvts("weighted__AOD") );
     if( !disablePRW ){
       CHECK( fillSysVector() );
     }
     // Vgamma OR Tool
     if (m_doVGammaOR) {
       m_vgammaOR = new VGammaORTool("vgamma_OR");
       CHECK( m_vgammaOR->initialize() );
       m_vgammaOR->setProperty("photon_pT_cuts", std::vector<float>({7}) );
     }
  }

  // Initialize PMG Tools for retrieving LHE3 weights with theoretical variation
  if(m_writeLHE3 && !m_isData){
    CHECK( init_PMGTruthWeightTool() );
  }

  // Photon vertex classification
  // Protected so it's only initialized if in xAODAccess mode
  if( m_writeMVAPhotonVtx && m_xAODAccess ){
    m_photonVtxSel = new CP::PhotonVertexSelectionTool("PhotonVertexSelector");
    CHECK( m_photonVtxSel->initialize() );
  }

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::init_numEvts(TString histName)
{

  // TODO: Disabled for now. Not used as far as im aware, remove? Or keep for future flexibility?
  return StatusCode::SUCCESS;

  TFile* file = TFile::Open( gSystem->ExpandPathName( m_numEvtPath.Data() ),"READ");

  if( !file->IsOpen() ){
    std::cout << "<EventObject::init_numEvts> ERROR Cannot open file: " << m_numEvtPath << std::endl;
    return StatusCode::FAILURE;
  }

  // Try to get histogram
  m_numEvtHist = (TH1F*)file->Get( histName.Data() );
  //
  if( !m_numEvtHist ){
    std::cout << "<EventObject::init_numEvts> ERROR Could not retrieve histogram weighted__AOD" << std::endl;
    return StatusCode::FAILURE;
  }

  // Disattach from TFile
  m_numEvtHist->SetDirectory(0);

  file->Close();

  std::cout << "<EventObject::init_numEvts> Initialized number of event histogram..." << std::endl;

  // Return gracefully
  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------- //
StatusCode EventObject::init_Xsec()
{

  // Path to xsec file
  //  ** DO NOT allow an empty string fed to PathResolverFindCalibFile!! **
  //     It causes a weird error that is only visible in the CI!!
  std::string path = m_xsecPathName != "" ? PathResolverFindCalibFile( m_xsecPathName.Data() ) : "";

  // If PathResolver fails, try to just expand the full path
  if( path == "" ){
    path = gSystem->ExpandPathName( m_xsecPathName.Data() );
  }

  // Initialize cross section tool
  if(m_usePMGXsecTool){
    if(path=="")
      m_susyCrossSection = new SUSY::CrossSectionDB(); // usePMGTools=true, default xsec file path is called
    else
      m_susyCrossSection = new SUSY::CrossSectionDB( path,false,false,true); // usePathResolver=false, usePMGTools=true
  }
  else
    m_susyCrossSection = new SUSY::CrossSectionDB( path,false,false,false); // usePathResolver=false, usePMGTools=false

  if( !m_susyCrossSection ){
    Info("EventObject::init_Xsec","Cannot initialize cross section tool");
    return StatusCode::FAILURE;
  }

  std::cout << "<EventObject::init_Xsec> Initialized SUSY::CrossSectionDB tool ... " << std::endl;

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::init_GRL()
{

  const char* APP_NAME = "EventObject";

  for( auto& grl : m_grlVec ){
    Info("EventObject::init_GRL","Initializing GRL tool with input %s", grl.c_str() );
  }

  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  CHECK( m_grl->setProperty( "GoodRunsListVec", m_grlVec ) );
  CHECK( m_grl->initialize() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::init_pileUpReweightTool()
{

  const char* APP_NAME = "EventObject";

  if (asg::ToolStore::contains<CP::PileupReweightingTool>("ToolSvc.PrwTool") ){
    MsgLog::INFO("EventObject::init_pileUpReweightTool","Found a pile-up reweighting tool from the ToolStore! Using it!");
    m_pileupReweightingTool = asg::ToolStore::get<CP::PileupReweightingTool>("ToolSvc.PrwTool");
    return StatusCode::SUCCESS;
  }

  // Initialize our own
  MsgLog::INFO("EventObject::init_pileUpReweightTool","Initializing the PRW tool...");

  m_pileupReweightingTool = new CP::PileupReweightingTool( "PrwTool" );
  m_pileupReweightingTool->expert()->SetUnrepresentedDataAction(2,1.0);

  CHECK( m_pileupReweightingTool->setProperty("ConfigFiles", m_prwFiles ) );
  CHECK( m_pileupReweightingTool->setProperty("LumiCalcFiles", m_lumiCalcFiles) );
  CHECK( m_pileupReweightingTool->setProperty("DataScaleFactor",1.0/1.03) );
  CHECK( m_pileupReweightingTool->initialize() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::init_PMGTruthWeightTool()
{

  if (asg::ToolStore::contains<PMGTools::PMGTruthWeightTool>("ToolSvc.PMGTruthWeightTool") ){
    MsgLog::INFO("EventObject::init_PMGTruthWeightTool","Found a PMGTruthWeightTool from the ToolStore! Using it!");
    m_PMGTruthWeightTool = asg::ToolStore::get<PMGTools::PMGTruthWeightTool>("ToolSvc.");
    return StatusCode::SUCCESS;
  }

  // Initialize our own

  // VERBOSE = 1, DEBUG = 2, INFO = 3, WARNING = 4, ERROR = 5
  int toolsMsgLvl = (int) MSG::INFO;
  CentralDB::retrieve(CentralDBFields::PMGTWTOOLSVERBOSITY, toolsMsgLvl);

  MsgLog::INFO("EventObject::init_PMGTruthWeightTool","Initializing the PMGTruthWeightTool...");
  m_PMGTruthWeightTool = new PMGTools::PMGTruthWeightTool( "PMGTruthWeightTool" );
  m_PMGTruthWeightTool->setProperty("OutputLevel", toolsMsgLvl);
  m_PMGTruthWeightTool->initialize();

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode EventObject::fillSysVector()
{

  if( !m_pileupReweightingTool ){
    MsgLog::ERROR("EventObject::fillSysVector","No valid PRW tool!!");
    return StatusCode::FAILURE;
  }

  const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics();

  //
  for(const auto& systSet : CP::make_systematics_vector(recommendedSystematics)) {
    for (const auto& sys : systSet) {

      // Pileup reweighting systematic variations
      if( m_pileupReweightingTool->isAffectedBySystematic(sys) ){
        CP::SystematicSet localSys;
        localSys.insert(sys);
        m_systSet.push_back(localSys);
      }
    }
  }

  // Finally add in nominal
  CP::SystematicSet defaultSet;
  m_systSet.push_back(defaultSet);

  return StatusCode::SUCCESS;


}
// ------------------------------------------------------------------------- //
bool EventObject::checkPRWInputHistogram(unsigned int channelNumber, unsigned int periodNumber)
{

  // Using nominal only
  if( !m_pileupReweightingTool ){
    std::cout << "<EventObject::checkPRWInputHistogram> No PRW tool, call setPileUpReweightToolRun( runNumber ) " << std::endl;
    return false;
  }

  //
  TH1* hist = m_pileupReweightingTool->expert()->GetInputHistogram(channelNumber,periodNumber);

  if( hist ) return true;
  else       return false;

}
// ------------------------------------------------------------------------- //
void EventObject::fillEventContainer(const xAOD::EventInfo* eventInfo,
                                     const xAOD::JetContainer* jets,
                                     const xAOD::MuonContainer* muons,
                                     const xAOD::TruthEventContainer* truthEvents,
                                     const xAOD::VertexContainer* primVertex,
                                     const xAOD::PhotonContainer* photons,
                                     std::map<TString,bool> evtL1Trigger,
                                     std::map<TString,bool> evtHLTTrigger,
                                     std::map<TString,float> evtTriggerPrescale,
                                     float met_L1,
                                     float met_HLT_pufit,
                                     float met_HLT_cell,
                                     float leadJetPt_L1,
                                     float leadJetPt_HLT,
                                     float sherpaVjetsWeight,
                                     const std::map<TString,float> &globalDiLepTrigSF,
                                     const std::map<TString,float> &globalMultiLepTrigSF,
                                     const std::map<TString,float> &globalPhotonTrigSF,
                                     int pdgid1,int pdgid2,
                                     std::string sys_name)
{

  std::map<TString,EventVariable*>::iterator it = m_eventMap.find(sys_name);
  if( it==m_eventMap.end() ){
    std::cout << "<EventObject::fillEventContainer> ERROR Request to get event for unknown systematic: " << sys_name << std::endl;
    return;
  }

  bool isVgammaOverlap = false;
  if (!m_isData && m_doVGammaOR) {
    m_vgammaOR->inOverlap(isVgammaOverlap);
  }

  bool isSimulation = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );

  //
  unsigned int channelNumber = 999;
  unsigned int runNumber     = 999;
  float mcEvtWeight          = 1.0;
  float genHt                = 0;
  float genMET                = 0;

  // MC based info
  if(isSimulation){
    channelNumber = eventInfo->mcChannelNumber();

    mcEvtWeight   = eventInfo->mcEventWeight();
    genHt         = cacc_GenFiltHT.isAvailable( *eventInfo ) ? cacc_GenFiltHT( *eventInfo ) / 1000.0 : -1.0;
    genMET         = cacc_GenFiltMET.isAvailable( *eventInfo ) ? cacc_GenFiltMET( *eventInfo ) / 1000.0 : -1.0;
  }

  // Run number
  runNumber = eventInfo->runNumber();

  // Log
  if (m_runNumber<0) m_runNumber = runNumber;
  if (m_dsid<0)      m_dsid      = channelNumber;


  // Fill PDFInfo for MC
  fillPDFInfo(truthEvents, it->second, !isSimulation);

  // Data event cleaning cuts
  bool passLAr   = true;
  bool passTile  = true;
  bool passIE    = true;
  bool passSCT   = true;

  if( !isSimulation ){
    if( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)  ) passLAr   = false;
    if( (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error) ) passTile  = false;
    if( (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error)  ) passSCT   = false;
    if( (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))              ) passIE    = false;
  }

  // Event cleaning cuts
  it->second->addEvtCleaning( EventVariable::EventCleaning::LAr, passLAr );
  it->second->addEvtCleaning( EventVariable::EventCleaning::Tile, passTile );
  it->second->addEvtCleaning( EventVariable::EventCleaning::IncompleteEvt, passIE );
  it->second->addEvtCleaning( EventVariable::EventCleaning::SCT, passSCT );
  it->second->addEvtCleaning( EventVariable::EventCleaning::PrimVtx, passGoodVtx(primVertex));
  it->second->addEvtCleaning( EventVariable::EventCleaning::BadJet, passBadJet(jets));
  it->second->addEvtCleaning( EventVariable::EventCleaning::BadMuon, passBadMuon(muons));
  it->second->addEvtCleaning( EventVariable::EventCleaning::CosmicMuon, passCosmicMuon(muons));
  it->second->addEvtCleaning( EventVariable::EventCleaning::GRL, passGRL(eventInfo->runNumber(), eventInfo->lumiBlock()) );
  it->second->addEvtCleaning( EventVariable::EventCleaning::EvtLooseBadJet, passEvtBadJet(eventInfo, true));
  it->second->addEvtCleaning( EventVariable::EventCleaning::EvtTightBadJet, passEvtBadJet(eventInfo, false));
  it->second->addEvtCleaning( EventVariable::EventCleaning::EmulEvtTightBad1Jet, passEmulEvtBadNJet(eventInfo, 1));
  it->second->addEvtCleaning( EventVariable::EventCleaning::EmulEvtTightBad2Jet, passEmulEvtBadNJet(eventInfo, 2));

  it->second->runNumber         = runNumber;
  it->second->randomRunNumber   = getRandomRunNumber(eventInfo);
  it->second->runLumi           = GetRunLumi(eventInfo);
  it->second->evtNumber         = eventInfo->eventNumber();
  it->second->lumiBlock         = eventInfo->lumiBlock();
  it->second->dsid              = channelNumber;
  it->second->timestamp         = eventInfo->timeStamp();
  it->second->beampos           = TVector3(eventInfo->beamPosX(),eventInfo->beamPosY(),eventInfo->beamPosZ());
  it->second->isMC              = isSimulation;
  it->second->stream            = isSimulation ? EventVariable::Stream::Simulation : EventVariable::Stream::Data;
  it->second->mcWeight          = mcEvtWeight;
  it->second->GenHt             = genHt;
  it->second->GenMET            = genMET;
  it->second->isVgammaOverlap   = isVgammaOverlap;

  // Cross sections
  unsigned int finalState = 0;
  // Retrieve SUSY final state from decorator set at derivation level if available
  if (cacc_SUSYFinalState.isAvailable(*eventInfo)){
    finalState = cacc_SUSYFinalState(*eventInfo);
  }
  else if(pdgid1!=0 && pdgid2!=0){
      finalState = SUSY::finalState(pdgid1,pdgid2);
  }

  // Cross section
  //   Note that the finalState is ignored in the getXsec function when m_usePMGXsecTool is true
  //   i.e. FS=0 cross section and its uncertainty will retrieved,
  //   since PMGTool only stores the FS=0 xsec right now. (23 Apr 2019 Shion)
  it->second->xsec        = getXsec(channelNumber,finalState,XSEC_INFO::ALL);
  it->second->xsec_uncert = getXsec(channelNumber,finalState,XSEC_INFO::UNCERT);
  it->second->FS          = finalState;

  // Pile-up information
  it->second->mu              = GetCorrectedAverageInteractionsPerCrossing(eventInfo);
  it->second->avg_mu          = GetAvgInteractionsPerCrossing(eventInfo);
  it->second->actual_mu       = GetActualInteractionsPerCrossing(eventInfo);
  it->second->PRWHash         = getPileupWeightHash(eventInfo);

  //
  fillPileUpWeights(eventInfo,it->second);

  // Addition information
  it->second->nVtx       = getNVtx(primVertex,2);
  it->second->numHSJets  = getNumHSJets(jets);

  // Number of events. Disabled for now
  it->second->nEvents  = 1.0; // getNumEvents(channelNumber);

  // Event trigger bits / SFs
  it->second->evtL1Trigger   = evtL1Trigger;
  it->second->evtHLTTrigger  = evtHLTTrigger;
  it->second->evtTriggerPrescale = evtTriggerPrescale;

  // Trigger-level values for MET and jet pT from emulation of Higgsino triggers
  it->second->met_L1 = met_L1;
  it->second->met_HLT_pufit = met_HLT_pufit;
  it->second->met_HLT_cell = met_HLT_cell;
  it->second->leadJetPt_L1 = leadJetPt_L1;
  it->second->leadJetPt_HLT = leadJetPt_HLT;

  // beamspot

  it->second->beamSpotX = eventInfo->beamPosX();
  it->second->beamSpotY = eventInfo->beamPosY();
  it->second->beamSpotZ = eventInfo->beamPosZ();
  it->second->beamSpotXerr = eventInfo->beamPosSigmaX();
  it->second->beamSpotYerr = eventInfo->beamPosSigmaY();
  it->second->beamSpotZerr = eventInfo->beamPosSigmaZ();
  it->second->beamSpotXYerr = eventInfo->beamPosSigmaXY();
  it->second->beamTiltXZ = eventInfo->beamTiltXZ();
  it->second->beamTiltYZ = eventInfo->beamTiltYZ();

  // Photon classified vertices
  fillPhotonVtxMVA(it->second,photons);

  // Primary vertex
  fillPrimaryVertex(it->second,primVertex);

  // Pileup vertex
  fillPileupVertex(it->second,primVertex);

  // Sherpa V+jets weights
  it->second->sherpaNJetWeight = sherpaVjetsWeight;

  // Global Trigger SF
  it->second->globalDiLepTrigSF    = globalDiLepTrigSF;
  it->second->globalMultiLepTrigSF = globalMultiLepTrigSF;
  it->second->globalPhotonTrigSF   = globalPhotonTrigSF;

  //
  it->second->sumOfWeightsPerEvent = m_sumOfWeightsPerEvent;

  // LHE3Weights
  if(m_PMGTruthWeightTool){
    for(auto name : m_PMGTruthWeightTool->getWeightNames() ){
      double mcEvtWeight_var = m_PMGTruthWeightTool->getWeight(name); // LHE weight (nominal: 1.0)

      TString sysName = name;
      //
      // Note:
      //   We used to apply a clean-up on the variation name since it contains many inappropriate characters (space, =, period, comma etc.)
      //   which is stopped now since the branch naming will be inconsistent with ones in CutBookKeppers otherwise, which is found to be more problematic.
      //
      sysName.ReplaceAll(" ","").ReplaceAll("=","").ReplaceAll(".","p").ReplaceAll(",","").ReplaceAll(":","");
      //

      // Treat as systematics: SF = weight with respect to the nominal
      double sf = mcEvtWeight_var/mcEvtWeight;
      it->second->MCCorr.addSF("LHE3Weight",sysName,sf);

      // Register the varied event weight as dataEff (in order to be consistent with the SF definition)
      it->second->MCCorr.addEff("LHE3Weight",sysName,mcEvtWeight,mcEvtWeight_var);
    }
  }

}
// ------------------------------------------------------------------------- //
void EventObject::fillEventContainerTruth(const xAOD::EventInfo* eventInfo,
                                          const xAOD::TruthParticleContainer* truthParticles,
                                          const xAOD::TruthEventContainer* truthEvents,
                                          std::string sys_name /*=""*/)
{

 std::map<TString,EventVariable*>::iterator it = m_eventMap.find(sys_name);

  if(it==m_eventMap.end()){
    std::cout<< "EventObject::fillEventContainer::Request to get event for unknown systematic: " << sys_name << std::endl;
    return;
  }

  bool isSimulation = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

  unsigned int channelNumber = 999;
  float mcEvtWeight          = 1.0;

  if(isSimulation){
    channelNumber   = eventInfo->mcChannelNumber();
    mcEvtWeight     = eventInfo->mcEventWeight();
  }

  // HACK FOR BUGGY TRUTH SAMPLES
  // STORED AS RUNNUMBER..
  if( channelNumber <= 0 ) channelNumber = eventInfo->runNumber();

  // Event cleaning cuts
  it->second->addEvtCleaning( EventVariable::EventCleaning::LAr, true );
  it->second->addEvtCleaning( EventVariable::EventCleaning::Tile, true );
  it->second->addEvtCleaning( EventVariable::EventCleaning::IncompleteEvt, true );
  it->second->addEvtCleaning( EventVariable::EventCleaning::SCT, true );
  it->second->addEvtCleaning( EventVariable::EventCleaning::PrimVtx, true);
  it->second->addEvtCleaning( EventVariable::EventCleaning::GRL,true);
  it->second->addEvtCleaning( EventVariable::EventCleaning::BadMuon, true);
  it->second->addEvtCleaning( EventVariable::EventCleaning::BadJet, true);
  it->second->addEvtCleaning( EventVariable::EventCleaning::EvtLooseBadJet, true);
  it->second->addEvtCleaning( EventVariable::EventCleaning::EvtTightBadJet, true);

  unsigned int finalState = 0;
  int pdgid1=0;
  int pdgid2=0;
  ST::SUSYObjDef_xAOD::FindSusyHardProc(truthParticles,pdgid1,pdgid2);
  if( pdgid1!=0 && pdgid2!=0 ){
    finalState = SUSY::finalState(pdgid1,pdgid2);
  }

  it->second->runNumber   = eventInfo->runNumber();
  it->second->evtNumber   = eventInfo->eventNumber();
  it->second->runLumi     = 1.0;
  it->second->dsid        = channelNumber;
  it->second->isMC        = isSimulation;
  it->second->stream      = EventVariable::Stream::Truth;
  it->second->mcWeight    = mcEvtWeight;

  // Cross section
  //   Note that the finalState is ignored in the getXsec function when m_usePMGXsecTool is true
  //   i.e. FS=0 cross section and its uncertainty will retrieved,
  //   since PMGTool only stores the FS=0 xsec right now. (23 Apr 2019 Shion)
  it->second->xsec        = getXsec(channelNumber,finalState,XSEC_INFO::ALL);
  it->second->xsec_uncert = getXsec(channelNumber,finalState,XSEC_INFO::UNCERT);
  it->second->FS          = finalState;

  // Pile up
  it->second->pileUp          = 1.0;
  it->second->pileUp_sysUp    = 1.0;
  it->second->pileUp_sysDown  = 1.0;

  // Addition information
  it->second->nVtx = 1;

  // TODO: This needs to be set!
  it->second->sherpaNJetWeight = 1.0;

  it->second->nEvents = getNumEvents(channelNumber);

  fillPDFInfo(truthEvents, it->second);

  // LHE3Weights
  if(m_PMGTruthWeightTool){
    for(auto name : m_PMGTruthWeightTool->getWeightNames() ){
      double mcEvtWeight_var = m_PMGTruthWeightTool->getWeight(name); // LHE weight (nominal: 1.0)

      TString sysName = name;
      //
      // Note:
      //   We used to apply a clean-up on the variation name since it contains many inappropriate characters (space, =, period, comma etc.)
      //   which is stopped now since the branch naming will be inconsistent with ones in CutBookKeppers otherwise, which is found to be more problematic.
      //
      sysName.ReplaceAll(" ","").ReplaceAll("=","").ReplaceAll(".","p").ReplaceAll(",","").ReplaceAll(":","");
      //

      // Treat as systematics: SF = weight with respect to the nominal
      double sf = mcEvtWeight_var/mcEvtWeight;
      it->second->MCCorr.addSF("LHE3Weight",sysName,sf);

      // Register the varied event weight as dataEff (in order to be consistent with the SF definition)
      it->second->MCCorr.addEff("LHE3Weight",sysName,mcEvtWeight,mcEvtWeight_var);
    }
  }

}
// ------------------------------------------------------------------------------------------- //
void EventObject::fillPileUpWeights(const xAOD::EventInfo* eventInfo, EventVariable*& event)
{

  //
  // Loop over all systematic variations, accept only those
  // affecting m_pileupReweightingTool.
  // Depends on the 1up and 1down naming structure (which is pretty stable) to fill branches
  //

  // Only for MC
  if( m_isData ){
    event->pileUp         = 1.0;
    event->pileUp_sysUp   = 1.0;
    event->pileUp_sysDown = 1.0;
    return;
  }

  //
  for( const auto& sys : m_systSet ){

    // Systematic name
    TString sysName = sys.name();

    // Nominal pileup weight
    if( sysName.IsWhitespace() ){
      event->pileUp = getPileUpWeight(eventInfo,sys);
    }

    //
    bool isPileupSys=false;
    for (const auto& s : sys) {
      if( m_pileupReweightingTool->isAffectedBySystematic(s) ){
        isPileupSys=true;
        break;
      }
    }

    if( !isPileupSys ) continue;

    if( sysName.Contains("up") ){
      event->pileUp_sysUp = getPileUpWeight(eventInfo,sys);
    }
    else if( sysName.Contains("down") ){
      event->pileUp_sysDown = getPileUpWeight(eventInfo,sys);
    }
    else{
      MsgLog::WARNING("EventObject::fillPileUpWeights","Unknown systematic variation affecting pileup %s!",sysName.Data() );
    }

  }

}
// ------------------------------------------------------------------------- //
void EventObject::fillPDFInfo(const xAOD::TruthEventContainer* truthEvents,
                              EventVariable* const event,
                              bool fillDefault /*=false*/)
{
  /**
   * Method to retrieve the PDFInfo from TruthEvent and fill it into
   * EventVariable. This should be saved in the same way in TRUTH and
   * SUSY Derivations. Not all samples have it saved, so setting
   * default values in case the auxitems are not existing. For some
   * non-TRUTH samples the information is probably broken, but lets
   * try where it works.
   * If 'fillDefault' is true, only fill default values.
   */

  // Not to be filled for data
  if( m_isData ) return;

  // Only for samples with valid TruthEvents container
  if( !truthEvents ) {
    MsgLog::WARNING("EventObject::fillPDFInfo","Invalid truthEvents! Won't fill anything.");
    fillDefault = true;
  }

  bool auxItemsExist = true;
  if (!fillDefault) {
    try
    {
      xAOD::TruthEventContainer::const_iterator truthE_itr = truthEvents->begin();
      (*truthE_itr)->pdfInfoParameter(event->id1, xAOD::TruthEvent::PDGID1);
      (*truthE_itr)->pdfInfoParameter(event->id2, xAOD::TruthEvent::PDGID2);
      (*truthE_itr)->pdfInfoParameter(event->x1, xAOD::TruthEvent::X1);
      (*truthE_itr)->pdfInfoParameter(event->x2, xAOD::TruthEvent::X2);
      (*truthE_itr)->pdfInfoParameter(event->pdf1, xAOD::TruthEvent::PDF1);
      (*truthE_itr)->pdfInfoParameter(event->pdf2, xAOD::TruthEvent::PDF2);
      (*truthE_itr)->pdfInfoParameter(event->scalePDF, xAOD::TruthEvent::SCALE);

    }
    catch (SG::ExcBadAuxVar& e) {
      auxItemsExist = false;
    }
  }

  if (fillDefault || !auxItemsExist) {
    event->x1        = -1.0;
    event->x2        = -1.0;
    event->pdf1      = -1.0;
    event->pdf2      = -1.0;
    event->scalePDF  = -1.0;
    // better have 0 here because antiparticles can have negative values
    event->id1       = 0;
    event->id2       = 0;
  }

}
// ------------------------------------------------------------------------- //
void EventObject::fillPhotonVtxMVA(EventVariable*& event,const xAOD::PhotonContainer* photonCont)
{

  //
  if( !m_writeMVAPhotonVtx ) return;
  if( !photonCont          ) return;

  xAOD::PhotonContainer photons = *photonCont;

  const xAOD::Vertex* vtx = nullptr;
  xAOD::PhotonContainer signalPhotons (SG::VIEW_ELEMENTS);
  for (auto p : photons){
    if( cacc_passOR.isAvailable(*p) && !cacc_passOR(*p) ) continue;
    if( cacc_signal.isAvailable(*p) && !cacc_signal(*p) ) continue;
    signalPhotons.push_back(p);
 }

  // Only run the classification for events
  // with at least two signal photons
  if( signalPhotons.size()<2 ) return;

  //
  m_photonVtxSel->getVertex(signalPhotons,vtx);
  if( vtx ){
    auto posCov = vtx->covariancePosition();
    event->phvtx.SetXYZ( vtx->x(), vtx->y(), vtx->z() );
    event->phvtxErrZ = TMath::Sqrt(posCov(2,2));
    event->phvtxFitQ = (vtx->chiSquared() / vtx->numberDoF() );
  }


}
// ------------------------------------------------------------------------- //
void EventObject::fillPrimaryVertex(EventVariable*& event,const xAOD::VertexContainer* primVertex)
{

  if( !m_writeMVAPhotonVtx ) return;

  xAOD::Vertex* privtx = 0;

  for( const auto& vx : *primVertex ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
       privtx = vx;
       break;
    }
  }

  if( privtx ){
    auto posCov = privtx->covariancePosition();
    event->privtx.SetXYZ( privtx->x(), privtx->y(), privtx->z() );
    event->privtxErrX = TMath::Sqrt(posCov(0,0));
    event->privtxErrY = TMath::Sqrt(posCov(1,1));
    event->privtxErrZ = TMath::Sqrt(posCov(2,2));
    event->privtxCorXY = TMath::Sqrt(posCov(0,1));
    event->privtxCorYZ = TMath::Sqrt(posCov(1,2));
    event->privtxCorXZ = TMath::Sqrt(posCov(0,2));
    event->privtxFitQ = (privtx->chiSquared() / privtx->numberDoF() );
  }

}
// ------------------------------------------------------------------------- //
void EventObject::fillPileupVertex(EventVariable*& event,const xAOD::VertexContainer* primVertex)
{
  //Fill PileUp vertices
  for( const auto& vx : *primVertex ) {
    if(vx->vertexType() == xAOD::VxType::PileUp){
      TVector3 vtx(vx->x(),vx->y(),vx->z());
      event->pileupvtx.push_back(vtx);
    }
  }

}
// ------------------------------------------------------------------------- //
int EventObject::getNumHSJets(const xAOD::JetContainer* jets)
{

  int numHSJets=0;

  // Only for MC
  if( m_isData ){
    return numHSJets;
  }

  //
  for( const auto& jet : *jets ){

    // Get GhostTruthAssociationLink
    if( jet->isAvailable< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink") && jet->auxdata< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink").isValid() ) {

      //
      auto tjet = *jet->auxdata< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink");

      // Check pT
      if( tjet->pt()>10000.0 ){
    numHSJets++;
      }
    }
  }

  return numHSJets;

}
// ------------------------------------------------------------------------- //
bool EventObject::passBadJet(const xAOD::JetContainer* jets)
{

  bool passBadJet=true;

  // Take the bad decoration directly,
  // Ensure jets have passed overlap removal (should be applied in ST anyways)
  // no other cuts, since they are applied
  // Event veto, therefore once any bad jet is found, break and return
  for( const auto& jet : *jets ){
    if( !jet->auxdata< char >("passOR") ) continue;
    if( jet->auxdata< char >("bad") ){
      passBadJet=false;
      break;
    }
  }

  return passBadJet;

}
// ------------------------------------------------------------------------- //
bool EventObject::passEvtBadJet(const xAOD::EventInfo* eventInfo, bool useLoose)
{

  bool passEvtBadJet=true;

  if(useLoose)
    passEvtBadJet = cacc_evtCleanLooseBad.isAvailable( *eventInfo ) ? cacc_evtCleanLooseBad( *eventInfo ) : true;
  else passEvtBadJet = cacc_evtCleanTightBad.isAvailable( *eventInfo ) ? cacc_evtCleanTightBad( *eventInfo ) : true;

  return passEvtBadJet;

}
// ------------------------------------------------------------------------- //
bool EventObject::passEmulEvtBadNJet(const xAOD::EventInfo* eventInfo, int n_jet)
{

  bool passEmulEvtBadNJet=true;

  if(n_jet == 1)
    passEmulEvtBadNJet = cacc_emulJetCleanTightBad1Jet.isAvailable( *eventInfo ) ? cacc_emulJetCleanTightBad1Jet( *eventInfo ) : true;
  else if (n_jet == 2)
    passEmulEvtBadNJet = cacc_emulJetCleanTightBad2Jet.isAvailable( *eventInfo ) ? cacc_emulJetCleanTightBad2Jet( *eventInfo ) : true;

  return passEmulEvtBadNJet;

}
// ------------------------------------------------------------------------- //
bool EventObject::passBadMuon(const xAOD::MuonContainer* muons)
{

  bool passBadMuon=true;

  // Should be applied *BEFORE* overlap removal
  // Event veto, therefore once any bad muon is found, break and return
  for( const auto& mu : *muons ){
    if( !mu->auxdata< char >("baseline") ) continue;
    if( mu->auxdata< char >("bad") ){
      passBadMuon=false;
      break;
    }
  }

  return passBadMuon;

}
// ------------------------------------------------------------------------- //
bool EventObject::passCosmicMuon(const xAOD::MuonContainer* muons)
{

  bool passCosmicMuon=true;

  // Should be applied *AFTER* overlap removal
  for( const auto& mu : *muons ){
    if( !mu->auxdata< char >("passOR")  ) continue;
    if( mu->auxdata< char >("cosmic") ){
      passCosmicMuon=false;
      break;
    }
  }

 return passCosmicMuon;

}
// ------------------------------------------------------------------------- //
bool EventObject::passGoodVtx(const xAOD::VertexContainer* vertices)
{

  /**
   * Method to check if the primary vertex exists. Note we merely request that the PV
   * exists and don't cut on its number of tracks (however a PV is formed with at least
   * two tracks, so requesting a PV to exist is asking for 2 tracks from it).
   *
   * Taken from recommendations here:
   *   => https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/InDetTrackingPerformanceGuidelines#Vertexing
   */

  for( const auto& vx : *vertices ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx) return true;
  }

  // Didn't find any PV
  return false;

}
// ------------------------------------------------------------------------- //
int EventObject::getNVtx(const xAOD::VertexContainer* vertices, unsigned int nTracks)
{

  /*
   * Method to get the number of good vertices, where a good vertex is defined
   * to have nTracks or more associated to it
   */

  int nGoodVertex = 0;

  // Check vertex has nTracks associated to it or more
  for( const auto& vx : *vertices ) {
    if( vx->nTrackParticles() >= nTracks ) nGoodVertex++;
  }

  return nGoodVertex;

}
// ------------------------------------------------------------------------- //
bool EventObject::passGRL(unsigned int runN, unsigned int lumiB)
{

  /**
   * Method to check if event passes GRL, returns true
   * when running over simulation
   */

  // Means we are running over MC
  if ( !m_grl || m_ignoreGRL ) return true;

  return m_grl->passRunLB(runN,lumiB);

}
// --------------------------------------------------------------------------------------- //
float EventObject::getNumEvents(unsigned int channelNumber,bool cache)
{

  float nEvents = 1.0;

  // Phasing this method out, return completely unweighted events
  return nEvents;

  // Data
  if( !m_numEvtHist ) return nEvents;

  // MC
  if(m_nEvents>0 && cache){
    return m_nEvents;
  }
  else{

    // User didn't initalize (should abort if initialization failed...)
    if( !m_numEvtHist ){
      //
      init_numEvts();

      // Still doesn't exist...
      if( !m_numEvtHist ){
        std::cout << "<EventObject::getNumEvents> ERROR Failed to initialize number of events histogram!" << std::endl;
        abort();
      }
     }

    std::stringstream ss;
    ss << channelNumber;
    TString bin = ss.str();

    // Get number of events from histogram
    nEvents = m_numEvtHist->GetBinContent( m_numEvtHist->GetXaxis()->FindBin( bin.Data() ) );

    // Cache number of events for later use
    m_nEvents = nEvents;

    // Return
    return nEvents;

  }

}
// -------------------------------------------------------------------------------------------- //
float EventObject::GetCorrectedAverageInteractionsPerCrossing(const xAOD::EventInfo* eventInfo)
{

  if( !m_pileupReweightingTool || m_disablePRW ) return 1.0;

  // use average mu for data 15-16 and average mu for data 17-18
  // no distinction required for MC als actual and average mu are identical in simulation
  if( m_isData && m_runNumber > 320000){
    return m_pileupReweightingTool->getCorrectedActualInteractionsPerCrossing( *eventInfo, true );
  }
  else{
    return m_pileupReweightingTool->getCorrectedAverageInteractionsPerCrossing( *eventInfo, true );
  }

}
// -------------------------------------------------------------------------------------------- //
float EventObject::GetAvgInteractionsPerCrossing(const xAOD::EventInfo* eventInfo)
{

 if( !m_pileupReweightingTool || m_disablePRW ) return 1.0;

 return m_pileupReweightingTool->getCorrectedAverageInteractionsPerCrossing( *eventInfo, true );

}
// -------------------------------------------------------------------------------------------- //
float EventObject::GetActualInteractionsPerCrossing(const xAOD::EventInfo* eventInfo)
{

 if( !m_pileupReweightingTool || m_disablePRW ) return 1.0;

 return m_pileupReweightingTool->getCorrectedActualInteractionsPerCrossing( *eventInfo, true );

}
// ------------------------------------------------------------------------- //
unsigned long long EventObject::getPileupWeightHash(const xAOD::EventInfo* eventInfo)
{

 // TODO: ***** This is DEF not what should be done *****
 if( m_isData || m_disablePRW ) return 1.0;
 else{
    return m_pileupReweightingTool->getPRWHash( *eventInfo );
 }

}
// ------------------------------------------------------------------------- //
float EventObject::getPileUpWeight(const xAOD::EventInfo* eventInfo , CP::SystematicSet systset)
{

 float pileUpWeight = 1.0;

 if( m_isData ) return pileUpWeight;
 else{

    if( m_pileupReweightingTool->applySystematicVariation( systset ) != CP::SystematicCode::Ok ){
      std::cout<<"Problem with systematic set"<<std::endl;
      abort();
    }

    pileUpWeight = m_pileupReweightingTool->getCombinedWeight( *eventInfo, true );

    return pileUpWeight;

 }

}
// -------------------------------------------------------------------------------------------- //
TString EventObject::makeWeightTree(TString outputDir,TString channelNumber,TString hashName, TString pileupName, int var)
{

  if( !m_pileupReweightingTool ){
    std::cout << "<EventObject::makeWeightTree> No valid PRW tool, please initialize it before calling this function!" << std::endl;
    abort();
  }

  // TODO: Systematic name: "up, down, nom"
  TString outFile = outputDir + "/" + channelNumber + ".root";
  outFile.ReplaceAll(",","_");

  std::cout << "Variations are not implemented at this time, note you passed: " << var << std::endl;

  // Do it!
  // TODO: Switch to control systematics
  //       This function needs to be called each time
  m_pileupReweightingTool->expert()->MakeWeightTree(channelNumber,outFile,hashName,pileupName);

  return outFile;


}
// -------------------------------------------------------------------- //
float EventObject::GetRunLumi(const xAOD::EventInfo* eventInfo)
{

  // A problem with the initialization
  if( !m_pileupReweightingTool ) return -1.0;

  if( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    return 1.0;
  }
  else{
    return m_pileupReweightingTool->GetIntegratedLumi(eventInfo->runNumber(),eventInfo->runNumber());
  }

}
// -------------------------------------------------------------------- //
unsigned int EventObject::getRandomRunNumber(const xAOD::EventInfo* eventInfo)
{

  // Copied from SUSYTools
  if (m_isData) {
    return eventInfo->runNumber();
  }
  if (!cacc_randomrunnumber.isAvailable(*(eventInfo))) {
    std::cout << "<EventObject::getRandomRunNumber> Failed to find RandomRunNumber decoration! You need to call susyObj.ApplyPRWTool() beforehand!" << std::endl;
    return 0;
  }
  return cacc_randomrunnumber(*(eventInfo));

}
// ------------------------------------------------------------------------- //
unsigned int EventObject::getRandomRunNumber(unsigned int DSID,int runNumber,unsigned long long evtNumber)
{

 // Running over data
 if( !m_pileupReweightingTool ) return 1;

  CP::TPileupReweighting* tool = m_pileupReweightingTool->expert();
  if( !tool ){
    Warning("EventObject::getRandomRunNumber","No active PRW tool!");
    return 0;
  }

  // Randomize
  tool->SetRandomSeed(314159*DSID*2718+evtNumber);

  // Done
  return tool->GetRandomRunNumber(runNumber);

}
// ------------------------------------------------------------------------- //
float EventObject::getXsec(unsigned int channelNumber,unsigned int finalState,XSEC_INFO xsec_info)
{

  float xSec = 1.0;

  // Running over data
  if( !m_susyCrossSection ) return xSec;
  if( !m_xsecExists ) return xSec;

  // Retrive FS=0 cross section if PMGTool xsec is called.
  if(m_usePMGXsecTool) finalState = 0;

  // Get cross section from SUSYTools
  SUSY::CrossSectionDB::Process p;
  p = m_susyCrossSection->process(channelNumber,finalState);

  // Set global DB tool
  m_crossSectionDB = p;

  // Get cross section
  float processXsec = m_crossSectionDB.xsect();
  if (processXsec == -1) {
    Warning("EventObject::getXsec","Missing PMG xsec entry, suppressing further warnings and returning 1.0");
    m_xsecExists = false;
    return 1.0;
  }
  if(xsec_info==ALL || xsec_info==XSEC) xSec *= m_crossSectionDB.xsect();
  // Get k-factor
  if(xsec_info==ALL || xsec_info==KFAC) xSec *= m_crossSectionDB.kfactor();
  // Get generator efficiency
  if(xsec_info==ALL || xsec_info==GEFF) xSec *= m_crossSectionDB.efficiency();


  // Outside ALL, rel. uncertainty
  if(xsec_info==UNCERT) xSec = m_crossSectionDB.relunc();

  return xSec;

}
// ------------------------------------------------------------------------- //
const EventVariable* EventObject::getEvt(TString sysName)
{

  std::map<TString,EventVariable*>::iterator it = m_eventMap.find(sysName);

  if(it==m_eventMap.end()){
    std::cout << "<EventObject::getEvt> ERROR Cannot get event for systematic: " << sysName << std::endl;
    return NULL;
  }

  return it->second;

}
// ------------------------------------------------------------------------- //
void EventObject::Reset()
{
  std::map<TString,EventVariable*>::iterator it;
  for(it = m_eventMap.begin(); it != m_eventMap.end(); it++){
    it->second->runNumber = 0.0;
    it->second->evtNumber = 0.0;
    it->second->isMC      = true;
    it->second->mcWeight  = 1.0;
    it->second->xsec      = 0.0;
    it->second->pileUp    = 1.0;
    it->second->nVtx      = 0.0;
    it->second->nEvents   = 1.0;
    it->second->phvtx.SetXYZ( 0.0,0.0,0.0 );
    it->second->phvtxErrZ = 0.0;
    it->second->phvtxFitQ = 0.0;
    it->second->privtx.SetXYZ( 0.0,0.0,0.0 );
    it->second->pileupvtx.clear();
    it->second->privtxErrZ = 0.0;
    it->second->eventCleaning.clear();
    it->second->MCCorr.clear();
  }

}
// ------------------------------------------------------------------------- //
std::vector<std::string> EventObject::getPRWProfileList()
{

  // Already loaded
  if( m_prwFiles.size()>0 ) return m_prwFiles;

  // Otherwise grab it from CentralDB,
  // using the run number to identify
  // which campaign the sample is from
  if( m_runNumber==284500 ){
    CentralDB::retrieve(CentralDBFields::PRWMC16A,m_prwFiles);
    MsgLog::INFO("EventObject::getPRWProfileList","Configuring mc16a PRW with CentralDBFields::PRWMC16A");
  }
  else if( m_runNumber==300000 ){
    CentralDB::retrieve(CentralDBFields::PRWMC16CD,m_prwFiles);
    MsgLog::INFO("EventObject::getPRWProfileList","Configuring mc16cd PRW with CentralDBFields::PRWMC16CD");
  }
  else if( m_runNumber==310000 ){
    CentralDB::retrieve(CentralDBFields::PRWMC16E,m_prwFiles);
    MsgLog::INFO("EventObject::getPRWProfileList","Configuring mc16d PRW with CentralDBFields::PRWMC16E");
  }
  else{
    MsgLog::WARNING("EventObject::getPRWProfileList","No known campaign for %i. Defaulting to the CentralDBFields::PRWGENERAL list!",m_runNumber);
    CentralDB::retrieve(CentralDBFields::PRWGENERAL,m_prwFiles);
  }

  return m_prwFiles;

}
// ------------------------------------------------------------------------- //
std::vector<std::string> EventObject::getLumiCalcList(bool isData)
{

  // Already cached
  if( m_lumiCalcFiles.size()>0 ) return m_lumiCalcFiles;

  //
  // Grab it from CentralDB
  //

  if(isData){
    // In data, load all of the lumicalcs!
    std::vector<std::string> lumicalc_mc16a;
    std::vector<std::string> lumicalc_mc16cd;
    std::vector<std::string> lumicalc_mc16e;

    CentralDB::retrieve(CentralDBFields::LUMICALCMC16A,lumicalc_mc16a);
    CentralDB::retrieve(CentralDBFields::LUMICALCMC16CD,lumicalc_mc16cd);
    CentralDB::retrieve(CentralDBFields::LUMICALCMC16E,lumicalc_mc16e);

    // Push these all into m_lumiCalcFiles
    for(auto lumiCalcStr_a : lumicalc_mc16a){
      m_lumiCalcFiles.push_back(lumiCalcStr_a);
    }
    for(auto lumiCalcStr_cd : lumicalc_mc16cd){
      m_lumiCalcFiles.push_back(lumiCalcStr_cd);
    }
    for(auto lumiCalcStr_e : lumicalc_mc16e){
      m_lumiCalcFiles.push_back(lumiCalcStr_e);
    }

    MsgLog::INFO("EventObject::getLumiCalcList","Configuring lumicalc for data with LumiCalcMC16A, LumiCalcMC16CD, and LumiCalcMC16E");
  }
  else{
    // Rely on the run number dependent settings for
    // mc16a (284500) or mc16c/d (300000).
    // Note that when mc16e exists, we'll use 310000. Based on:
    // https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ConditionsRun1RunNumbers#RUN_2_RUN_3_and_RUN_4
    if( m_runNumber==284500 ){
      CentralDB::retrieve(CentralDBFields::LUMICALCMC16A,m_lumiCalcFiles);
      MsgLog::INFO("EventObject::getLumiCalcList","Configuring mc16a lumicalc with CentralDBFields::LumiCalcMC16A");
    }
    else if( m_runNumber==300000 ){
      CentralDB::retrieve(CentralDBFields::LUMICALCMC16CD,m_lumiCalcFiles);
      MsgLog::INFO("EventObject::getLumiCalcList","Configuring mc16c/d lumicalc with CentralDBFields::LumiCalcMC16CD");
    }
    else if( m_runNumber==310000 ){
      CentralDB::retrieve(CentralDBFields::LUMICALCMC16E,m_lumiCalcFiles);
      MsgLog::INFO("EventObject::getLumiCalcList","Configuring mc16e lumicalc with CentralDBFields::LumiCalcMC16E");
    }
    else{
      MsgLog::WARNING("EventObject::getLumiCalcList","No known campaign for %i. Defaulting to the CentralDBFields::LumiCalc list!",m_runNumber);
    }
  }

  // If the previous retrieve attempts failed, rely on the default list
  // from the pre-mc16 days
  if( m_lumiCalcFiles.size() == 0 ){
    CentralDB::retrieve(CentralDBFields::LUMICALC,m_lumiCalcFiles);
    MsgLog::INFO("EventObject::getLumiCalcList","Configured 'mc15-style' lumicalc with CentralDBFields::LumiCalc");
  }

  //
  return m_lumiCalcFiles;

}
// ------------------------------------------------------------------------- //
std::vector<std::string> EventObject::getActualMuList()
{

  // Already read in
  if( m_actualMuList.size()>0 ) return m_actualMuList;

  //
  CentralDB::retrieve(CentralDBFields::ACTUALMU,m_actualMuList);
  return m_actualMuList;

}

// ------------------------------------------------------------------------- //
void EventObject::Log(MetaData* metaData)
{

  metaData->log("EventObject","Path to cross section files",m_xsecPathName);
  metaData->log("EventObject","Data set ID",m_dsid);
  metaData->log("EventObject","Path to number of events histograms",m_numEvtPath);
  metaData->log("EventObject","Disable PRW?",m_disablePRW);
  metaData->log("EventObject","Disable the GRL tool",m_ignoreGRL);


  // Log all GRL files
  for( auto grl : m_grlVec ){
    metaData->log("EventObject","GRL file",grl);
  }

  // Log all PRW info
  for( auto prw : m_prwFiles ){
    metaData->log("EventObject","Pile up re-weighting files",prw);
  }

  // Lumi calc files
  for( auto lumiCalc : m_lumiCalcFiles ){
    metaData->log("EventObject","Lumi calc files",lumiCalc);
  }

}
// ------------------------------------------------------------------------- //
void EventObject::emulateTightEvtCleaningPF(const xAOD::JetContainer* jets_PF, const xAOD::JetContainer* jets_EMTopo, const xAOD::EventInfo* eventInfo)
{ 
  // to emulate a TightEvtCleaning flag we search the EMTopo counterparts to the
  // n_jet leading PF jets and check if these pass the TightBad criteria as explained
  // in the twiki: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HowToCleanJets2017
  bool good_event = true;
  for(unsigned int i=0; i < (unsigned int)m_tightEvtCleaningNJets; i++){
    // bail out if the jet containers have less than n_jets
    if (jets_PF->size() < i+1 || jets_EMTopo->size() < i+1) break;

    // Note: we could restrict to PF/EMTopo jets passing the JVT and/or OR
    // but it likely doesn't make much of a difference
    // jet->auxdata< char >("passOR") && jet->auxdata< char >("passJvt")

    // search nearest EMTopo jet to this PF jet
    const xAOD::Jet* jet_EMTopo= 0;
    double min_dR = 999;
    for (const xAOD::Jet* jet : *jets_EMTopo){
      double dR = jets_PF->at(i)->p4().DeltaR(jet->p4());
      if (dR  < min_dR && dR < 0.6){
        jet_EMTopo = jet;
        min_dR = dR;
      }
    }
    
    // EMTopo counterpart found, can test if it passes the Tight WP
    if (jet_EMTopo){
      // these requirements should be contained in the decoration but as they are listed
      // on the twiki we apply them again to be sure
      bool passTight=true;
      float fracSamplingMax=-1.0;
      std::vector<float> sumpttrk_vec;
      jet_EMTopo->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumpttrk_vec);
      jet_EMTopo->getAttribute(xAOD::JetAttribute::FracSamplingMax, fracSamplingMax);
      float sumpttrk;
      if (sumpttrk_vec.size() > 0) {
        sumpttrk = sumpttrk_vec[0]; //assuming you are using the default PV selection
      } else {
        sumpttrk = 0;
      }
      if (fabs(jet_EMTopo->eta())<2.4 && fracSamplingMax>0 && (sumpttrk/jet_EMTopo->pt())/fracSamplingMax<0.1)
        passTight=false;

      // check passTight flag and TightBad decoration of nearest EMTopo jet
      if (!passTight && !cacc_jetCleanTightBad(*jet_EMTopo)){
        good_event = false;
      }
    }

    // fill decorator for considering only the leading PF jet
    if (i == 0){
      dec_emulJetCleanTightBad1Jet(*eventInfo) = good_event;
    }
    // fill decorator for considering leading and subleading PF jet
    if (i == 1){
      dec_emulJetCleanTightBad2Jet(*eventInfo) = good_event;
    }    
  }

  // decorate flag on EventInfo if it's not available
  if (!cacc_evtCleanTightBad.isAvailable(*eventInfo)){
    dec_jetCleanTightBad(*eventInfo) = good_event;
  }
}
// ------------------------------------------------------------------------- //
EventObject::~EventObject()
{

  // Clean up memory
  // SUSYTools is cleaning this up, and
  // if we do first will cause SUSYTools
  // to seg fault...
  //if( m_pileupReweightingTool ) delete m_pileupReweightingTool;
  if( m_grl                   ) delete m_grl;
  if( m_susyCrossSection      ) delete m_susyCrossSection;
  if( m_numEvtHist            ) delete m_numEvtHist;

  m_systSet.clear();

}
