################################################################################
# Package: SusySkimMaker
################################################################################

# Declare the package name:
atlas_subdir( SusySkimMaker )

# Extra dependencies based on the build environment:
set( extra_deps )
set( extra_libs )

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Event/xAOD/xAODBase
   PhysicsAnalysis/SUSYPhys/SUSYTools
   PhysicsAnalysis/AnalysisCommon/IsolationSelection
   PhysicsAnalysis/MCTruthClassifier
   PhysicsAnalysis/AnalysisCommon/PileupReweighting
   PhysicsAnalysis/AnalysisCommon/PMGTools
   Reconstruction/Jet/BoostedJetTaggers
   DataQuality/GoodRunsLists
   Event/xAOD/xAODTrigEgamma
   Event/xAOD/xAODCutFlow
   Event/xAOD/xAODCaloEvent
   IFFTruthClassifier
   PhysicsAnalysis/D3PDTools/SampleHandler
   PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection
   InnerDetector/InDetRecTools/InDetTrackSelectionTool
   ${extra_deps}
   Control/CxxUtils
   Event/EventPrimitives
   TruthDecayContainer
   PhysicsAnalysis/AnalysisCommon/PMGOverlapRemovalTools/GammaORTools)



# External dependencies:
find_package( Boost )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO )
find_package( GTest )

if( XAOD_STANDALONE )
   atlas_add_root_dictionary( SusySkimMakerLib SusySkimMakerCintDict
      ROOT_HEADERS Root/LinkDef.h
      EXTERNAL_PACKAGES ROOT )
endif()

# Libraries in the package:
atlas_add_library( SusySkimMakerLib STATIC
   SusySkimMaker/*.h Root/*.cxx ${SusySkimMakerCintDict}
   PUBLIC_HEADERS SusySkimMaker
   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
   LINK_LIBRARIES AsgTools xAODCore xAODEgamma xAODEventInfo
   xAODJet xAODMissingET xAODMuon xAODCaloEvent xAODTau xAODTracking xAODTruth xAODCutFlow
   AssociationUtilsLib PATInterfaces TrigDecisionToolLib PMGToolsLib BoostedJetTaggersLib
   MCTruthClassifierLib JetJvtEfficiencyLib JetSubStructureUtils
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${BOOST_LIBRARIES}
   AthContainers EventPrimitives FourMomUtils xAODBTagging xAODBase
   xAODPrimitives IsolationSelectionLib PileupReweightingLib
   ElectronEfficiencyCorrectionLib ElectronPhotonFourMomentumCorrectionLib
   ElectronPhotonSelectorToolsLib ElectronPhotonShowerShapeFudgeToolLib
   IsolationCorrectionsLib InDetTrackSelectionToolLib
   PhotonEfficiencyCorrectionLib JetSelectorToolsLib
   xAODBTaggingEfficiencyLib MuonEfficiencyCorrectionsLib
   MuonMomentumCorrectionsLib MuonSelectorToolsLib TauAnalysisToolsLib
   JetCPInterfaces JetCalibToolsLib JetInterface JetResolutionLib
   JetUncertaintiesLib JetMomentToolsLib METInterface METUtilitiesLib
   PathResolver TriggerMatchingToolLib TrigConfInterfaces TrigConfxAODLib GammaORToolsLib
   xAODTrigMissingET SUSYToolsLib PMGToolsLib SampleHandler PhotonVertexSelectionLib GoodRunsListsLib IFFTruthClassifierLib TruthDecayContainerLib FTagAnalysisInterfacesLib ${extra_libs} )

foreach( exec run_batch run_download run_downloadPRW run_dumpSampleInfo
         run_makeDirectory run_sampleVal
         moveCondorOutputsToGroupsetStructure )
  atlas_add_executable( ${exec} util/${exec}.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} SusySkimMakerLib )
endforeach()


atlas_install_data( data/* )
atlas_install_data( data/config/* )
atlas_install_data( data/triggers/* )
atlas_install_data( data/PRW/* )
atlas_install_data( data/GRLs/* )
atlas_install_data( scripts/* )
atlas_install_data( scripts/batch/* )

if( EXISTS ${CMAKE_SOURCE_DIR}/SusySkimMaker/.git/ )
  file( GLOB HOOK_SCRIPTS git_hooks/* )
  if ( HOOK_SCRIPTS )
    file(COPY ${HOOK_SCRIPTS} DESTINATION ${CMAKE_SOURCE_DIR}/SusySkimMaker/.git/hooks/)
  endif ( HOOK_SCRIPTS )
endif()
