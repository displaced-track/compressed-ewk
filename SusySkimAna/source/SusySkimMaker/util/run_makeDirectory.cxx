#include <cstdlib>
#include <string>

// ROOT
#include "TChain.h"

// RootCore
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/ArgParser.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/StatusCodeCheck.h"

using namespace std;

int main(int argc, char** argv)
{

  const char* APP_NAME = "run_download";

  TString baseDir       = "";
  TString user          = gSystem->ExpandPathName("$USER");
  TString tag           = "";
  TString deepConfig    = "";
  TString groupSet      = "";

  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if( arg.parse(argv[i],"-d","Path to base directory. Will create three directories inside: trees, skims, and merged") )
      baseDir = argv[++i];
    else if (arg.parse(argv[i],"-u","Grid user name, default is taken as $USER") )
      user = argv[++i];
    else if (arg.parse(argv[i],"-tag","Tag for production you wish to process" ))
      tag = argv[++i];
    else if (arg.parse(argv[i],"-deepConfig","Deep configuration file" ))
      deepConfig = argv[++i];
    else if (arg.parse(argv[i],"-groupSet","Group set name" ) )
      groupSet = argv[++i];
    else
    {
      std::cout << "Unknown argument" << argv[i] << std::endl;
      return 0;
    }
  }

  // Absoutely required for all executables
  CHECK( PkgDiscovery::Init( deepConfig ) );

  SampleHandler* sh = new SampleHandler();
  sh->initialize(baseDir,tag,groupSet,"user.*");

  return 0;

}
