#ifndef SusySkimMaker_Objects_h
#define SusySkimMaker_Objects_h

// M.G. TODO:
// Check the pT and eta thresholds
// applied in this class, with those
// defined in SUSYTools, if the user
// has set them for this class.


/**
 *
 *  Written by Matthew Gignac
 *  Email: matthew.gignac@cern.ch
 *  Institute: UBC/TRIUMF
 *
 *  This class serves as a container for all objects (electron,muons, etc)
 *
 */

#include "SusySkimMaker/EventObject.h"
#include "SusySkimMaker/ElectronObject.h"
#include "SusySkimMaker/MuonObject.h"
#include "SusySkimMaker/TauObject.h"
#include "SusySkimMaker/MetObject.h"
#include "SusySkimMaker/JetObject.h"
#include "SusySkimMaker/TruthObject.h"
#include "SusySkimMaker/TrackObject.h"
#include "SusySkimMaker/PhotonObject.h"

#include "SusySkimMaker/V0Object.h" // JEFF
#include "SusySkimMaker/VSIObject.h" // SICONG

#include "TruthDecayContainer/TruthEvent_Vjets.h"
#include "TruthDecayContainer/TruthEvent_VV.h"
#include "TruthDecayContainer/TruthEvent_VVV.h"
#include "TruthDecayContainer/TruthEvent_TT.h"
#include "TruthDecayContainer/TruthEvent_XX.h"
#include "TruthDecayContainer/TruthEvent_GG.h"

// Forward declarations
class CutFlowTool;

//
typedef std::vector<LeptonVariable*> LeptonVector;

class Objects
{

 public:

  ///
  /// Constructor
  ///
  Objects();

  ///
  /// Destructor
  ///
  ~Objects();

  ///
  /// State for this tool to indiciate whether running over xAODs (GRID) or skims (SKIMS)
  ///
  enum objLevel{
    GRID=0,
    SKIM=1
  };

  ///
  /// Print configuration of cuts
  ///
  void print(objLevel level = GRID);

  ///
  /// Objects classes are used to fill baseline and signal vectors
  /// They should be filled before calling ObjectTools::get_objects(),
  /// when reading skims in locally. Alternatively, if they are NULL,
  /// ObjectTools::get_objects() will try and fill the baseline and signal
  /// vectors using internal vectors filled when running on the grid
  ///

  EventVariable*     skimEvt;
  MetVector*         skimMet;
  MuonVector*        skimMuons;
  ElectronVector*    skimElectrons;
  TauVector*         skimTaus;
  JetVector*         skimJets;
  JetVector*         skimFatjets;
  JetVector*         skimTrackjets;
  PhotonVector*      skimPhotons;
  TrackVector*       skimTracks;
  ObjectVector*      skimCalo;
  TruthVector*       skimTruth;
  TruthEvent*        skimTruthEvent;
  TruthEvent_Vjets*  skimTruthEvent_Vjets;
  TruthEvent_VV*     skimTruthEvent_VV;
  TruthEvent_VVV*    skimTruthEvent_VVV;
  TruthEvent_TT*     skimTruthEvent_TT;
  TruthEvent_XX*     skimTruthEvent_XX;
  TruthEvent_GG*     skimTruthEvent_GG;

  V0Vector*          skimV0s; // JEFF
  VSIVector*         skimVSIs; // SICONG
  ///
  /// Returns true if all skim objects exist, false if any is NULL.
  /// Note that 'Truth' objects are not included in this filtering,
  /// as they are only optionally produced with SusySkimMaker
  ///

  bool skimObjectsExist() const;

  ///
  /// Read in objects from skims. Basically copies this pointers into the internal
  /// vectors of this class. Also sorts the internal vectors by pT when necessary
  ///
  void readObjects(const EventVariable* skimEvt,
		   const MetVector* skimMet,
		   const MuonVector* skimMuons,
		   const ElectronVector* skimElectrons,
		   const JetVector* skimJets,
		   const JetVector* skimFatjets,
		   const JetVector* skimTrackjets,
		   const TauVector* skimTaus,
                   const PhotonVector* skimPhotons,
                   const TrackVector* skimTracks,
                   const ObjectVector* skimCalo,
                   const V0Vector* skimV0s, // JEFF
                   const VSIVector* skimVSIs, // SICONG
		   const TruthEvent* skimTruthEvent,
		   const TruthEvent_Vjets* skimTruthEvent_Vjets,
		   const TruthEvent_VV* skimTruthEvent_VV,
		   const TruthEvent_VVV* skimTruthEvent_VVV,
		   const TruthEvent_TT* skimTruthEvent_TT,
		   const TruthEvent_XX* skimTruthEvent_XX,
		   const TruthEvent_GG* skimTruthEvent_GG,
		   const TruthVector* skimTruth);
  ///
  /// Apply user defined cuts to signal objects
  ///
  void classifyObjects();

  ///
  /// Copy base and signal electrons/muons into common lepton vectors
  ///
  void fillLeptonVectors();

  ///
  /// General containers, these are filled when calling readObjects
  ///
  EventVariable      evt;
  MetVector          metVector;
  MuonVector         muons;
  ElectronVector     electrons;
  JetVector          jets;
  JetVector          fatjets;
  JetVector          trackjets;
  TauVector          taus;
  PhotonVector       photons;
  TrackVector        tracks;
  V0Vector           v0s; // JEFF
  VSIVector          vsiVec; // SICONG

  ObjectVector       caloClus;
  TruthEvent         truthEvent;
  TruthEvent_Vjets   truthEvent_Vjets;
  TruthEvent_VV      truthEvent_VV;
  TruthEvent_VVV     truthEvent_VVV;
  TruthEvent_TT      truthEvent_TT;
  TruthEvent_XX      truthEvent_XX;
  TruthEvent_GG      truthEvent_GG;
  TruthVector        truth;

  ///
  /// Specific containers: these are filled when calling the get methods, i.e. getMuons()
  /// They retrieve the objects from the general containers. This should be what the user interacts with
  /// and uses
  ///
  MetVariable        met;              // Default met
  MetVariable        trackMet;         // Default track met
  LeptonVector       baseLeptons;      // Baseline electron/muons
  LeptonVector       signalLeptons;    // Signal electrons/muons
  MuonVector         baseMuons;        // Baseline muons
  MuonVector         signalMuons;      // Signal muons
  ElectronVector     signalElectrons;  // Signal electrons
  ElectronVector     baseElectrons;    // Baseline electrons
  TauVector          baseTaus;         // Baseline taus
  TauVector          signalTaus;       // Signal taus
  JetVector          baselineJets;     // Baseline jets, not subjecte to ST signal
  JetVector          cJets;            // Central jets
  JetVector          bJets;            // B-tagged jetds
  JetVector          fJets;            // Forward jets
  JetVector          aJets;            // All jets
  JetVector          cFatjets;         // Central fat jets
  PhotonVector       signalPhotons;    // Signal photons
  PhotonVector       basePhotons;      // Baseline photons
  TrackVector        baseTracks;       // Base tracks
  TrackVector        signalTracks;     // Signal tracks
  TrackVector        pixelThreeLayer;  // Disappearing tracks, 3-pixel layers
  TrackVector        pixelFourLayer;   // Disappearing tracks, >=4-pixel layers
  TrackVector        msTracks;         // MS only tracks
  TrackVector        stdTracks;        // Standard tracks
  TruthVector        truthParticles;   // Truth particles, not really used anymore
  TruthVector        truthLeptons;
  TruthVector        truthJets;
  TruthVector        charginos;
  TruthVector        truthStatus3Leptons;   //for merging Sherpa V + jet samples
  TruthVector        truthAllJets; //all jets for calculating truth mjj
  TruthVector        truthTaus; //for mjj overlap removal
  V0Vector           v0_vertices;      // V0 vertices // JEFF
  VSIVector           vsi_vertices;      // VSI vertices // SICONG
  ///
  /// Clear all internal vectors (general + specific containers)
  ///
  void clear();

  ///
  /// Overlap removal
  ///
  void doOverlap(MuonVector& muons, ElectronVector& electrons, PhotonVector& photons, JetVector& jets);
  void doOverlap_FatJet(ElectronVector& electrons, PhotonVector& photons, JetVector& fatjets);
  bool passTrackOR(TrackVariable* trk);

  ///
  /// Check baseMuons if it has a cosmic muon
  ///
  bool hasCosmicMuon();

  ///
  /// Check if event has a bad jet
  ///
  bool hasBadJet();

  ///
  /// Check if event has a jet passing any bad tile modules
  ///
  bool hasBadTileJet();

  ///
  /// Check if event has a bad  muon
  ///
  bool hasBadMuon();

  ///
  /// Central function to call for event cleaning cuts
  /// Some control with the methods below
  ///
  bool passEventCleaning(CutFlowTool*& cutflow, TString stream, double weight);
  void applyBadJetVeto(bool applyBadJetVeto){ m_applyBadJetVeto=applyBadJetVeto; }
  void applyEvtLooseBadJetVeto(bool applyEvtLooseBadJetVeto){m_applyEvtLooseBadJetVeto=applyEvtLooseBadJetVeto; }
  void applyEvtTightBadJetVeto(bool applyEvtTightBadJetVeto){m_applyEvtTightBadJetVeto=applyEvtTightBadJetVeto; }
  void applyTSTCleaning(bool applyTSTCleaning){ m_applyTSTCleaning=applyTSTCleaning; }
  void applyBadMuonVeto(bool applyBadMuonVeto){ m_applyBadMuonVeto=applyBadMuonVeto; }
  void applyCosmicMuonVeto(bool applyCosmicMuonVeto){ m_applyCosmicMuonVeto=applyCosmicMuonVeto; }
  void applyBadTileJetVeto(bool applyBadTileJetVeto){ m_applyBadTileJetVeto=applyBadTileJetVeto; }

  ///
  /// Muon classifications
  ///
  void getMuons();
  bool IsBaselineMuon(ObjectVariable* mu);
  bool IsTruthMuon(ObjectVariable* mu);
  bool IsSignalMuon(MuonVariable* mu);

  ///
  /// Electron classifications
  ///
  void getElectrons();
  bool IsBaselineElectron(ObjectVariable* ele);
  bool IsTruthElectron(ObjectVariable* ele);
  bool IsSignalElectron(ElectronVariable* ele);

  bool isTightPP(const ElectronVariable* ele);
  bool isMediumPP(const ElectronVariable* ele);

  ///
  /// Jet classifiation
  ///
  void getJets();
  bool isCentralJet(ObjectVariable* jet); // So we can pass truth containers to these exact objects
  bool isForwardJet(JetVariable* jet);
  bool isBtaggedJet(JetVariable* jet);

  // Bad tile modules info
  struct BadTileInfo {
    int   minRN,  maxRN;
    float minEta, maxEta;
    float minPhi, maxPhi;
  };
  inline BadTileInfo fillBadTileInfo(int minRN, int maxRN, float minEta, float maxEta, float minPhi, float maxPhi){
    BadTileInfo bi;
    bi.minRN  = minRN;   bi.maxRN  = maxRN;
    bi.minEta = minEta;  bi.maxEta = maxEta;
    bi.minPhi = minPhi;  bi.maxPhi = maxPhi;
    return bi;
  }
  void        loadListOfBadTileModules();

  std::vector<BadTileInfo> m_badTileDict;

  bool isBadTileJet(ObjectVariable* jet); // So we can pass truth containers to these exact objects

  ///
  /// Fatjet classifiation
  ///
  void getFatjets();
  bool isCentralFatjet(ObjectVariable* fatjet);

  ///
  /// Photon classifications
  ///
  void getPhotons();
  bool IsBaselinePhoton(ObjectVariable* pho);
  bool IsSignalPhoton(PhotonVariable* pho);

  ///
  /// Tau classifications
  ///
  void getTaus();
  bool IsSignalTau(TauVariable* tau);

  ///
  /// V0s
  ///
  void getV0s(); // JEFF
  
  // void getVSIs(); // SICONG

  ///
  /// Track classifications
  ///
  void getTracks();
  bool IsSignalTrack(TrackVariable* t,bool doDisappearingTrk=true);
  bool isAssociatedTrk(TrackVariable* trk,TrackVariable* parent=0 );

  ///
  /// MET
  ///
  void getMET();
  const MetVariable* getMETFlavor(MetVariable::MetFlavor flavor, bool verbose=true) const;

  ///
  /// Truth classification
  ///
  void getTruth();

  ///
  /// Triggers this class to use SUSYTools signal def.
  ///
  void useSUSYToolsSignalDef(bool ST_def){s_ST_def=ST_def;}
  void disableOR(bool disableOR){m_disableOR=disableOR;}
  void disableORBaselineElectrons(bool disableORBaselineElectrons){m_disableORBaselineElectrons=disableORBaselineElectrons;}
  void disableORBaselineMuons(bool disableORBaselineMuons){m_disableORBaselineMuons=disableORBaselineMuons;}

  ///
  /// Methods to set signal electron cuts
  ///

  void setBaselineElectronEt(float electron_et){b_electron_et=electron_et;}
  void setBaselineElectronEta(float electron_eta){b_electron_eta=electron_eta;}
  void setTruthElectronEt(float electron_et){t_electron_et=electron_et;}
  void setTruthElectronEta(float electron_eta){t_electron_eta=electron_eta;}
  void setSignalElectronEt(float electron_et){s_electron_et=electron_et;}
  void setSignalElectronEta(float electron_eta){s_electron_eta=electron_eta;}
  void setSignalElectronRelIso(float electron_relIso){s_electron_relIso=electron_relIso;}
  void setSignalElectronD0(float electron_d0){s_electron_d0=electron_d0;}
  void setSignalElectronZ0(float electron_z0){s_electron_z0=electron_z0;}
  void setSignalElectronTight(bool electron_tight){s_electron_tight=electron_tight;}
  float getSignalElectronEt(){return s_electron_et;}
  float getSignalElectronRelIso(){return s_electron_relIso;}
  float getSignalElectronD0(){return s_electron_d0;}
  float getSignalElectronZ0(){return s_electron_z0;}
  float getBaselineElectronEt(){ return b_electron_et; }

  ///
  /// Methods to set signal muons cuts
  ///

  void setBaselineMuonPt(float muon_pt){b_muon_pt=muon_pt;}
  void setBaselineMuonEta(float muon_eta){b_muon_eta=muon_eta;}
  void setTruthMuonPt(float muon_pt){t_muon_pt=muon_pt;}
  void setTruthMuonEta(float muon_eta){t_muon_eta=muon_eta;}
  void setSignalMuonPt(float muon_pt){s_muon_pt=muon_pt;}
  void setSignalMuonEta(float muon_eta){s_muon_eta=muon_eta;}
  void setSignalMuonAbsIso(float muon_absIso){s_muon_absIso=muon_absIso;}
  void setSignalMuonRelIso(float muon_relIso){s_muon_relIso=muon_relIso;}
  void setSignalMuonD0(float muon_d0){s_muon_d0=muon_d0;}
  void setSignalMuonZ0(float muon_z0){s_muon_z0=muon_z0;}
  void setSignalMuonIsCombined(bool muon_isCombinedMuon){s_muon_isCombinedMuon=muon_isCombinedMuon;}
  float getSignalMuonPt(){return s_muon_pt;}
  float getSignalMuonAbsIso(){return s_muon_absIso;}

  ///
  /// Methods to set jets cuts
  ///
  void setCJetPt(float cJet_pt){s_cJet_pt=cJet_pt;}
  void setCJetEta(float cJet_eta){s_cJet_eta=cJet_eta;}
  void setFJetPt(float fJet_pt){s_fJet_pt=fJet_pt;}
  void setFJetEtaMin(float fJet_eta_min){s_fJet_eta_min=fJet_eta_min;}
  void setFJetEtaMax(float fJet_eta_max){s_fJet_eta_max=fJet_eta_max;}

  ///
  /// Methods to set fat jets cuts
  ///
  void setCFatjetPt(float cFatjet_pt){s_cFatjet_pt=cFatjet_pt;}
  void setCFatjetEta(float cFatjet_eta){s_cFatjet_eta=cFatjet_eta;}
  void setCFatjetM(float cFatjet_m){s_cFatjet_m=cFatjet_m;}

  ///
  /// Methods to set photon cuts
  ///
  void setBaselinePhotonEt(float photon_et){b_photon_et=photon_et;}
  void setBaselinePhotonEta(float photon_eta){b_photon_eta=photon_eta;}
  void setSignalPhotonEt(float photon_et){s_photon_et=photon_et;}
  void setSignalPhotonEta(float photon_eta){s_photon_eta=photon_eta;}
  void setSignalPhotonRelIso(float photon_relIso){s_photon_relIso=photon_relIso;}
  void setSignalPhotonTightQuality(bool photon_tight){s_photon_tight=photon_tight;}

  ///
  /// Methods to set signal track cuts
  ///
  void setTrkPt(float trk_pt){s_trk_pt=trk_pt;}
  void setTrkMinEta(float trk_minEta){s_trk_minEta=trk_minEta;}
  void setTrkMaxEta(float trk_maxEta){s_trk_maxEta=trk_maxEta;}
  void setTrkD0(float trk_d0){s_trk_d0=trk_d0;}
  void setTrkD0Sig(float trk_d0Sig){s_trk_d0Sig=trk_d0Sig;}
  void setTrkZ0SinTheta(float trk_z0SinTheta){s_trk_z0SinTheta=trk_z0SinTheta;}
  void setTrkRelTrkIso(float trk_relTrkIso){s_trk_relTrkIso=trk_relTrkIso;}
  void setTrkCaloIso20(float trk_caloIso20){s_trk_caloIso20=trk_caloIso20;}
  void setFitQuality(float trk_fitQuality){s_trk_fitQuality=trk_fitQuality;}
  void setTrkMinPixHits(int trk_minPixHits){s_trk_minPixHits=trk_minPixHits;}
  void setTrkMaxSCTHits(int trk_maxSCTHits){s_trk_maxSCTHits=trk_maxSCTHits;}
  void setTrkPixSpoilHits(int trk_maxPixSpoiltHits){s_trk_maxPixSpoiltHits=trk_maxPixSpoiltHits;}
  void setMaxGangedFakes(int trk_maxGangedFakes){s_trk_maxGangedFakes=trk_maxGangedFakes;}
  void setMaxSiHoles(int trk_nSiHoles){s_trk_nSiHoles=trk_nSiHoles;}
  void setMaxPixelOutliers(int trk_pixOutliers){s_trk_pixOutliers=trk_pixOutliers;}
  void setMinPixelLayers(int trk_nPixLayers){s_trk_nPixLayers=trk_nPixLayers;}
  void setMinExpBlayerHits(int trk_expBLayerHits){s_trk_expBLayerHits=trk_expBLayerHits;}


  ///
  /// Control what MET flavor you want by default
  /// Will point the corresponding MetVariable to the standard
  /// Objects::met object
  ///
  void setMetFlavor(MetVariable::MetFlavor flavor){s_METFlavor=flavor;}

  ///
  /// Methods to set signal tau cuts
  ///
  void setSignalTauPt(float tau_pt){s_tau_pt=tau_pt;}
  void setSignalTauEta(float tau_eta){s_tau_eta=tau_eta;}

  float getSignalTauPt(){return s_tau_pt;}
  float getSignalTauEta(){return s_tau_eta;}

  ///
  /// Methods to set overlap removal deltaR values
  ///
  void doUserOR(bool user_or, bool doBOR=false){s_user_or=user_or; s_user_or_baware=doBOR; }
  inline void setMinDrMuJet(float minDr_m_j){m_minDr_m_j=minDr_m_j;}
  inline void setMinDrJetEle(float minDr_j_e){m_minDr_j_e=minDr_j_e;}
  inline void setMinDrEleJet(float minDr_e_j){m_minDr_e_j=minDr_e_j;}
  inline void setMinDrJetGamma(float minDr_j_g){m_minDr_j_g=minDr_j_g;}
  inline void setMinDrGammaJet(float minDr_g_j){m_minDr_g_j=minDr_g_j;}
  inline void setMinDrTrkMu(float minDr_t_m){m_minDr_t_m=minDr_t_m;}
  inline void setMinDrTrkEle(float minDr_t_e){m_minDr_t_e=minDr_t_e;}
  inline void setMinDrTrkJet(float minDr_t_j){m_minDr_t_j=minDr_t_j;}
  inline void setMinDrTruthJetEle(float truthEleJetDR){m_truthEleJetDR=truthEleJetDR;}
  inline void setMinDrTruthJetMu(float truthMuJetDR){m_truthMuJetDR=truthMuJetDR;}
  inline void setMinDrRecoJetEle(float recoEleJetDR){m_recoEleJetDR=recoEleJetDR;}
  inline void setMinDrRecoJetMu(float recoMuJetDR){m_recoMuJetDR=recoMuJetDR;}

  ///
  /// Methods to fill cut dictionary
  ///
  void collectCuts();
  void defineCutName(std::string name,float variable);
  std::vector< std::pair<std::string,float> > getLog();

  ///
  /// Sorting function to order object vectors by pT
  ///
  static bool compare(const ObjectVariable* a, const ObjectVariable* b){return a->Pt() > b->Pt();}

 protected:

  /// All signal object cuts
  bool  s_ST_def;
  bool  m_disableOR;
  bool  m_disableORBaselineElectrons;
  bool  m_disableORBaselineMuons;
  float b_electron_et;
  float b_electron_eta;
  float t_electron_et;
  float t_electron_eta;
  float s_electron_et;
  float s_electron_eta;
  float s_electron_relIso;
  bool s_electron_tight;
  float s_electron_d0;
  float s_electron_z0;
  float b_muon_pt;
  float b_muon_eta;
  float t_muon_pt;
  float t_muon_eta;
  float s_muon_pt;
  float s_muon_eta;
  float s_muon_absIso;
  float s_muon_relIso;
  float s_muon_d0;
  float s_muon_z0;
  float b_photon_et;
  float b_photon_eta;
  float s_photon_et;
  float s_photon_eta;
  float s_photon_relIso;
  bool s_photon_tight;
  float s_cJet_pt;
  float s_cJet_eta;
  float s_fJet_pt;
  float s_fJet_eta_min;
  float s_fJet_eta_max;
  float s_cFatjet_pt;
  float s_cFatjet_eta;
  float s_cFatjet_m;
  float s_tau_pt;
  float s_tau_eta;
  float s_trk_pt;
  float s_trk_minEta;
  float s_trk_maxEta;
  float s_trk_d0;
  float s_trk_d0Sig;
  float s_trk_z0SinTheta;
  float s_trk_relTrkIso;
  float s_trk_caloIso20;
  float s_trk_fitQuality;
  int s_trk_minPixHits;
  int s_trk_maxSCTHits;
  int s_trk_maxPixSpoiltHits;
  int s_trk_maxGangedFakes;
  int s_trk_nSiHoles;
  int s_trk_pixOutliers;
  int s_trk_nPixLayers;
  int s_trk_expBLayerHits;
  MetVariable::MetFlavor s_METFlavor;
  bool s_user_or;
  bool s_user_or_baware;
  float m_minDr_m_j;
  float m_minDr_j_e;
  float m_minDr_j_g;
  float m_minDr_e_j;
  float m_minDr_g_j;
  float m_minDr_t_m;
  float m_minDr_t_e;
  float m_minDr_t_j;
  float m_truthEleJetDR;
  float m_truthMuJetDR;
  float m_recoEleJetDR;
  float m_recoMuJetDR;


  bool s_muon_isCombinedMuon;
  bool m_applyBadJetVeto;
  bool m_applyEvtLooseBadJetVeto;
  bool m_applyEvtTightBadJetVeto;
  bool m_applyTSTCleaning;
  bool m_applyBadMuonVeto;
  bool m_applyCosmicMuonVeto;
  bool m_applyBadTileJetVeto;

  bool m_useRapidity = true; // this enable rapidity in the ROOT::TLorentzVector::deltaR


  /// Cut dictionary
  std::vector< std::pair<std::string,float> > m_cutDic;
  std::vector< std::pair<std::string,float> > m_log;

};

#endif
