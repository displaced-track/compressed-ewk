#ifndef SusySkimMaker_TruthEvent_h
#define SusySkimMaker_TruthEvent_h

#include "SusySkimMaker/ObjectVariable.h"

class TruthEvent : public ObjectVariable
{

 public:
  TruthEvent();
  virtual ~TruthEvent(){};

  TruthEvent(const TruthEvent&);
  TruthEvent& operator=(const TruthEvent&);

  ///
  /// Enum to describe how ttbar decayed
  ///
  enum TTbarDecayMode{
    NONE=0,
    LEPLEP=1,
    LEPLTAU=2,
    LTAULTAU=3,
    LEPHAD=4,
    LEPHTAU=5,
    LTAUHAD=6,
    LTAUHTAU=7,
    HADHAD=8,
    HADHTAU=9,
    HTAUHTAU=10,
    UNKNOWN=11
  };


  enum ZdecayMode{
    Z_NONE=0,
    Z_LLEP=1,
    Z_TAU=2,
    Z_INVISIBLE=3,
    Z_HADRONIC=4,
    Z_UNCLASSIFIED=5
  };

  enum WdecayMode{
    W_NONE=0,
    W_LLEP=1,
    W_TAU=2,
    W_HADRONIC=3,
    W_UNCLASSIFIED=4
  };

  std::map<TString,TLorentzVector*> TTbarTLVs;
  std::map<TString,TLorentzVector*> TauTLVs;
  TTbarDecayMode decayMode;
  int Tau1decayMode;
  int Tau2decayMode;

  // Only filled for N2 decays from the specified DSIDs
  double truthMll; // in GeV

  // Polarization correction weight for W/Z decaying from EWKinos.
  // Event-level weight obtained by multiplying the individual weight.
  // Calculated for the MadGraph EWKino direct production signal samples without MadSpin being used.
  double polWeight;

  // Used for estimate some V+jets theory uncertainties
  //   https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ThirdGenTheoreticalUncertainties2019#V_jets
  // Note: To make use of this parametrized uncertainties, you need to use the AntiKt4TruthJets collection (as opposed to the current standard AntiKt4TruthDressedWZJets)
  int nAntiKt4TruthJets20;

  std::vector<ZdecayMode> Zdecays;
  std::vector<WdecayMode> Wdecays;

  int nTruthTauFromW;
  std::vector<int> TruthTauFromW_DMV;

  const TLorentzVector* getTTbarTLV(const TString name); //!
  const TLorentzVector* getTauTLV(const TString name); //!

  std::vector<TLorentzVector> truthFatjets;
  std::vector<TLorentzVector> truthJets;
  std::vector<int> truthJets_label;
  std::vector<std::pair<TLorentzVector,int> > partons;

  std::vector<TLorentzVector> truthZbosons;

  ClassDef(TruthEvent, 4);

};

#endif
