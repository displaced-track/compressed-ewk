#ifndef SusySkimMaker_STATUSCODECHECK_H
#define SusySkimMaker_STATUSCODECHECK_H

#include "AsgTools/StatusCode.h"


#define CHECK( ARG )                                       \
   do {                                                        \
      const StatusCode sc__ = ARG;                             \
      if( ! sc__.isSuccess() ) {                               \
         ::Error( APP_NAME, "Failed to execute: \"%s\"",       \
                  #ARG );                                      \
         return sc__;                                          \
      }                                                        \
   } while( 0 )

#endif




