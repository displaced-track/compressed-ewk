#ifndef SusySkimMaker_MCCorrections_h
#define SusySkimMaker_MCCorrections_h

// C++
#include <vector>
#include <iostream>
#include <utility>
#include <map>

// ROOT
#include "TObject.h"
#include "TString.h"


class MCCorrElement: public TObject
{
  
 public:
  
  /// 
  /// Default constructor
  ///
  MCCorrElement();

  ///
  /// Destructor
  ///
  virtual ~MCCorrElement(){};
  
  ///
  // Copy constructor
  ///
  MCCorrElement(const MCCorrElement&);
  
  ///
  /// Assignment operator
  ///
  MCCorrElement& operator=(const MCCorrElement&);

  ///
  /// Add an efficiency to m_eff map, for a given systematic
  ///
  void addEff(TString sys,double mc_eff,double data_eff);

  ///
  /// Add a SF to m_eff map, for a given systematic
  ///
  void addSF(TString sys,double sf);
    
  ///
  /// Printing 
  /// 
  void print() const;

  ///
  /// Accessors
  ///
  std::map<TString,std::pair<float,float>> getEff();
  std::map<TString,float> getSF();


  ///
  /// Key is systematic name
  /// Values are efficiency in data/mc
  ///
  std::map<TString,std::pair<float,float>> m_eff;

  ///
  /// Key is systematic name
  /// Value is data/mc SF
  ///
  std::map<TString,float> m_sf;
  
 public:
  ClassDef(MCCorrElement,6);
  

};
// -------------------------------------------------------------- //
class MCCorrContainer: public TObject
{
  
 public:
 
  /// 
  /// Default constructor
  ///
  MCCorrContainer();

  ///
  /// Destructor
  ///
  virtual ~MCCorrContainer(){};
  
  ///
  // Copy constructor
  ///
  MCCorrContainer(const MCCorrContainer&);

  ///
  /// Assignment operator
  ///
  MCCorrContainer& operator=(const MCCorrContainer&);

  ///
  /// For a given systematic name and instance, store the corresponding SF
  ///
  void addSF(TString instance,TString systematic, double SF);  

  ///
  /// For a given systematic name and instance, store the efficiency in data and mc
  ///
  void addEff(TString instance,TString systematic,double mc_eff, double data_eff);

  /// 
  /// Printing
  ///
  void print() const;

  /// 
  /// Free the momery / delete the elements in the container
  ///
  void clear(); 

  ///
  /// Retrieve the data and mc efficiency for a given instance and systematic
  /// If not found, mc_eff and data_eff are not updated
  ///
  void getEff(TString instance, TString systematic, double& mc_eff, double& data_eff);

  ///
  /// Retrieve the sf for a given instance and systematic
  /// If not found, sf is not updated 
  ///
  void getSF(TString instance, TString systematic, double& sf);

  ///
  /// Print the contents of this class
  ///
  void print(); //! 

  /// 
  /// Return MCCorrElement in the global container for a given instance
  ///
  MCCorrElement* getElement(TString instance); //!

  /// 
  /// Return global map
  ///
  const std::map<TString,MCCorrElement*> getContainer(); //!
  
 //private:
  
  MCCorrElement* m_element;
  ///
  /// General container to hold SFs and eff
  // key = any particular instance. e.g. a b-tagging working point,
  /// or trigger SF chain, or lepton working point definiton
  ///  
  std::map<TString,MCCorrElement*> m_container;

 public:
  ClassDef(MCCorrContainer,6);
  
};


#endif
