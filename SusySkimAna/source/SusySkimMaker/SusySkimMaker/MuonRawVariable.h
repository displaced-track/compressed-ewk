#ifndef SusySkimMaker_MuonRawVariable_h
#define SusySkimMaker_MuonRawVariable_h

// Rootcore
#include "SusySkimMaker/MuonVariable.h"

class MuonRawVariable: public MuonVariable 
{


 public:
  MuonRawVariable();
  virtual ~MuonRawVariable(){};

  MuonRawVariable(const MuonRawVariable&);
  MuonRawVariable& operator=(const MuonRawVariable&);
  MuonRawVariable& operator=(MuonVariable&);

  int truthType; 
  int truthOrigin;

  float qoverp;
  float qoverp_cov;
  float d0_cov;
  float z0_cov;

  float me_qoverp_exPV;
  float id_qoverp_exPV;
  float me_theta_exPV;
  float id_theta_exPV;
  float id_theta;

  float me_qoverp_cov;
  float id_qoverp_cov;
  float me_d0_cov;
  float me_z0_cov;
  float id_d0_cov;
  float id_z0_cov;

  float momentumBalanceSignificance;
  float scatteringCurvatureSignificance;

  bool loose;
  bool isCombinedMuon;
  bool isSegmentTaggedMuon;
  bool isStandAloneMuon;
  bool isCaloTaggedMuon;
    
  int nPixHits;
  int nPixelDeadSensors; 
  int nPixHoles;
  int nSCTHits;
  int nSCTDeadSensors;
  int nSCTHoles;
  int nTRTHits;
  int nTRTOutliers;

  unsigned int nMuPrecLayerHits;
  unsigned int nMuPrecLayerHoles;

  float chiSquared;
  float nDOF;

  ClassDef(MuonRawVariable, 6);

};

#endif
