#ifndef SusySkimMaker_TruthObject_h
#define SusySkimMaker_TruthObject_h

// RootCore includes
#include "SusySkimMaker/TruthVariable.h"
#include "SusySkimMaker/TruthEvent.h"
#include "SusySkimMaker/BaseHeader.h"

// xAOD includes
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"

// CP tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"

// TruthDecayContainer
#include "TruthDecayContainer/DecayHandle.h"
#include "TruthDecayContainer/BosonPolReweightingTool.h"
#include "TruthDecayContainer/TruthEvent_Vjets.h"
#include "TruthDecayContainer/TruthEvent_VV.h"
#include "TruthDecayContainer/TruthEvent_VVV.h"
#include "TruthDecayContainer/TruthEvent_TT.h"
#include "TruthDecayContainer/TruthEvent_XX.h"
#include "TruthDecayContainer/TruthEvent_GG.h"



class TreeMaker;
class StatusCode;

typedef std::vector<TruthVariable*> TruthVector;
typedef TLorentzVector tlv;


class TruthObject
{

 private:

  ///
  /// Internal initialized of all tools needed for this class
  ///
  StatusCode init_tools();

 public:
  TruthObject();
  ~TruthObject();

  //
  StatusCode init(TreeMaker*& treeMaker, int dsid);

  // Fill truth particle container
  // Requires CentralDBFields::SAVETRUTHPARTICLES to be set by the user
  // in their deep configuration file (default false)
  void fillTruthParticleContainer(const xAOD::TruthParticleContainer* truthParticles,
                                  const xAOD::JetContainer* truthJets,
                                  const xAOD::TruthParticleContainer* truthBSMs
                                  );

  // Fill truth event container

  // Requires CentralDBFields::SAVETRUTHEVT to be set by the user
  // in their deep configuration file (default false)
  void fillTruthEventContainer(const xAOD::TruthParticleContainer* truthParticles,
			       const xAOD::TruthParticleContainer* truthElectrons,
			       const xAOD::TruthParticleContainer* truthMuons,
			       const xAOD::TruthParticleContainer* truthTaus,
			       const xAOD::TruthParticleContainer* truthPhotons,
			       const xAOD::TruthParticleContainer* truthNeutrinos,
			       const xAOD::TruthParticleContainer* truthBosonWDP,
			       const xAOD::TruthParticleContainer* truthZBosons,
			       const xAOD::TruthParticleContainer* truthTopWDP,
			       const xAOD::TruthParticleContainer* truthBSMWDP,
			       const xAOD::TruthParticleContainer* truthTauWDP,
			       const xAOD::JetContainer* truthJets,
			       const xAOD::JetContainer* truthFatjets,
			       const xAOD::EventInfo* eventInfo);

  // !!!! Sherpa only !!!!
  void classifyBosons(const xAOD::TruthParticleContainer* truthParticles,const xAOD::EventInfo* eventInfo);

  bool TruthFinder_tt(const xAOD::TruthParticleContainer* truthParticles,
                      const xAOD::EventInfo* eventInfo,
                      std::map<TString,TLorentzVector*>& ttbarTLVs,
                      int &pdgId11, int &pdgId12, int &pdgId21, int &pdgId22,
                      int &TauMode1, int &TauMode2);

  bool TruthFinder_tau(const xAOD::TruthParticleContainer* truthParticles,
               const xAOD::EventInfo* eventInfo,
               std::map<TString,TLorentzVector*>& tauTLVs);

  TruthEvent::TTbarDecayMode GetTTbarMode(int pdgId11,int pdgId21,int TauMode1,int TauMode2);
  void CountMode(int pdgId, int TauMode, int& nLep, int& nHad, int& nTaulep, int& nTauhad);

  // Returns truth mll in MeV, for the specified DSIDs
  double getN2TruthMll(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo);

  //
  // Reset containers.
  //
  void Reset();

  //
  // Return objects.
  //
  const TruthVector* getObj(TString sysName);
  const TruthEvent* getTruthEvent(TString sysName);
  const TruthEvent_Vjets* getTruthEvent_Vjets(TString sysName);
  const TruthEvent_VV*    getTruthEvent_VV   (TString sysName);
  const TruthEvent_VVV*   getTruthEvent_VVV   (TString sysName);
  const TruthEvent_TT*    getTruthEvent_TT   (TString sysName);
  const TruthEvent_XX*    getTruthEvent_XX   (TString sysName);
  const TruthEvent_GG*    getTruthEvent_GG   (TString sysName);


  // Flag telling if TruthDecayContainer
  bool saveTruthEvtInfoFull(){ return m_saveTruthEvtInfoFull; }

 private:
  bool isTruthBJet(const xAOD::Jet* jet,const xAOD::TruthParticleContainer* truthParticles);


 protected:
  float          m_convertFromMeV;
  int            m_dsid;
  bool           m_saveTruthEvtInfoFull;
  bool           m_dressTruthLeptons;

  TruthVector*   m_truthVector;
  TruthEvent*    m_truthEvent;

  ///
  /// DeltaR value for associating truth jets and b-hadrons
  ///
  float          m_deltaRJetBHadron;

  //
  // Main driver class in TruthDecayContainers retreiving decay chains from TruthParticles.
  // Activated only when m_saveTruthEvtInfoFull is true.
  DecayHandle *m_decayHandle;

  //
  // Tools to calculate the polarization correction weight (for W/Z from EWKinos decays)
  BosonPolReweightingTool *m_bosonRwgtTool;

  // Container classes storing full decay chains filled only when m_saveTruthEvtInfoFull is true through TruthDecayContainers::DecayHandle.
  TruthEvent_Vjets *m_truthEvent_Vjets;
  TruthEvent_VV    *m_truthEvent_VV;
  TruthEvent_VVV   *m_truthEvent_VVV;
  TruthEvent_TT    *m_truthEvent_TT;
  TruthEvent_XX    *m_truthEvent_XX;
  TruthEvent_GG    *m_truthEvent_GG;

};

#endif
