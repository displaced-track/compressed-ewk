#ifndef SusySkimMaker_MsgLog_h
#define SusySkimMaker_MsgLog_h

// ROOT include(s)
#include "TString.h"

// C++ includes
#include <fstream>
#include <iostream>
#include <stdarg.h>

/*
 
  Written by: Matthew Gignac (UBC)

  Static class used to log and output messages
  Interally will store all msgs, such that we can 
  retrieve them later when running test jobs to see
  if any errors or warnings have occured

  Note, messages are not logged for INFO stream,
  since its expected to be much larger and not important
  to parse 

*/


class MsgLog
{

 public:
   MsgLog();

   // TODO: Add in print of warning and error messages
   // and then clean up the space
   ~MsgLog(){}

   static void WARNING(TString className,const char *va_fmt, ...);
   static void ERROR(TString className,const char *va_fmt, ...);  
   static void INFO(TString className,const char *va_fmt, ...);
   static TString format(const char* va_fmt, va_list args);
   static TString formatOutput(TString className,TString msg,TString msgStreamSize);

   static bool doPrint(TString msg);


};


#endif
