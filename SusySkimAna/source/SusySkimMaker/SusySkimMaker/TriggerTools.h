#ifndef xAODNTUPLEMAKER_TRIGGERTOOLS_H
#define xAODNTUPLEMAKER_TRIGGERTOOLS_H

// Infrastructure include(s):
//#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
//#include "xAODRootAccess/TStore.h"

// xAOD includes(s)
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "TriggerMatchingTool/MatchFromCompositeTool.h"

#include "xAODBase/IParticle.h"
#include "xAODEgamma/Electron.h"
#include "xAODTrigEgamma/TrigElectronContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"

// ROOT includes (s)
#include "TString.h"
#include "TSystem.h"

class TriggerTools : public TObject
{

 public:
  TriggerTools( bool writeAllDecisions );
  ~TriggerTools();

  ///
  /// Always needs to be called at the start of the job
  ///
  StatusCode initialize(xAOD::TEvent* event);

  ///
  /// Resets the L1 and HLT decision maps
  ///
  void Reset_perEvt();

  ///
  /// Resets the trigger decision maps. Systematics could affect this decision
  ///
  void Reset_perSys();

  ///
  /// Load into queue all user requested triggers
  ///
  void loadTriggers();
  void getTriggerListFromFile(std::string fileName,std::vector<TString>& triggerNames);

  ///
  /// General function called once per event to perform trigger emulation
  /// Any emulation should be in this function, or at the very least called from this method
  ///
  StatusCode emulateMETTriggers();
  StatusCode emulateJetTriggers();
  StatusCode emulateHiggsinoTriggers();

  ///
  /// Main function to call
  ///
  const std::map<TString,bool> matchLeptonTriggers(const xAOD::IParticle* p);
  const std::map<TString,bool> matchMetTriggers();
  const std::map<TString,bool> matchJetTriggers(const xAOD::Jet* offlineJet);
  const std::map<TString,bool> matchPhotonTriggers(const xAOD::Photon* offlinePhoton);

  ///
  /// Main function to call to just get isPassed decision, no matching
  ///
  void LoadEvtTriggers();

  ///
  /// Require a given chain to be associated to a year
  /// Not the best way to do this, but if you want a particular chain to be
  /// only associated to a given year only add it to this function
  /// When return true, this chain is OK to use, otherwise they are not consistent
  ///
  static bool requireYear(TString chain,int year);

  ///
  /// Dump information regarding trigger matching
  ///
  void setDbg(bool dbg){m_dbg=dbg;}

  ///
  /// Access to evt level decisions
  ///
  const std::map<TString,bool> getHLTEvtDec(){ return m_HLT_evtDecision; }
  const std::map<TString,bool> getL1EvtDec(){ return m_L1_evtDecision; }
  const std::map<TString,float> getTriggerPrescale(){ return m_prescale; }

  const std::vector<TString> getMuonTriggerSFList(){ return m_SFs_muons; }
  const std::vector<TString> getElectronTriggerSFList(){ return m_SFs_electrons; }

  ///
  /// Get emulated trigger values
  ///
  float get_met_L1(){return m_met_L1;};
  float get_met_HLT_pufit(){return m_met_HLT_pufit;};
  float get_met_HLT_cell(){return m_met_HLT_cell;};
  float get_leadJetPt_L1(){return m_leadJetPt_L1;};
  float get_leadJetPt_HLT(){return m_leadJetPt_HLT;};

  ///
  /// Return true when any trigger has fired, otherwise false
  ///
  bool hasEvtHLTBit();

  ///
  /// Return a pointer to the xAOD::TEvent
  ///
  std::vector<std::string> GetTriggerList(std::string& chain);

 private:

  ///
  /// When false, the trigger matching maps will only save bits which have been successfully matched.
  /// In your analysis code, if you don't find a trigger bit in the map returned by matchLeptonTriggers or matchMetTriggers,
  /// you can assume it was not matched. When true, both trigger bits matching and unmatched are saved.
  bool m_writeAllDecisions;

  ///
  /// Called from public matchTriggers
  ///
  const std::map<TString,bool> matchMuonTriggers(const xAOD::Muon* offlineMuon);
  const std::map<TString,bool> matchElectronTriggers(const xAOD::Electron* offlineElectron);

  ///
  /// Called from LoadEvtTriggers
  ///
  void LoadEvtHLTTriggers(std::vector<TString> HLTChains);
  void LoadEvtL1Triggers(std::vector<TString> L1Chains);
  void LoadEvtPrescales(std::vector<TString> HLTChains);

  ///
  /// Get bool decision from pre-saved map
  ///
  bool getHLTDecFromMap(const std::string trigChainName);

  //
  // Delete tools associated by ToolHandle
  //
  template <typename T>
    inline static void DeleteTool(ToolHandle<T>& handle)
    {
      if (handle.empty()) return;

      std::string name = handle.name();
      if (asg::ToolStore::contains<T>(name)) {
    auto tool = asg::ToolStore::get<T>(name);
    delete tool;
      }
    }


 protected:

   ///
   /// TEvent
   ///
   xAOD::TEvent* m_event;

   ///
   /// Debug mode
   ///
   bool m_dbg;

   ///
   /// Cone size for matching online trigger objects to offline muon candidates
   ///
   float m_muDeltaR;

   ///
   /// Cone size for matching online trigger objects to offline electron candidates
   ///
   float m_eleDeltaR;

   ///
   /// Cone size for matching online trigger objects to offline jet candidates
   ///
   float m_jetDeltaR;

   ///
   /// Cone size for matching online trigger objects to offline photon candidates
   ///
   float m_phoDeltaR;

   ///
   /// Turn off matching
   ///
   bool m_disableMatching;

   ///
   /// Tools
   ///
   ToolHandle<TrigConf::ITrigConfigTool> m_configHandle;
   ToolHandle<TrigConf::ITrigConfigTool> m_configTool;
   ToolHandle<Trig::TrigDecisionTool>    m_trigDecTool;
   ToolHandle<Trig::IMatchingTool>       m_trigMatchingTool;

   ///
   /// These vectors contain all triggers we will consider
   ///
   std::vector<TString> m_HLT_electrons;
   std::vector<TString> m_HLT_muons;
   std::vector<TString> m_HLT_met;
   std::vector<TString> m_HLT_jet;
   std::vector<TString> m_HLT_photons;
   std::vector<TString> m_L1;


   ///
   /// List of supported trigger scale factors
   ///
   std::vector<TString> m_SFs_muons;
   std::vector<TString> m_SFs_electrons;

   ///
   /// Decision of the trigger matching. Only stored if the match == true
   ///
   std::map<TString,bool> m_HLT_dec_electrons;
   std::map<TString,bool> m_HLT_dec_muons;
   std::map<TString,bool> m_HLT_dec_met;
   std::map<TString,bool> m_HLT_dec_jet;
   std::map<TString,bool> m_L1_dec;

   ///
   /// If the trigger fired the event, for HLT and L1. Decision is saved for both passing and failing
   ///
   std::map<TString,bool> m_L1_evtDecision;
   std::map<TString,bool> m_HLT_evtDecision;
   std::map<TString,float> m_prescale;

   ///
   /// Conversion factors to convert from MeV into desired units
   ///
   float m_convertFromMeV;

   ///
   /// Store L1 and HLT MET and leading jet pT values from Higgsino triggers (in GeV)
   ///
   float m_met_L1;
   float m_met_HLT_pufit;
   float m_met_HLT_cell;
   float m_leadJetPt_L1;
   float m_leadJetPt_HLT;

   ///
   /// Flags to turn on/off trigger emulation.
   /// Note these can be turned off within the code when the containers do not exist
   ///
   bool m_emulateJetTriggers;
   bool m_emulateMetTriggers;
   bool m_emulateHiggsinoTriggers;

   ///
   /// Path to look for trigger files. Controlled using CentralDB
   ///
   TString m_trigFileDir;

   ///
   /// Flag to turn on/off trigger prescales
   ///
   bool m_writeTriggerPrescales;

   ///
   /// Some derivations such as DAOD_PHYS have a thinned trigger navigation and
   /// and trigger matching has been already performed upstream which requires
   /// to use a dedicated tool for reading out the associated "matching "containers
   ///
   bool m_upstreamTriggerMatching;
   TString m_upstreamMatchingPrefix;

   ///
   /// this enable rapidity in the ROOT::TLorentzVector::deltaR
   ///
   bool m_useRapidity = true;



};

#endif
