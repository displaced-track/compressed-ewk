 // use -*- C++ -*- mode
#ifndef TOPNESSTOOL_H
#define TOPNESSTOOL_H

#include <vector>
#include "SusySkimMaker/Objects.h"
#include "TLorentzVector.h"
#include "TVector2.h"
//#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
//class SUSYObjDef;

class TopnessTool {
public:
  // Initialize object using measured kinematic variables.  two jets are
  // required. j1 is always assumed to be a b-jet. if there is exactly 1
  // b-tagged jet in the event, three jets should be provided. the combinations
  // (j1,j2) and (j1,j3) are evaluated in that case. units are assumed to be
  // either MeV or GeV, depending on the use_GeV flag.
  TopnessTool(const TLorentzVector& p_l, const TVector2& p_miss_xy,
              const TLorentzVector& p_j1, const TLorentzVector& p_j2,
              bool use_GeV = false);
  TopnessTool(const TLorentzVector& p_l, const TVector2& p_miss_xy,
              const TLorentzVector& p_j1, const TLorentzVector& p_j2,
              const TLorentzVector& p_j3, bool use_GeV = false);

  // constructor for use with SUSYTools
  /*TopnessTool(SUSYObjDef* susyObjDef,
              int selected_electron,
              int selected_muon,
              std::vector<int> selected_jets,
              std::vector<int> btagged_jets,
              const TVector2& E_T_miss);
*/
 // TopnessTool(TLorentzVector Lepton, JetVariable* jet, MetVariable* met);
 TopnessTool(TLorentzVector Lepton, const xAOD::JetContainer* jets , const xAOD::MissingET* MET);
  // Run minimization to find missing W and neutrino kinematic parameters.
  // All possible b-jet assignments are considered.
  // The best topness value found is returned.
  double minimize(int n_iterations = 10, double tolerance = 4e-9, double step = 20e3);

  // return value of topness for current set of parameters.
  double topness() const;

  // return value of topness for given parameters.
  double topness(const std::vector<double>& params) const;

  // parameters are meaningful only after running a minimization.
  double p_nu_x;
  double p_nu_y;
  double p_nu_z;
  double p_W_z;

private:
  TLorentzVector p_l;
  TVector2 p_miss_xy;
  TLorentzVector p_j1;
  TLorentzVector p_j2;
  TLorentzVector p_j3;
};

#endif
 

