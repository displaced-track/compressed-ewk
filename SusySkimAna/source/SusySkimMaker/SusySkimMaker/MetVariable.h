#ifndef SusySkimMaker_MetVariable_h
#define SusySkimMaker_MetVariable_h

#include "SusySkimMaker/ObjectVariable.h"

class MetVariable : public ObjectVariable 
{

 public:

  ///
  /// Constructor
  ///
  MetVariable();

  ///
  /// Destructor
  ///
  virtual ~MetVariable();

  MetVariable(const MetVariable&);
  MetVariable& operator=(const MetVariable&);

  enum MetFlavor{
    DEFAULT=0,       // Default configuration 
    TRACK=1,         // Track based MET
    INVISIBLE_LEP=2,  // Leptons are treated as invisible
    INVISIBLE_MUON=3,  // Muons are treated as invisible
    INVISIBLE_ELECTRON=4,  // Electrons are treated as invisible
    INVISIBLE_PHOTON=5,  // Photons are treated as invisible
    LOOSE=6,       // Default configuration 
    TIGHTER=7,       // Default configuration 
    TENACIOUS=8,       // Default configuration
    TRACKLOOSE=9,       // Default configuration 
    TRACKTIGHTER=10,       // Default configuration 
    TRACKTENACIOUS=11,       // Default configuration 
    NUM_FLAVOR=12     // Counter, increment me when adding new flavors
  };

  ///
  /// Identifer for how the MET was rebuilt
  ///
  MetFlavor flavor;

  float Et;
  float phi;
  float px;
  float py;

  float Et_truth;
  float px_truth;
  float py_truth;

  float Et_ele;
  float phi_ele;
  float px_ele;
  float py_ele;

  float Et_muon;
  float phi_muon;
  float px_muon;
  float py_muon;

  float Et_jet;
  float phi_jet;
  float px_jet;
  float py_jet;

  float Et_soft;
  float phi_soft;
  float px_soft;
  float py_soft;

  float Et_muons_invis;
  float phi_muons_invis;
  float px_muons_invis;
  float py_muons_invis;


  float metSignif;

  void Reset(); //!

  TLorentzVector getTruthTLV() const; //!

  ClassDef(MetVariable, 4);

};

#endif
