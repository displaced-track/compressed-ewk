#ifndef SusySkimMaker_DuplicateEventChecker_h
#define SusySkimMaker_DuplicateEventChecker_h

#include <map>
#include <iostream>
#include <vector>
#include "TString.h"

class DuplicateEventChecker
{

  // One for each DSID
  struct EventInfo{ 
    
    unsigned int DSID;
    double nTotal;
    
    // Key is the event number
    // Value is a vector of <reco value,truth value>
    std::map<unsigned long long, std::pair<float,float> > index;
    std::vector<unsigned long long> dxAOD_duplicate_log;
    std::vector<unsigned long long> evtGen_duplicate_log;
 
    // Only function that needs to be called
    void check( unsigned long long evtNumber, float truthValue, float recoValue ){
      
      // Keep track of number of times this was called
      nTotal++;
      
      auto it = index.find( evtNumber );
      
      // This event number was not found, add it in!
      if( it == index.end() ){
    
    // Cache
    index.insert( std::pair<unsigned long long, std::pair<float,float> >(evtNumber, std::pair<float,float>(recoValue,truthValue) ) );

      }
      else{

    bool sameTruthQuantity=false;
    bool sameRecoQuantity=false;

    if( it->second.first==recoValue   ) sameRecoQuantity=true;
    if( it->second.second==truthValue ) sameTruthQuantity=true;

    // DXAOD duplicates should have both the same truth and reco quantity
    // Note, loosen the comparison to only reco-quantity, such that the comparisons works for data
    if( sameRecoQuantity  )
      dxAOD_duplicate_log.push_back(evtNumber );
    // EVGEN duplicates should have the same truth but different reco, since they have different PU profiles
    else if( sameTruthQuantity && !sameRecoQuantity ) 
      evtGen_duplicate_log.push_back( evtNumber );
    
      }
    }

  };

 public:
  DuplicateEventChecker();
  ~DuplicateEventChecker();

  ///
  /// Performs the check if its a duplicate event, called for each event
  ///
  void check(unsigned int DSID,unsigned long long evtNumber, float truthValue, float recoValue);

  ///
  /// Write header for output text file
  void writeHeader();

  ///
  /// Write custom text to output text file
  void writeAdditional(TString str);

  ///
  /// User should call at the end, and then the methods below to retrieve the information 
  void collectResults();

  // Filled in collectResults()
  std::map<unsigned int,double> getEvtGenDuplicateFrac(){ return m_evtGenDuplicatesFraction; }
  std::map<unsigned int, std::vector<unsigned long long>> getEvtGenDuplicateLog(){ return m_evtGen_duplicate_log; }
  std::map<unsigned int, std::vector<unsigned long long>> getDxAODDuplicateLog(){ return m_dxAOD_duplicate_log; }

  // Helper functions
  //
  void setOutputInfo(TString outputFilePath, TString identifier){ 
    m_outputFilePath=outputFilePath;
    m_identifier=identifier;
    m_fullPath=m_outputFilePath+"/"+m_identifier+".txt";
  }


 protected:

  TString m_outputFilePath;
  TString m_identifier;
  TString m_fullPath;
  bool m_headerWritten;

  std::map<int,EventInfo*> m_eventLog;
  std::map<unsigned int,double> m_evtGenDuplicatesFraction;
  std::map<unsigned int, std::vector<unsigned long long>> m_dxAOD_duplicate_log;
  std::map<unsigned int, std::vector<unsigned long long>> m_evtGen_duplicate_log;
 

};

#endif
