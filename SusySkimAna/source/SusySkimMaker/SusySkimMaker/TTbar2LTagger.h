#ifndef XAODNTUPLEANALYSIS_TTbar2LTagger_H
#define XAODNTUPLEANALYSIS_TTbar2LTagger_H

// TComplex
// #include "Riosfwd.h" // warning: #warning "Riosfwd.h is deprecated. It will be removed in ROOT v6.12. Please use #include <iosfwd>, instead." [-Wcpp]
#include <iosfwd>
#include "TComplex.h"

using namespace std;
class TTbar2LTagger {

 public:

  //TTbar2LTagger(Double_t pb1[4], Double_t pl1[4],Double_t pb2[4], Double_t pl2[4], Double_t pxm, Double_t pym, Double_t pxa[4], Double_t pxb[4], Double_t pya[4], Double_t pyb[4], Double_t pza[4], Double_t pzb[4], Int_t nsol);
  /// solution for ttbar constrains
  void solve2l(Double_t pl1a[4], Double_t pl2a[4], Double_t pl1b[4], Double_t pl2b[4], Double_t pxm, Double_t pym);
  void  single(Double_t pl1[4], Double_t pl2[4]);
  void  solve2d2(Double_t c[6], Double_t d[6]);
  void  r8poly4_root ( Double_t a, Double_t b, Double_t c, Double_t d, Double_t e, TComplex *r1, TComplex *r2, TComplex *r3, TComplex *r4 );
  void  r8poly3_root ( Double_t a, Double_t b, Double_t c, Double_t d, TComplex *r1, TComplex *r2, TComplex *r3 );
  Double_t  c8_sqrt ( TComplex x );
  Double_t  r8_sign ( Double_t x );
  Double_t  c8_argument ( TComplex x );
  Double_t  c8_magnitude ( TComplex x );

  //// variables for solutions of ttbar constrains
  double pl1[4];
  double pl2[4];
  double pb1[4];
  double pb2[4];
  double pxa[4];
  double pxb[4];
  double pya[4];
  double pyb[4];
  double pza[4];
  double pzb[4];
  double a_single[6];
  double xsol_solve[4], ysol_solve[4];
  int nsol_solve;
  int nsol;
  int nbjet1;
  int nbjet2;
  int nsol1;
  int nsol2;

};
#endif
