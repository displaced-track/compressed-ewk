#ifndef SusySkimMaker_BASEUSER_H
#define SusySkimMaker_BASEUSER_H

// Tells the preprocessor to only include this file once
// We have a problem if we have many ana packages all using 
// this base class 
#pragma once

// RootCore
#include "SusySkimMaker/Observables.h"
#include "SusySkimMaker/ConfigMgr.h"
#include "SusySkimMaker/MergeTool.h"

// ROOT
#include "TFile.h"
#include "TString.h"
#include "TH1.h"

// C++
#include <vector>

class BaseUser : public Observables
{

 private:
   BaseUser(){};

 public:
  BaseUser(TString packageName,TString selectorName);

  virtual ~BaseUser() {};

  virtual void setup(ConfigMgr*& configMgr) = 0;
  virtual bool doAnalysis(ConfigMgr*& configMgr) = 0;
  virtual bool passCuts(ConfigMgr*& configMgr)  = 0;
  virtual void finalize(ConfigMgr*& configMgr) = 0;

  virtual bool init_merge(MergeTool*& mergeTool) = 0;
  virtual bool execute_merge(MergeTool*& mergeTool) = 0;

  // M.G. Where is this used? Should be protected
  TString sampleName;

  // Return the selector name 
  TString getSelectorName(){ return m_selectorName; } 

  // Return the RootCore package name in which 
  // this selector resides
  TString getPackageName(){ return m_packageName; }

  // Set flags
  void setTruthOnly(bool truthOnly){     m_truthOnly   = truthOnly; }
  void setDisableCuts(bool disableCuts){ m_disableCuts = disableCuts; }

 protected:
   TString m_selectorName;
   TString m_packageName;

   // Default set of flags that can be used inside the selector
   bool m_truthOnly;
   bool m_disableCuts;

};

// Global implementations, which allow the user to get 
// a BaseUser child class
std::vector<BaseUser*> *getList();
BaseUser* getAnalysisSelector(TString name);



#endif
