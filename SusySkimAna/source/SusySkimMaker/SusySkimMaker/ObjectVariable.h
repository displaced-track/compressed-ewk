#ifndef SusySkimMaker_ObjectVariable_h
#define SusySkimMaker_ObjectVariable_h

// C++
#include <map>
#include <utility>
#include <iostream>
#include <iomanip>
#include <string>

// Root
#include "TLorentzVector.h"

/*
 *
 * Common base class to all objects
 * All fundamental quanities to all objects go here
 *
*/


class ObjectVariable : public TLorentzVector
{

 public:
  ObjectVariable();
  virtual ~ObjectVariable() {};

  ObjectVariable(const ObjectVariable&);
  ObjectVariable& operator=(const ObjectVariable&);

  // using TLorentzVector::DeltaR;
  // float DeltaR(TLorentzVector*);

  std::map<TString,bool> triggerMap;

  ///
  /// Filled when a truth match is found for a reco particle
  ///
  TLorentzVector truthTLV;

  // Barcode of a truth particle that has been
  // associated to this class
  int barcode;

  bool trigMatchResult(const TString trigChain); //!
  template<typename T, typename S> void setDefault( T& var, S def) {var = (T)def;}

  Double_t DeltaR(const TLorentzVector & v, const Bool_t useRapidity=false);

  ClassDef(ObjectVariable, 1);

};


#endif
