git clone https://$USER@gitlab.cern.ch/SusySkimAna/SusySkimMaker.git

- **List of updated required to migrate to CMake** <br>
  1) Replace all paths relying on `$ROOTCOREBIN` with PathResolver  <br>
     https://twiki.cern.ch/twiki/bin/view/AtlasComputing/PathResolver#What_if_I_really_have_to_keep_a <br> 
  2) Write the CMakeLists.txt file <br>
  3) --- DONE --- Remove all unit tests -- they are not actually unit tests yet.. <br>
  4) Package `NNLOReweighter` needes to be transitioned to CMake, or removed for the time being (not really used) <br>
  5) Package `NearbyLepIsoCorrection` needs to be transitioned to CMake <br>

 -- Test, Test, test <br>

- **On pulling master from 20 Feb. 2018** <br>
  The [TruthDecayContainer](https://gitlab.cern.ch/shion/TruthDecayContainer) package needs to be checked out.
  Please git clone in `source/`. <br>
  SusySkimDriver also needs to be updated, so please make sure pull the master as well.

- **On pulling master from 26 Feb. 2018** <br>
  It turns that `SUSYTools::GetMETSig` is only available from 21.2.16. <br>
  In case you want to work in the releases older than it, please either mask the call in ObjectTools.cxx [here](https://gitlab.cern.ch/SusySkimAna/SusySkimMaker/blob/master/Root/ObjectTools.cxx#L251), or checkout the new SUSYTools package by hand. 
