#include "SusySkimMaker/BaseUser.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimDriver/ConfigExtract.h"

ConfigExtract::ConfigExtract()
{


}
// ------------------------------------------------------------------------------------- //
TString ConfigExtract::getPackageName(TString selector,TString packageName)
{

  if( !packageName.IsWhitespace() ){
    MsgLog::INFO("ConfigExtract::getPackageName","Using the user passed package name %s",packageName.Data() );
    return packageName;
  }
  else if( !selector.IsWhitespace() ){
    BaseUser* anaClass = getAnalysisSelector(selector);

    if( !anaClass ){
      MsgLog::ERROR("ConfigExtract::getPackageName","Invalid selector! Aborting...");
      abort();
    }
    else{
      MsgLog::INFO("ConfigExtract::getPackageName","Found analysis package %s from users selector %s",anaClass->getPackageName().Data(),selector.Data() );
      return anaClass->getPackageName();
    }

  }
  else{
    MsgLog::WARNING("ConfigExtract::getPackageName","Could not get analysis package name! This assumes a full (relative) path for deepConfig");
    return packageName;
  }


}
