#include <cstdlib>
#include <string>

#include "TChain.h"
#include "TString.h"

#include "SusySkimDriver/SkimEvtLooper.h"


using namespace std;


void help()
{
  cout << "  Options:"                          << endl;

  cout << "  -F name of single input file"      << endl;
  cout << "     defaults: ''"                   << endl;

  cout << "  -f name of input filelist"         << endl;
  cout << "     defaults: ''"                   << endl;

  cout << "  -s sample name, for naming files"  << endl;
  cout << "     defaults: ntuple sample name"   << endl;
  
  cout << "  -w sumOfWeights histogram file"    << endl;

  cout << "  --isTruthOnly Run over truth only samples"    << endl;

  cout << "  --disableCuts disable the selector level cuts"    << endl;

  cout << "  -h print this help"                << endl;
}

bool check(const char* inputFlag, const char* compar, const char* log)
{

  if (strcmp(inputFlag, compar) == 0   ) return true;
  if ((strcmp(inputFlag, "-help") ==0 || strcmp(inputFlag, "-h") ==0 )){
    printf("  => %-30s : %-50s \n",compar,log);
    return false;
  }
  return false;
}


int main(int argc, char** argv)
{

  int nEvt = -1;
  int nSkip = 0;
  string sample      = "processed_skims";
  string treeName    = "nom";
  string systematic  = "NOM";
  string file = "";
  string fileList = "";
  string fileDir; 
  string selector = "";
  string sumOfWeightsHist = "";
  bool   isTruthOnly = false;
  bool   disableCuts = false;  
  
  cout << "SusyNtTest" << endl;
  cout << endl;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if ((!strcmp(argv[i], "-help") || !strcmp(argv[i], "-h") ))
      printf("%s","Printing help menu... \n");
    if (check(argv[i], "-n", "Set the maximum number of events to run over"))
      nEvt = atoi(argv[++i]);
    else if (check(argv[i], "-k", "Start from the given event number"))
      nSkip = atoi(argv[++i]);
    else if (check(argv[i], "-F", "Run on single file"))
      file = argv[++i];
    else if (check(argv[i], "-f", "Run on filelist (txtfile)"))
      fileList = argv[++i];
    else if (check(argv[i], "-D", "Not supported yet"))
      fileDir = argv[++i];
    else if (check(argv[i], "-s", TString::Format("Output file basename. Default: %s", sample.c_str()).Data()))
      sample = argv[++i];
    else if (check(argv[i], "-w", "Rootfile containing sumOfWeights to normalize"))
      sumOfWeightsHist = argv[++i];
    else if (check(argv[i], "-sys", "Not supported yet"))
      systematic = argv[++i];
    else if (check(argv[i], "-treeName", "Not supported yet"))
      treeName = argv[++i];
    else if (check(argv[i], "-selector", "Name of the selector to use (required)"))
      selector = argv[++i];
    else if (check(argv[i], "--isTruthOnly","Run over truth only samples"))
      isTruthOnly=true;
    else if (check(argv[i], "--disableCuts","disable the selector level cuts"))
      disableCuts = true;
    else if ((!strcmp(argv[i], "-help") || !strcmp(argv[i], "-h") ))
      return 0;
    else{
      std::cout << "Unknown argument:  " << argv[i] << std::endl;
      return 0;
    }
  }

  if(!file.empty())     cout << "  input   " << file     << endl;
  if(!fileList.empty()) cout << "  input   " << fileList << endl;
  if(!fileDir.empty())  cout << "  input   " << fileDir  << endl;
  cout << endl;

  // Build the input chains
  TChain* chain = new TChain("skim_NoSys");

  // Hold all the input files, such that we can extract the normalization
  std::vector<TString> inputFileList;
  inputFileList.clear();

  // Inputs passed by -f
  if( !file.empty() ){

    // Check if user provided a comma seperated list
    TString fileToken = TString(file);
    fileToken.ReplaceAll(" ","");
   
    TString separator="";
    if      ( fileToken.Contains(",") ) separator=",";
    else if ( fileToken.Contains(";") ) separator=";";

    if( !fileToken.IsWhitespace() ){
      TObjArray* tokenFiles = fileToken.Tokenize(",");
      // Get the list of cuts
      for(int i=0; i<=tokenFiles->GetLast(); i++){
        TString sam = ( (TObjString*)tokenFiles->At(i) )->String().Data();
	chain->Add( sam.Data() );
	inputFileList.push_back(sam);
      }
    }
    else{
      chain->Add(file.c_str());
      inputFileList.push_back(TString(file));
    }
  }
  else if ( !fileList.empty() ){
    ifstream if_fileList(fileList.c_str());
    if(!if_fileList.is_open()){
      cout << "ERROR opening fileList " << fileList << endl;
      exit(0);
    }
    string tm_fileName;
    while(if_fileList >> tm_fileName){
      cout << tm_fileName << endl;
      // Add protection against file read errors
      if(chain->Add(tm_fileName.c_str(), -1)==0){
        cout << "ERROR adding file " << tm_fileName << endl;
        exit(0);
      }
      inputFileList.push_back(TString(tm_fileName));
    }
    if_fileList.close();
  }
  else{
    std::cout << "No input! Exiting!" << std::endl;
    exit(0);
  }

  Long64_t nEntries = chain->GetEntries();
  chain->ls();

  TChain* metaData = new TChain("metaData");
  if(!file.empty()) metaData->Add(file.c_str());

  // Build the TSelector
  SkimEvtLooper* susyAna = new SkimEvtLooper();
  susyAna->setSampleName(sample);
  susyAna->setSelectorName(selector);
  susyAna->setInputFiles(inputFileList);

  // Set sumOfWeights histogram for normalization
  if (!sumOfWeightsHist.empty()) {
    susyAna->setSumOfWeightsHist(sumOfWeightsHist.c_str());
  }

  if (nSkip > 0) {
    std::cout << nSkip << " entries will be skipped" << std::endl;
    std::cout << "Won't copy sumw hist in this run!" << std::endl;
    susyAna->setSkipCopySumwHist(true);
  }

  // Is truth only?
  susyAna->isTruthOnly(isTruthOnly);

  // Disable cuts in case one wants
  susyAna->setDisableCuts(disableCuts);

  // Run the job
  if(nEvt<0) nEvt = nEntries;
  cout << endl;
  cout << "Total entries:   " << nEntries << endl;
  cout << "Process entries: " << nEvt << endl;

  chain->Process(susyAna, sample.c_str(), nEvt, nSkip);

  cout << endl;
  cout << "NtSelector job done" << endl;

  delete chain;
  return 0;
}
