
#ifndef NEARBYLEPISOCORRECTION_LINKDEF_h
#define NEARBYLEPISOCORRECTION_LINKDEF_h
#include <NearbyLepIsoCorrection/NearbyLepIsoCorrection.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#pragma link C++ namespace NearLep;
#pragma link C++ class NearLep::IsoCorrection;

#endif
#endif
