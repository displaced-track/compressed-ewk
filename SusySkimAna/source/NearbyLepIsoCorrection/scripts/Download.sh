#!/bin/bash

if [ -z ${ROOTCOREBIN} ]; then
	echo "Please Setup an ASG RootCore  Base Release >=2.3.24"
	exit
fi

cd ${ROOTCOREBIN}/../
git clone ssh://git@gitlab.cern.ch:7999/jojungge/NearbyLepIsoCorrection.git
# svn co ${SVNOFF}/PhysicsAnalysis/SUSYPhys/NearbyLepIsoCorrection/trunk NearbyLepIsoCorrection
# svn co ${SVNOFF}/InnerDetector/InDetRecTools/InDetTrackSelectionTool/tags/InDetTrackSelectionTool-00-02-17 InDetTrackSelectionTool
# svn co ${SVNOFF}/Reconstruction/RecoTools/IsolationTool/tags/IsolationTool-00-12-00 IsolationTool
# svn co ${SVNOFF}/Reconstruction/RecoTools/RecoToolInterfaces/tags/RecoToolInterfaces-00-04-00 RecoToolInterfaces
