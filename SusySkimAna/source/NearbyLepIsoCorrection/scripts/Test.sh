#!/bin/bash

if [ -z ${ROOTCOREBIN} ]; then
	echo "Please Setup an ASG RootCore  Base Release >=2.3.39"
	exit
fi

cd ${ROOTCOREBIN}/../
#~ rc compile NearbyLepIsoCorrection/
NearByLepCorrectionTester -I dcap://grid-srm.rzg.mpg.de/pnfs/rzg.mpg.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/mc15_13TeV/42/2b/AOD.05819610._000001.pool.root.1
#~ NearByLepCorrectionTester -I dcap://grid-srm.rzg.mpg.de/pnfs/rzg.mpg.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/mc15_13TeV/65/36/DAOD_SUSY2.06550485._000001.pool.root.1
