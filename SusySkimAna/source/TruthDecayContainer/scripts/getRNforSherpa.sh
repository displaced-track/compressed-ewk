baseDir=/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/

rm -f log_sherpa.dsid
for dir in `(cd ${baseDir}/; ls -d DSID*xxx/)`; do
    echo "dir: ${dir}" `cat log_sherpa.dsid | wc -l`
    grep -r "Sherpa" ${baseDir}/${dir}/MC* | grep -v svn | grep include | cut -d ":" -f 1 | rev | cut -d "/" -f 1 | rev | cut -d "." -f 2 | sort | uniq >> log_sherpa.dsid
done
echo `root -l -b -q 'squash_consecutive_DSIDs.cc("log_sherpa.dsid")' | grep -v "Processing squash_consecutive_DSIDs.cc"` | tee log_sherpa.txt
