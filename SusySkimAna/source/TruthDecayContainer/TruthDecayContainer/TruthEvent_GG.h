#ifndef TRUTHEVENT_GG_h
#define TRUTHEVENT_GG_h

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TObject.h"

#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/TruthEvent_XX.h"

typedef TLorentzVector tlv;


class TruthEvent_GG : public TruthEvent_XX {

 public:

  TruthEvent_GG();

  ~TruthEvent_GG();

  TruthEvent_GG(const TruthEvent_GG &rhs);

  //
  virtual void set_evt_GG(const tlv &in_pq11, const tlv &in_pq12, const int &in_q11_pdg, const int &in_q12_pdg, 
			  const tlv &in_pq21, const tlv &in_pq22, const int &in_q21_pdg, const int &in_q22_pdg, 	
			  const int &in_chi1_pdg, const int &in_chi2_pdg,
			  const tlv &in_pN1A    , const tlv &in_pN1B,
			  const Decay_boson &in_B1, const Decay_boson &in_B2);
  
  virtual void finalize();

  virtual int getQLabel(const int &pdg1, const int &pdg2);

  virtual void swap12();

  virtual void check_swap();

  virtual void print();

  virtual void clear();

  ClassDef(TruthEvent_GG,2);

  // tlv 
  tlv pgo1, pgo2;
  tlv pq11, pq12, pq21, pq22;

  // decay label
  int decayLabel_q1;
  int decayLabel_q2;

  // pdg
  int q11_pdg, q12_pdg, q21_pdg, q22_pdg;

 private:  




};







#endif
