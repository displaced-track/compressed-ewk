#ifndef DECAY_H_h
#define DECAY_H_h

#include "TruthDecayContainer/Decay_boson.h"

//+++++++++++++++++++ include STL +++++++++++++++++++++//
#include <iostream>
#include <vector>

//++++++++++++++++++ include ROOT lib ++++++++++++++++++++++//
#include "TLorentzVector.h"

//+++++++++++++++++++ include Ex lib +++++++++++++++++++++//
#include "TruthDecayContainer/Decay_boson.h"


typedef TLorentzVector tlv;


class Decay_H : public Decay_boson {

 public:

  Decay_H();

  ~Decay_H();
    
  Decay_H(const Decay_H &rhs);

  void finalize(const Decay_boson   &W1,   const Decay_boson   &W2,
		const Decay_boson   &Z1,   const Decay_boson   &Z2);

  static TString getDecayString(const int &decayLabel,
				const int &decayLabel_sub1,
				const int &decayLabel_sub2);

  void swap12();

  void check_swap();
  
  virtual void print();

  virtual void clear();

  ClassDef(Decay_H,2);
    
  // grandchildren info
  tlv pq11, pq12, pq21, pq22;
  int q11_pdg, q12_pdg,q21_pdg, q22_pdg;
  int decayLabel_sub1, decayLabel_sub2;

 private:  




};







#endif
