#include "TruthDecayContainer/Decay_tau.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/TruthDecayUtils.h"

//////////////////////////////////////////////////////
//
// Container storing the information of tau decay
//
/////////////////////////////////////////////////////

typedef TLorentzVector tlv;

Decay_tau::Decay_tau() :
  decayLabel(-1),
  subdecayLabel(-1),
  pdg(0),
  child_pdg(0),
  child2_pdg(0),
  coschild_tau(0.),
  coschild_rho(0.)
{
  clear();  
}

Decay_tau::~Decay_tau(){}

////////////////////////////////////////////////////////////////
Decay_tau::Decay_tau(const Decay_tau &rhs) :
  TObject(rhs),
  P4(rhs.P4),
  pMis(rhs.pMis),
  pchild(rhs.pchild),
  pchild2(rhs.pchild2),
  ptaunu(rhs.ptaunu),
  prhochild(rhs.prhochild),
  prhochild2(rhs.prhochild2),
  decayLabel(rhs.decayLabel),
  subdecayLabel(rhs.subdecayLabel),
  pdg(rhs.pdg),
  child_pdg(rhs.child_pdg),
  child2_pdg(rhs.child2_pdg),
  coschild_tau(rhs.coschild_tau),
  coschild_rho(rhs.coschild_rho)
{}

////////////////////////////////////////////////////////////////
void Decay_tau::finalize(){

  // Members that need to be filled by hand beforehand: 
  //
  //         tau's tlv (P4), pdg, visible tau's tlv, tlv, pdg, decayLabel, taunu tlv
  //
  
  // calc. cos-theta (angle between vistau and tau in the tau rest frame)
  coschild_tau =   TruthDecayUtils::costh_decay(pchild,P4);
  coschild_rho =   TruthDecayUtils::costh_decay(prhochild,pchild);
  //  

  pMis = P4-pchild;

}

////////////////////////////////////////////////////////////////
TString Decay_tau::getDecayString(int in_decayLabel){

  switch(in_decayLabel) {
  case 0:    return "#pi^{#pm}";
  case 1:    return "#pi^{#pm} #pi^{0}";
  case 2:    return "#pi^{#pm} n#pi^{0} (n>=2)";
  case 3:    return "3#pi^{#pm}";
  case 4:    return "3#pi^{#pm} n#pi^{0} (n>=1)";
  case 5:    return "(2,4,5)#pi^{#pm}";
  case 6:    return "(0,6)#pi^{#pm}";
  case 8:    return "leptonic";
  default :  return "Error";
  }
  return "";
}

////////////////////////////////////////////////////////////////
TString Decay_tau::getDecayString_tautau(int decayLabel1, int decayLabel2){  
  int ntaul=0; 
  if(decayLabel1==8) ntaul++;
  if(decayLabel2==8) ntaul++;
  return ntaul==2 ? "leplep" : ntaul==1 ? "lephad" : "hadhad";
}


////////////////////////////////////////////////////////////////
void Decay_tau::print(){


  std::cout << "  Decay_tau::print() --------------------------------------" << std::endl;
  TruthDecayUtils::print4mom(P4, "P4");
  TruthDecayUtils::print4mom(pchild,  "pvistau");
  TruthDecayUtils::print4mom(pchild2,  "p(electron/muon-neutrino associated with the leptonic tau decay)");
  TruthDecayUtils::print4mom(ptaunu,   "ptaunu");
  TruthDecayUtils::print4mom(pMis,     "pMis");

  std::cout << "    pdg: " <<  pdg << std::endl;
  std::cout << "    decayLabel: " << decayLabel << ", name: " << getDecayString(decayLabel) <<  std::endl;
  std::cout << "    child_pdg: " <<  child_pdg << std::endl;
  std::cout << "    child2_pdg: " <<  child2_pdg << std::endl;

  std::cout << "    costchild_tau: " <<  coschild_tau << std::endl;  
  std::cout << "  --------------------------------------------------------" << std::endl;


  return;
}

////////////////////////////////////////////////////////////////
void Decay_tau::clear(){

  // tlv
  P4.SetXYZM(0,0,0,0);
  pMis.SetXYZM(0,0,0,0); 

  pchild.SetXYZM(0,0,0,0); 
  pchild2.SetXYZM(0,0,0,0); 
  ptaunu.SetXYZM(0,0,0,0); 

  prhochild.SetXYZM(0,0,0,0); 
  prhochild2.SetXYZM(0,0,0,0); 

  // labels
  decayLabel=-1;
  subdecayLabel=-1;

  // pdg
  pdg=0;   child_pdg=0;   child2_pdg=0;

  // angles
  coschild_tau=0;
  coschild_rho=0;

   return;
}
