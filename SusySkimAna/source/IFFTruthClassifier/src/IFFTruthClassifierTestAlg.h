#ifndef IFFTRUTHCLASSIFFIER_TESTALG__H
#define IFFTRUTHCLASSIFFIER_TESTALG__H


#include "GaudiKernel/ToolHandle.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "IFFTruthClassifier/IIFFTruthClassifier.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"


class IFFTruthClassifierTestAlg:public AthAlgorithm {
    public:
    IFFTruthClassifierTestAlg (const std::string& name, ISvcLocator* pSvcLocator);
    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();
private: 
    ToolHandle<IIFFTruthClassifier> m_classifier; 
    ToolHandle<CP::IMuonSelectionTool> m_muon_selector; 


  SG::AuxElement::Accessor<int> m_tT;
  SG::AuxElement::Accessor<int> m_tO;
  SG::AuxElement::Accessor<int> m_mum_type;
  SG::AuxElement::Accessor<int> m_mum_origin;



};

#endif
