#ifndef IFFTRUTHCLASSIFIER_IIFFTRUTHCLASSIFIER_H_
#define IFFTRUTHCLASSIFIER_IIFFTRUTHCLASSIFIER_H_

#include "AsgTools/IAsgTool.h"
#include "IFFTruthClassifier/IFFTruthClassifierDefs.h"
#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"


class IIFFTruthClassifier : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IIFFTruthClassifier)
 public:
  virtual ~IIFFTruthClassifier() {}

  virtual IFF::Type classify(const xAOD::Electron& electron) const = 0;
  virtual IFF::Type classify(const xAOD::Muon& muon) const = 0;
};

#endif  // IFFTRUTHCLASSIFIER_IIFFTRUTHCLASSIFIER_H_
