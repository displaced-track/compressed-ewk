#ifndef SusySkimHiggsino_FakeSelector_H
#define SusySkimHiggsino_FakeSelector_H

//RootCore
#include "SusySkimMaker/BaseUser.h"

#include "SusySkimHiggsino/Common.h"

class FakeSelector : public BaseUser
{

 public:
  FakeSelector();
  ~FakeSelector() {};

  Common com;

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& );
  bool execute_merge(MergeTool*& );

  // Running on data or MC?
  bool doMC    = false;
  bool doTtbar = false;

  // Counter for keeping only fraction of MC events
  int counter = 0;

  // Prescaled single-lepton triggers to be saved:
  std::vector<TString> prescaledTriggers = {
    "HLT_e5_etcut",
    "HLT_e5_lhloose_nod0",
    "HLT_e5_lhloose_idperf",
    "HLT_e5_lhloose_nod0_idperf",
    "HLT_e5_lhvloose",
    "HLT_e5_lhvloose_nod0", 
    "HLT_e5_lhtight_idperf",
    "HLT_e5_lhtight_nod0",
    "HLT_e9_lhloose_nod0",
    "HLT_e10_etcut_L1EM7",
    "HLT_e10_lhvloose_nod0_L1EM7",
    "HLT_e10_lhtight_idperf",
    "HLT_e10_lhvloose_L1EM7",
    "HLT_e12_lhloose_nod0",
    "HLT_e12_lhvloose_L1EM10VH",
    "HLT_e12_lhvloose_nod0_L1EM10VH",
    "HLT_e15_etcut_L1EM7",
    "HLT_e15_lhvloose_L1EM13VH",
    "HLT_e15_lhvloose_nod0_L1EM7",
    "HLT_e17_lhvloose",
    "HLT_e17_lhvloose_nod0",
    "HLT_e17_lhvloose_nod0_L1EM15VHI",
    "HLT_e19_lhvloose_nod0",
    "HLT_e20_lhvloose",
    "HLT_e20_lhvloose_nod0",
    "HLT_mu4", 
    "HLT_mu6",
    "HLT_mu10",
    "HLT_mu10_idperf",
    "HLT_mu14",
    "HLT_mu14_ivarloose",
    "HLT_mu18",
  };

};

static const BaseUser* FakeSelector_instance __attribute__((used)) = new FakeSelector();

#endif
