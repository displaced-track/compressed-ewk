#ifndef SusySkimHiggsino_MonojetSelector_H
#define SusySkimHiggsino_MonojetSelector_H

#include "SusySkimMaker/BaseUser.h"
#include "SusySkimMaker/Objects.h"

#include <TLorentzVector.h>
#include <TVector3.h>
#include <TFormula.h>
#include <vector>
#include <map>

#include "SusySkimHiggsino/Common.h"

class MonojetSelector : public BaseUser
{

 public:

  MonojetSelector();

  ~MonojetSelector();

  Common com;

  // Main processors
  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& );
  bool execute_merge(MergeTool*& );

  // Cut flow processor (when in cutFlowOnly mode)
  void doCutFlow(ConfigMgr*& configMgr);
  void fillCutFlows(ConfigMgr*& configMgr, TString cutFlowName, TString description);

  //
  // Member variables
  //

  // Control flags
  bool m_cutFlowOnly;
};

static const BaseUser* MonojetSelector_instance __attribute__((used)) = new MonojetSelector();

#endif
