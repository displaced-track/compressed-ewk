#ifndef SusySkimHiggsino_HiggsinoSelector_H
#define SusySkimHiggsino_HiggsinoSelector_H

//RootCore
#include "SusySkimMaker/BaseUser.h"

#include "SusySkimHiggsino/Common.h"

class HiggsinoSelector : public BaseUser
{

 public:
  HiggsinoSelector();
  ~HiggsinoSelector() {};

  Common com;

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& );
  bool execute_merge(MergeTool*& );

};

static const BaseUser* HiggsinoSelector_instance __attribute__((used)) = new HiggsinoSelector();

#endif
