#include "SusySkimHiggsino/FakeSelector.h"
#include <algorithm>

// includes needed for emulation of L1Topo based triggers
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODJet/JetContainer.h"

// includes needed for RJR variable computation 
#include "RestFrames/RestFrames.hh"
#include <TLorentzVector.h>
#include <TVector3.h>
#include <vector>


// ------------------------------------------------------------------------------------------ //
FakeSelector::FakeSelector() :
  BaseUser("SusySkimHiggsino", "FakeSelector")
{

}



// ------------------------------------------------------------------------------------------ //
void FakeSelector::setup(ConfigMgr*& configMgr)
{

    com.setup(configMgr);

  // Turn off OR for baseline muons for fakes selection
    configMgr->obj->disableORBaselineMuons(true);

  // Add prescaled single-lepton triggers
    for (auto chainName : prescaledTriggers) {
        configMgr->treeMaker->addFloatVariable(chainName+"_prescale", -1.0);
    // These triggers are already saved by Common, so no need to make new branches for them
        bool inCommon = false;
        for (auto trigger : com.triggerChains) {
            if (chainName == trigger) {
                inCommon = true;
            }
        }
        if (!inCommon) {
            configMgr->treeMaker->addBoolVariable(chainName, false);
        }
    }
}
  





// ------------------------------------------------------------------------------------------ //
bool FakeSelector::doAnalysis(ConfigMgr*& configMgr)
{
  /*
    This is the main method, which is called for each event
  */

  // Skims events by imposing any cuts you define in this method below
  if( !passCuts(configMgr) ) return false;

  // Add prescaled single-lepton triggers
    for (auto chainName : this->prescaledTriggers) {
        configMgr->treeMaker->setFloatVariable(chainName+"_prescale", configMgr->obj->evt.getTriggerPrescale(chainName.Data()));
    // These triggers are already covered by Common:
        bool inCommon = false;
        for (auto trigger : com.triggerChains) {
            if (chainName == trigger) {
                inCommon = true;
            }
        }
        if (!inCommon) {
            configMgr->treeMaker->setBoolVariable(chainName, configMgr->obj->evt.getHLTEvtTrigDec(chainName.Data()));
        }
    }  


    com.doAnalysis(configMgr);

    // Fill the output tree
    configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");


    return true;

}



// ------------------------------------------------------------------------------------------ //
bool FakeSelector::passCuts(ConfigMgr*& configMgr)
{

  //This method is used to apply any cuts you wish before writing
  //the output trees

  // JEFF: SHOULD SOME OF THIS BE PUT INTO A COMMON::PASSCUTS FUNCTION?
  double weight = 1; // pb-1
  if(!configMgr->obj->evt.isData()){

    // Note that 2015+2016+2017 is not exactly 80 fb-1, but close enough
    weight = 140000; // pb-1
  }
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::GEN);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::EVT);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::LEP);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::JVT);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::PILEUP);

  // Fill cutflow histograms
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  ////////////////////////////////////////
  //// begin ntuple preselection cuts ////
  ////////////////////////////////////////

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  // At least one baseline lepton
  if( configMgr->obj->baseLeptons.size() < 1 ) return false;
  configMgr->cutflow->bookCut("cutFlow","At least one baseline lepton", weight );

  // At least one signal jet with pT > 30 GeV
  if( getNJets(configMgr->obj, 30.0) < 1 ) return false;
  configMgr->cutflow->bookCut("cutFlow",">= 1 signal jet with pT > 30 GeV", weight );

  // Only select events that pass at least one of the prescaled single lepton triggers
  std::vector<bool> prescaledTrigDec;
  for (auto chainName : prescaledTriggers) {
    prescaledTrigDec.push_back( configMgr->obj->evt.getHLTEvtTrigDec(chainName.Data()) );
  }

  bool failsAllPrescaledTrig = all_of(prescaledTrigDec.begin(), prescaledTrigDec.end(), [](bool trigDec) { return  trigDec == false; });
  if(failsAllPrescaledTrig == true){ return false; }  
  configMgr->cutflow->bookCut("cutFlow","Passed prescaled triggers", weight );
  
  return true;
}

// ------------------------------------------------------------------------------------------ //
void FakeSelector::finalize(ConfigMgr*& configMgr)
{

  /*
   This method is called at the very end of the job. Can be used to merge cutflow histograms 
   for example. See CutFlowTool::mergeCutFlows(...)
  */

}
// ------------------------------------------------------------------------------------------ //
bool FakeSelector::init_merge(MergeTool*& mergeTool){return true; }
// ------------------------------------------------------------------------------------------ //
bool FakeSelector::execute_merge(MergeTool*& mergeTool){return true;}
// ------------------------------------------------------------------------------------------ //
