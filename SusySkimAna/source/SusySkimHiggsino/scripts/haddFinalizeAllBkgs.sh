#cd R21_SUSY16_Bkgs_Combined_mc16acd
#source haddAll.sh
python $TestArea/SusySkimHiggsino/scripts/renameTrees.py;
python $TestArea/SusySkimHiggsino/scripts/haddTreesWithDifferentNames.py diboson diboson2L diboson3L diboson4L; 
python $TestArea/SusySkimHiggsino/scripts/haddTreesWithDifferentNames.py top ttbar singletop; 
python $TestArea/SusySkimHiggsino/scripts/haddTreesWithDifferentNames.py other topOther higgs triboson;
python $TestArea/SusySkimHiggsino/scripts/haddTreesWithDifferentNames.py Zjets_all Zjets Zjets_lowHT;
python $TestArea/SusySkimHiggsino/scripts/haddTreesWithDifferentNames.py Zttjets_all Zttjets Zttjets_lowHT;
$TestArea/SusySkimHiggsino/scripts/finalizeBkgs.sh;

