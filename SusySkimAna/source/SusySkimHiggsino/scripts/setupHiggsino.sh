setupATLAS

# Note that several previous releases had issues with 
# EventLoop and Condor, but 2.4.37 should be fine!
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisBaseReleaseNotes24
rcSetup Base,2.4.37
lsetup pyami rucio

# don't forget to setup a proxy! I run this on it's own, so the script
# doesn't stop to wait for me to enter a password
# voms-proxy-init -voms atlas --valid 96:00

# if the ssh protocol fails, there is also the http or krb5 protocols from the gitlab site
# e.g. git clone https://:@gitlab.cern.ch:8443/atlas-phys-susy-higgsino/SusySkimHiggsino.git
# or git clone https://gitlab.cern.ch/atlas-phys-susy-higgsino/SusySkimHiggsino.git

git clone ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimMaker.git
git clone ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimDriver.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-higgsino/SusySkimHiggsino.git

git clone -b NearbyLepIsoCorrection-00-01-00 --single-branch ssh://git@gitlab.cern.ch:7999/jojungge/NearbyLepIsoCorrection.git

rc checkout_pkg atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-69

# Only need to do the below if the current tag isn't up to date...
# Get the latest xsecs: make sure to check which revision of the trunk is the latest one, for easy documentation!!
#svn export svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/trunk/data/susy_crosssections_13TeV.txt
#svn export svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/trunk/data/mc15_13TeV/MGPy8EG_A14N23LO_Higgsino_2L.txt
#svn export svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/trunk/data/mc15_13TeV/MGPy8EG_A14N23LO_SlepSlep_direct_XX_YY.txt
#mv susy_crosssections_13TeV.txt SUSYTools/data/
#mv MGPy8EG_A14N23LO_Higgsino_2L.txt SUSYTools/data/mc15_13TeV
#mv MGPy8EG_A14N23LO_SlepSlep_direct_XX_YY.txt SUSYTools/data/mc15_13TeV

rc checkout ${ROOTCOREBIN}/../SUSYTools/doc/packages.txt

rc find_packages
rc clean
rc compile
