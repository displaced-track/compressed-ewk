import glob,os,sys

rucioAccount = os.environ.get("RUCIO_ACCOUNT")
if not rucioAccount :
    exit("Need to set RUCIO_ACCOUNT variable!")

rucioScope = "user."+rucioAccount

datasetName = str(sys.argv[1])
if not datasetName.startswith(rucioScope) :
    exit("Invalid dataset name! Has to begin with "+rucioScope+":")

os.system("rucio add-dataset "+datasetName)

listOfFiles = glob.glob("*tree*.root")

for fName in listOfFiles :
    os.system("rucio upload --rse BNL-OSG2_SCRATCHDISK --scope " + rucioScope +" " + fName)
    os.system("rucio attach "+datasetName+" "+rucioScope+":"+fName)

# copy to a few LOCALGROUPDISKS, so these files don't get deleted
os.system("rucio add-rule "+datasetName+" 1 BNL-OSG2_LOCALGROUPDISK --notify Y")
os.system("rucio add-rule "+datasetName+" 1 NET2_LOCALGROUPDISK --notify Y")
