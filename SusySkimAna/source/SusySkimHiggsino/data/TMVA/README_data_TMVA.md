# TMVA xml file summary

## Nominal PLT
### Electrons
-File name
  - TMVAClassification_BDT_Electron_PromptLeptonVeto.weights.xml
- Input Variables
  - TrackJetNTrack
  - rnnip
  - DL1mu
  - PtRel
  - PtFrac
  - DRlj
  - TopoEtCone30Rel
  - PtVarCone30Rel
- Descriptions
  - Nominal PLT xml files, taken from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetTagNonPromptLepton/InputData-2017-10-27/

### Muons
- File name
  - TMVAClassification_BDT_Muon_PromptLeptonVeto.weights.xml
- Input Variables
  - TrackJetNTrack
  - rnnip
  - DL1mu
  - PtRel
  - PtFrac
  - DRlj
  - TopoEtCone30Rel
  - PtVarCone30Rel
- Descriptions
  - Nominal PLT xml files, taken from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetTagNonPromptLepton/InputData-2017-10-27/

## Low-Pt trained PLT
### Electrons
- File name
  - TMVAClassification_BDT_Electron_LowPtPromptLeptonTagger.weights.xml
- Input Variables
  - CorrPtvarcone20/Pt/1000
  - CorrTopoetcone20/Pt
  - PLTInput_PtRel
  - PLTInput_PtFrac
  - PLTInput_DL1mu
  - PLTInput_rnnip
  - PLTInput_DRlj
  - PLTInput_TrackJetNTrack
- Spectator Variables
  - IsoCorrFixedCutTightTrackOnly
  - IsoCorrLooseTrackOnly
  - Pt
  - IsoCorrGradientLoose
- Descriptions
  - Low-Pt trained PLT xml files.
  - Training with no-iso, pt < 7 GeV leptons. Setups summarized in https://its.cern.ch/jira/browse/HIGGSINO-104
  - The same xml files as used for user.sakatsuk:SusySkimHiggsino_v2.4_addNominalv6	

### Muons
- File name
  - TMVAClassification_BDT_Muon_LowPtPromptLeptonTagger.weights.xml
- Input Variables
  - CorrPtvarcone30/Pt/1000
  - CorrTopoetcone20/Pt
  - PLTInput_PtRel
  - PLTInput_PtFrac
  - PLTInput_DL1mu
  - PLTInput_rnnip
  - PLTInput_DRlj
  - PLTInput_TrackJetNTrack
- Spectator Variables
  - IsoCorrFixedCutTightTrackOnly
  - IsoCorrLooseTrackOnly
  - Pt
  - IsoCorrGradientLoose
- Descriptions
  - Low-Pt trained PLT xml files.
  - Training with no-iso, pt < 7 GeV leptons. Setups summarized in https://its.cern.ch/jira/browse/HIGGSINO-104
  - The same xml files as used for user.sakatsuk:SusySkimHiggsino_v2.4_addNominalv6	
