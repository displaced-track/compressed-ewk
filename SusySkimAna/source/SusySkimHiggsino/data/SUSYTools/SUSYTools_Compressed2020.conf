########################################################
# SUSYTools config for Compressed 2020 Config
# Based on EWK Combinations Config, possible sources of contention noted (baseline electrons and muons)
########################################################
#### default baseline electrons #####
EleBaseline.Pt: 4500.
EleBaseline.Eta: 2.47
EleBaseline.Id: VeryLooseLLH  #EWKCombo: LooseAndBLayerLLH
EleBaseline.z0: 0.5
EleBaseline.CrackVeto: false

#
#### signal electrons #####
Ele.Et: 4500. 
Ele.Eta: 2.47
Ele.CrackVeto: false
Ele.Iso: FCLoose
Ele.IsoHighPt: FCHighPtCaloOnly
Ele.Id: MediumLLH
Ele.d0sig: 5.
Ele.z0: 0.5
Ele.DoModifiedId: true
# ChargeIDSelector WP
Ele.CFT: None
#
#### default baseline muons #####
MuonBaseline.Pt: 3000.
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 3 #  1 = Medium, 2 = Loose, 3 = VeryLoose, (EWKCombo = Medium)
MuonBaseline.z0: 0.5
#
#### signal muons #####
Muon.Pt: 3000.
Muon.Eta: 2.5
Muon.Id: 1 
# TODO: AR updated needed adjustment, reverted to ST defaults --> requires check!
#Muon.Iso: FCTightTrackOnly
#Muon.IsoHighPt: FCLoose # change WP if you want
Muon.Iso: Loose_VarRad
Muon.IsoHighPt: Loose_VarRad # change WP if you want
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.2
#
#### everything below this is analysis dependent, please edit freely #####
#
#
PhotonBaseline.Pt: 25000.
PhotonBaseline.Eta: 2.37
PhotonBaseline.Id: Tight
#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.5
Tau.Id: Medium
#Tau.DoTruthMatching: false
#
Jet.Pt: 20000.
Jet.Eta: 4.5
Jet.InputType: 9 # EMTopo 1, PFlow: 9 
#Jet.JvtWP: Medium
Jet.JVT_WP: Medium
Jet.JvtPtMax: 120.0e3
#Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_SimpleJER.config
Jet.UncertConfig: rel21/Summer2019/R4_SR_Scenario1.config
Jet.JMSCalib: false 

#
FwdJet.doJVT: true
FwdJet.JvtEtaMin: 2.5 
# TODO: AR updated needed adjustment, reverted to ST defaults --> requires check!
#FwdJet.JvtPtMax: 50e3
#FwdJet.JvtOp: Tight
FwdJet.JvtWP: Tight # Tight fJVT with Tight MET / Loose fJVT with Tenacious MET
FwdJet.JvtPtMax: 120.0e3
#
Jet.LargeRcollection: None # AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets, set to None to turn this off
Jet.LargeRuncConfig: None # rel21/Spring2019/R10_GlobalReduction.config, set to None to turn this off
#Jet.LargeRuncVars: pT,mass,D2Beta1     # W/Z Taggers
#Jet.LargeRuncVars: pT,Split23,Tau32WTA # Top taggers
#
# The appropriate Large-R Jet Tagger uncertainty tool will be set up provided the config below is set up correctly. More information can be found here for taggers: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BoostedJetTaggingRecommendationFullRun2 and here for the associated uncertainties: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21ConsolidatedLargeRTaggerSF. These taggers/uncertainties are valid only for FullSim. 
Jet.WtaggerConfig: SmoothedInclWTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_SUSYOpt_MC16_20210129.dat # set to None to turn this off
Jet.ZtaggerConfig: SmoothedInclZTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_SUSYOpt_MC16_20210129.dat # set to None to turn this off
Jet.ToptaggerConfig: JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat # set to None to turn this off
#
BadJet.Cut: LooseBad
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
#
Btag.Tagger: DL1r # DL1, DL1mu, DL1r, MV2c10mu, MV2c10r, MC2cl100_MV2c100
# ST defaults for calib file and time stamp outdated (status Feb 21) -> should be up-to-date now (Feb 22)
# Btag.CalibPath: xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root
Btag.TimeStamp: 201903
Btag.WP: FixedCutBEff_85
Btag.MinPt: 20000.
#
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets # AntiKt2PV0TrackJets
TrackJet.Pt: 20000.
TrackJet.Eta: 2.8
BtagTrkJet.Tagger: DL1  
BtagTrkJet.WP: FixedCutBEff_85
BtagTrkJet.MinPt: 20000.
#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: false
OR.ElBjet: false 
OR.MuBjet: false 
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
OR.BJetPtUpperThres: -1
#
OR.DoFatJets: false
OR.EleFatJetDR: -999.
OR.JetFatJetDR: -999.
#
SigLep.RequireIso: true
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight # Loose, Tight, Tighter, Tenacious
MET.RemoveOverlappingCaloTaggedMuons: true 
#MET.DoRemoveMuonJets: true
#MET.UseGhostMuons: false
#MET.DoMuonEloss: false
#MET.DoMuonJetOR: 1
MET.DoTrkSyst: 1
MET.DoCaloSyst: 0
#
# Trigger SFs configuration
# dilepton trigger SF setup - taken from Higgs->Inivs analysis
Trig.Dilep2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50 || 2e12_lhloose_L12EM10VH || mu18_mu8noL1
Trig.Dilep2016: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e17_lhvloose_nod0 || mu22_mu8noL1
Trig.Dilep2017: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || mu22_mu8noL1
Trig.Dilep2018: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || mu22_mu8noL1



# actual Mu files have to be set in SUSYTools
PRW.ActualMu2017File: GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRW.ActualMu2018File: GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root

#
StrictConfigCheck: true
