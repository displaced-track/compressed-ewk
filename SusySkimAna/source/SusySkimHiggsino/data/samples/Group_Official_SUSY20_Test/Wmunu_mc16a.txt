# NAME          : Wmunu_mc16a
# Data          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_SUSY20.e8351_e7400_s3126_r9364_r9315_p4927
mc16_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_SUSY20.e8351_s3126_r9364_r9315_p4927
mc16_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_SUSY20.e8351_s3126_r9364_r9315_p4927
