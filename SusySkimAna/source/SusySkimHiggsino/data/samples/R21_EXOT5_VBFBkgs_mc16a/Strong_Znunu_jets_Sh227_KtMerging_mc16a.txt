
# NAME 		 : Strong_Znunu_jets_Sh227_KtMerging 
# DATA 		 : 0 
# AF2 		 : 0 
# PRIORITY 	 : 0 

mc16_13TeV.312484.Sh_227_NN30NNLO_Znunu_PTV100_140_MJJ0_500_KtMerging.deriv.DAOD_EXOT5.e7728_s3126_r9364_p4060
mc16_13TeV.312485.Sh_227_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_KtMerging.deriv.DAOD_EXOT5.e7728_s3126_r9364_p4060
mc16_13TeV.312486.Sh_227_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_KtMerging.deriv.DAOD_EXOT5.e7728_s3126_r9364_p4060
mc16_13TeV.312487.Sh_227_NN30NNLO_Znunu_PTV140_220_MJJ0_500_KtMerging.deriv.DAOD_EXOT5.e7739_s3126_r9364_p4060
mc16_13TeV.312488.Sh_227_NN30NNLO_Znunu_PTV140_220_MJJ500_1000_KtMerging.deriv.DAOD_EXOT5.e7739_s3126_r9364_p4060
mc16_13TeV.312489.Sh_227_NN30NNLO_Znunu_PTV140_220_MJJ1000_E_CMS_KtMerging.deriv.DAOD_EXOT5.e7739_s3126_r9364_p4060
mc16_13TeV.312490.Sh_227_NN30NNLO_Znunu_PTV220_280_MJJ0_500_KtMerging.deriv.DAOD_EXOT5.e7728_s3126_r9364_p4060
mc16_13TeV.312491.Sh_227_NN30NNLO_Znunu_PTV220_280_MJJ500_1000_KtMerging.deriv.DAOD_EXOT5.e7728_s3126_r9364_p4060
mc16_13TeV.312492.Sh_227_NN30NNLO_Znunu_PTV220_280_MJJ1000_E_CMS_KtMerging.deriv.DAOD_EXOT5.e7728_s3126_r9364_p4060
mc16_13TeV.312493.Sh_227_NN30NNLO_Znunu_PTV280_500_MJJ0_500_KtMerging.deriv.DAOD_EXOT5.e7739_s3126_r9364_p4060
mc16_13TeV.312494.Sh_227_NN30NNLO_Znunu_PTV280_500_MJJ500_1000_KtMerging.deriv.DAOD_EXOT5.e7739_s3126_r9364_p4060
mc16_13TeV.312495.Sh_227_NN30NNLO_Znunu_PTV280_500_MJJ1000_E_CMS_KtMerging.deriv.DAOD_EXOT5.e7739_s3126_r9364_p4060
