# NAME          : diboson_fullLep_jj_noVVV_Sherpa2212
# Data          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.700539.Sh_2212_lllljj_Min_N_TChannel.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700540.Sh_2212_lllvjj_Min_N_TChannel.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700541.Sh_2212_llvvjj_os_Min_N_TChannel.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700542.Sh_2212_llvvjj_ss_Min_N_TChannel.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
