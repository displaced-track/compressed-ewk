
# NAME     : VBFwinobino_40_35_2L2MET75
# DATA     : 0
# AF2      : 1
# PRIORITY : 0

mc16_13TeV:mc16_13TeV.398085.MGPy8EG_A14N23LO_SM_N2C1pWBVBFQED_40_35_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398115.MGPy8EG_A14N23LO_SM_N2C1mWBVBFQED_40_35_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398145.MGPy8EG_A14N23LO_SM_C1C1WBVBFQED_40_35_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
