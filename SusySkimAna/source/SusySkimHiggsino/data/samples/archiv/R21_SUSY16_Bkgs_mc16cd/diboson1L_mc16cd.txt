
# NAME 		: diboson1L
# DATA 		: 0
# AF2 		: 0
# PRIORITY 	: 0

mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_SUSY16.e5583_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_SUSY16.e5583_s3126_r10201_r10210_p3387
mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_SUSY16.e5983_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.deriv.DAOD_SUSY16.e5525_s3126_r10201_r10210_p3387
mc16_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.deriv.DAOD_SUSY16.e5525_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_SUSY16.e5916_e5984_s3126_r10201_r10210_p3387
