
# NAME     : VBFwinobino_100_90_2L2MET75
# DATA     : 0
# AF2      : 1
# PRIORITY : 0

mc16_13TeV:mc16_13TeV.398110.MGPy8EG_A14N23LO_SM_N2C1pWBVBFQED_100_90_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10724_r10726_p3652
mc16_13TeV:mc16_13TeV.398140.MGPy8EG_A14N23LO_SM_N2C1mWBVBFQED_100_90_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10724_r10726_p3652
mc16_13TeV:mc16_13TeV.398170.MGPy8EG_A14N23LO_SM_C1C1WBVBFQED_100_90_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10724_r10726_p3652
