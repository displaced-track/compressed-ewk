# NAME          : data17
# Data          : 1
# AF2           : 0
# PRIORITY      : 0

data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodC.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodH.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodK.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodI.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodE.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
data17_13TeV:data17_13TeV.periodF.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002
