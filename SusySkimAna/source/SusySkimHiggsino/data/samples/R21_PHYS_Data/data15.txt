# NAME          : data15
# Data          : 1
# AF2           : 0
# PRIORITY      : 0

data15_13TeV:data15_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002
data15_13TeV:data15_13TeV.periodE.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002
data15_13TeV:data15_13TeV.periodF.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002
data15_13TeV:data15_13TeV.periodG.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002
data15_13TeV:data15_13TeV.periodH.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002
data15_13TeV:data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002
