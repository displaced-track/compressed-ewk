# NAME          : diboson_fullLep_jj
# Data          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.364283.Sherpa_222_NNPDF30NNLO_lllljj_EW6.deriv.DAOD_PHYS.e6055_s3126_r10201_p5001
mc16_13TeV.364284.Sherpa_222_NNPDF30NNLO_lllvjj_EW6.deriv.DAOD_PHYS.e6055_s3126_r10201_p5001
mc16_13TeV.364286.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW4.deriv.DAOD_PHYS.e6055_s3126_r10201_p5001
mc16_13TeV.364287.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW6.deriv.DAOD_PHYS.e6055_s3126_r10201_p5001
